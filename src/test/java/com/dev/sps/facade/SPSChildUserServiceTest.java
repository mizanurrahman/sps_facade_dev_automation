package com.dev.sps.facade;

/***************************************************************************************************************************
 ***  @@Author
 ***  Mohammed Rahman
 ***  This Tests Will Be Using To Test Facade Child Services.
 ***************************************************************************************************************************/


import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

public class SPSChildUserServiceTest extends BaseTest
{

	//Child
	//private String title="MR";
	private String childfirstName="Zach";
	private String childlastName="White";
	private String childbirthYear="2006";
	private String childbirthMonth="09";
	private String childbirthDay="24";
	private String childGrade="04";
	private String childSPSID;
	private List<String> teacherIds=new ArrayList<String>();
	private List<String> parentIds=new ArrayList<String>();
	//private List<String> readinglevelIds=new ArrayList<String>();
	//private List<String> interestIds=new ArrayList<String>();
	
	//Update Child Info
	private String changedchildfirstName="Jack";
	private String changedchildlastName="White";
	private String changedchildbirthYear="2007";
	private String changedchildbirthMonth="07";
	private String changedchildbirthDay="27";
	private String changedchildGrade="03";
	//private List<String> changedreadinglevelIds=new ArrayList<String>();
	//private List<String> changedinterestIds=new ArrayList<String>();
	//Teacher
	private String teacherfirstName="Willam";
	private String teacherlastName="Jackson";
	private String email;
	private String password="passed1";
	private String schoolId="411785";
	private List<String> userType1=new ArrayList<String>();
	private String teacherSPSID;
	//private boolean firstYearTeaching = true;
	
	//Parent
	private String parentfirstName="William";
	private String parentlastName="Green";
	private String parentSPSID;
	private List<String> userType2=new ArrayList<String>();
	//private String displayName="TicToc";
	//private List<String> interesParent=new ArrayList<String>();
	//private String parentPrimaryRole="2";
	
	//End Points
	private static final String ENDPOINT_PARENT_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_PARENT_GET="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_TEACHER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_TEACHER_GET="/spsuser/{spsId}?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_ADDCHILD="/spschilduser/?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_GET_EDIT_DELETE_CHILD="/spschilduser/{spsId}?clientId=SPSFacadeAPI";
	
 @Test
	public void createChildUserGetChildTest()
	{
		System.out.println("########################################################################");	
		System.out.println("******* Create Teacher and Parent, Add Child and Get Child Info*********");
		System.out.println("########################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(createTeacherPayload()).
								when()
										.post(ENDPOINT_TEACHER_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createTeacherResponseValidator())
										.extract();		
		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Get Teacher 
		ExtractableResponse<Response> getTeacherResponse=
								given()
										.pathParam("spsId",teacherSPSID).
								when()
										.get(ENDPOINT_TEACHER_GET).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getTeacherResponse.asString());
			
		// Light Parent Registration
		ExtractableResponse<Response> createParentResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(createParentPayload()).
								when()
										.post(ENDPOINT_PARENT_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createParentResponseValidator())
										.extract();		
		parentSPSID=createParentResponse.path("spsId");	
		System.out.println(parentSPSID);
		// Get Parent
		ExtractableResponse<Response> getParentResponse=
								given()
										.pathParam("spsId",parentSPSID).
								when()
										.get(ENDPOINT_PARENT_GET).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getParentResponse.asString());
		
		// Add Child
		ExtractableResponse<Response> createAddChildResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(addChildPayload()).
								when()
										.post(ENDPOINT_ADDCHILD).
								then()
										.statusCode(201)
										.spec(addChildResponseValidator())
										.extract();	
		childSPSID=createAddChildResponse.path("spsId");	
		System.out.println(childSPSID);
	
		// Get Child
		ExtractableResponse<Response> getChildResponse=
								given()
										.pathParam("spsId",childSPSID).
								when()
										.get(ENDPOINT_GET_EDIT_DELETE_CHILD).
								then()
										.statusCode(200)
										.extract();	
		System.out.println(">>>>>>>>>> Child Info <<<<<<<<<<");
		System.out.println(getChildResponse.asString());
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
}	

 @Test
	public void createChildEditAndGetChildTest() throws InterruptedException
	{
		System.out.println("##################################################################################");	
		System.out.println("******* Create Teacher and Parent, Add Child then Update and Get Child Info*******");
		System.out.println("##################################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(createTeacherPayload()).
								when()
										.post(ENDPOINT_TEACHER_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createTeacherResponseValidator())
										.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(">>>>>>>>>Teacher's SPSID = " +teacherSPSID);
					
		// Light Parent Registration
		ExtractableResponse<Response> createParentResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(createParentPayload()).
								when()
										.post(ENDPOINT_PARENT_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createParentResponseValidator())
										.extract();		
		parentSPSID=createParentResponse.path("spsId");	
		System.out.println(">>>>>>>>>Parent's SPSID = " +parentSPSID);
		Thread.sleep(4000L);		
		// Add Child
		ExtractableResponse<Response> createAddChildResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(addChildPayload()).
								when()
										.post(ENDPOINT_ADDCHILD).
								then()
										.statusCode(201)
										.spec(addChildResponseValidator())
										.extract();	
		childSPSID=createAddChildResponse.path("spsId");	
		System.out.println(">>>>>>>>>Child's SPSID = " +childSPSID);
				
		// Get Child
		ExtractableResponse<Response> getChildResponse=
								given()
										.pathParam("spsId",childSPSID).
								when()
										.get(ENDPOINT_GET_EDIT_DELETE_CHILD).
								then()
										.statusCode(200)
										.extract();		
		//System.out.println(getChildResponse.asString());
		
		// Update Child
		//ExtractableResponse<Response> createEditChildResponse=
			    				  given()
										.log().all()
										.pathParam("spsId",childSPSID)
										.contentType("application/json")
										.body(updateChildPayload()).
								  when()
								  		.put(ENDPOINT_GET_EDIT_DELETE_CHILD).
								  then()
										.statusCode(200)
										.extract();	
				//childSPSID=createEditChildResponse.path("spsId");	
				//System.out.println(childSPSID);
				
		// Get Child
		ExtractableResponse<Response> getUpdatedChildResponse=
								given()
										.pathParam("spsId",childSPSID).
								when()
										.get(ENDPOINT_GET_EDIT_DELETE_CHILD).
								then()
										.statusCode(200)
										.spec(updateChildResponseValidator())
										.extract();	
		System.out.println(">>>>>>>>>>Updated ChildInfo<<<<<<<<<<");
		System.out.println(getUpdatedChildResponse.asString());
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		
		System.out.println(">>>>>>>>>>ChildInfo<<<<<<<<<<");
		System.out.println(getChildResponse.asString());
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}	
 @Test
	public void deleteChildUserGetChildTest() 
	{
		System.out.println("##################################################################################");	
		System.out.println("******* Create Teacher and Parent, Add Child then Delete and Get Child Info*******");
		System.out.println("##################################################################################");
		
		// Light Teacher Registration
		ExtractableResponse<Response> createTeacherResponse=
						given()
								.log().all()
								.contentType("application/json")
								.body(createTeacherPayload()).
						when()
								.post(ENDPOINT_TEACHER_REGISTRATION).
						then()
								.statusCode(201)
								.spec(createTeacherResponseValidator())
								.extract();		
		teacherSPSID=createTeacherResponse.path("spsId");	
		System.out.println(teacherSPSID);
					
		// Get Teacher 
		ExtractableResponse<Response> getTeacherResponse=
								given()
										.pathParam("spsId",teacherSPSID).
								when()
										.get(ENDPOINT_TEACHER_GET).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getTeacherResponse.asString());
			
		// Light Parent Registration
		ExtractableResponse<Response> createParentResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(createParentPayload()).
								when()
										.post(ENDPOINT_PARENT_REGISTRATION).
								then()
										.statusCode(201)
										.spec(createParentResponseValidator())
										.extract();		
		parentSPSID=createParentResponse.path("spsId");	
		System.out.println(parentSPSID);
		// Get Parent
		ExtractableResponse<Response> getParentResponse=
								given()
										.pathParam("spsId",parentSPSID).
								when()
										.get(ENDPOINT_PARENT_GET).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getParentResponse.asString());
		
		// Add Child
		ExtractableResponse<Response> createAddChildResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(addChildPayload()).
								when()
										.post(ENDPOINT_ADDCHILD).
								then()
										.statusCode(201)
										.spec(addChildResponseValidator())
										.extract();	
		childSPSID=createAddChildResponse.path("spsId");	
		System.out.println(childSPSID);
		
		// Get Child
		ExtractableResponse<Response> getChildResponse=
								given()
										.pathParam("spsId",childSPSID).
								when()
										.get(ENDPOINT_GET_EDIT_DELETE_CHILD).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getChildResponse.asString());
		
		// Delete Child
		ExtractableResponse<Response> deleteChildUserResponse=
			    				given()
			    						.pathParam("spsId",childSPSID).
								when()
										.delete(ENDPOINT_GET_EDIT_DELETE_CHILD).
								then()
										.statusCode(200)
										.extract();	
		//childSPSID=createEditChildResponse.path("spsId");	
		//System.out.println(childSPSID);
		System.out.println(">>>>>>>>>>After Deleted Child<<<<<<<<<<");
		System.out.println(deleteChildUserResponse.asString());
		System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
	}		
	
 	private String getEmail()
 	{
		email="sps_dev"+String.valueOf(System.currentTimeMillis())+"@sample.com";
		return email;
 	}
	
	private Map<String, Object> createTeacherPayload()
	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",teacherfirstName);
		teacherRegistrationInfo.put("lastName",teacherlastName);
		teacherRegistrationInfo.put("email",getEmail());
		userType1.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType1);
		teacherRegistrationInfo.put("password",password);
		teacherRegistrationInfo.put("userName",email);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
	}
	
	private Map<String, Object> createParentPayload()
	{
		Map<String, Object> parentRegistrationInfo = new HashMap<String, Object>();
		parentRegistrationInfo.put("firstName",parentfirstName);
		parentRegistrationInfo.put("lastName",parentlastName);
		parentRegistrationInfo.put("email",getEmail());
		userType2.add("CONSUMER");
		parentRegistrationInfo.put("userType", userType2);
		parentRegistrationInfo.put("password",password);
		parentRegistrationInfo.put("userName",email);
		return parentRegistrationInfo;
	}
		
	private Map<String, Object> addChildPayload()
	{
		Map<String, Object> addChildInfo = new HashMap<String, Object>();
		addChildInfo.put("firstName",childfirstName);
		addChildInfo.put("lastName",childlastName);
		addChildInfo.put("birthYear",childbirthYear);
		addChildInfo.put("birthMonth",childbirthMonth);
		addChildInfo.put("birthDay",childbirthDay);
		addChildInfo.put("grade",childGrade);
		teacherIds.add(teacherSPSID);
		addChildInfo.put("teacherIds", teacherIds);
		parentIds.add(parentSPSID);
		addChildInfo.put("parentIds", parentIds);
		//readinglevelIds.add("100");
		//readinglevelIds.add("200");
		//addChildInfo.put("readingLevelIds", readinglevelIds);
		//interestIds.add("10");
		//addChildInfo.put("interestIds", interestIds);
		return addChildInfo;
	}
		
	private Map<String, Object> updateChildPayload()
	{
		Map<String, Object> updateChildInfo = new HashMap<String, Object>();
		updateChildInfo.put("firstName",changedchildfirstName);
		updateChildInfo.put("lastName",changedchildlastName);
		updateChildInfo.put("birthYear",changedchildbirthYear);
		updateChildInfo.put("birthMonth",changedchildbirthMonth);
		updateChildInfo.put("birthDay",changedchildbirthDay);
		updateChildInfo.put("grade",changedchildGrade);
		//teacherIds.add(teacherSPSID);
		updateChildInfo.put("teacherIds", teacherIds);
		//parentIds.add(parentSPSID);
		updateChildInfo.put("parentIds", parentIds);
		//readinglevelIds.add("100");
		//readinglevelIds.add("200");
		//updateChildInfo.put("readingLevelIds", readinglevelIds);
		//interestIds.add("10");
		//updateChildInfo.put("interestIds", interestIds);
		return updateChildInfo;
	}
				
	private ResponseSpecification createTeacherResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("userName",equalTo(email))
		//.expectBody("modifiedDate",equalTo(""))
		//.expectBody("registrationDate",equalTo(new Date()))
		.expectBody("schoolId",equalTo(schoolId))
		.expectBody("orgZip",is(not(empty())))
		.expectBody("userType",equalTo(userType1))
		.expectBody("isEducator",is(not(empty())))
		.expectBody("cac",is(not(empty())))
		.expectBody("cacId",is(not(empty())))
		.expectBody("isIdUsed",is(not(empty())))
		.expectBody("isEnabled",is(not(empty())))
		.expectBody("schoolUcn",is(not(empty())))
		.expectBody("email",equalTo(email))
		.expectBody("firstName",equalTo(teacherfirstName))
		.expectBody("lastName",equalTo(teacherlastName))
		//.expectBody("password",equalTo(password))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
		
	private ResponseSpecification createParentResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("userName",equalTo(email))
		.expectBody("userType",equalTo(userType2))
		.expectBody("email",equalTo(email))
		.expectBody("firstName",equalTo(parentfirstName))
		.expectBody("lastName",equalTo(parentlastName))
		//.expectBody("password",equalTo(password))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
		
	private ResponseSpecification addChildResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("birthYear",equalTo(childbirthYear))
		//.expectBody("modifiedDate",equalTo(""))
		//.expectBody("registrationDate",equalTo(new Date()))
		.expectBody("cac",is(not(empty())))
		.expectBody("firstName",equalTo(childfirstName))
		.expectBody("lastName",equalTo(childlastName))
		.expectBody("lastName",equalTo(childlastName))
		.expectBody("birthMonth",equalTo(childbirthMonth))
		.expectBody("birthDay",equalTo(childbirthDay))
		.expectBody("birthYear",equalTo(childbirthYear))
		.expectBody("grade",equalTo(childGrade))
		//.expectBody("readingLevelIds",equalTo(readinglevelIds))
		//.expectBody("interestIds",equalTo(interestIds))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
		
	private ResponseSpecification updateChildResponseValidator()
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("birthYear",equalTo(changedchildbirthYear))
		//.expectBody("modifiedDate",equalTo(""))
		//.expectBody("registrationDate",equalTo(new Date()))
		.expectBody("cac",is(not(empty())))
		.expectBody("firstName",equalTo(changedchildfirstName))
		.expectBody("lastName",equalTo(changedchildlastName))
		.expectBody("lastName",equalTo(changedchildlastName))
		.expectBody("birthMonth",equalTo(changedchildbirthMonth))
		.expectBody("birthDay",equalTo(changedchildbirthDay))
		.expectBody("birthYear",equalTo(changedchildbirthYear))
		.expectBody("grade",equalTo(changedchildGrade))
		//.expectBody("readingLevelIds",equalTo(readinglevelIds))
		//.expectBody("interestIds",equalTo(interestIds))
		.expectBody("spsId",is(not(empty())));
		return rspec.build();		
	}
}
