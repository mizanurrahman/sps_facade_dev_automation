package com.dev.sps.facade;

import static com.jayway.restassured.RestAssured.given;




//import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.dev.sps.facade.BaseTest;

import java.util.Map;

import static org.hamcrest.Matchers.*;

public class UserUpdateServicesTest extends BaseTest
{
	
	//Valid User and Password
		private String user ="devtestsample706@juno1.com";
		//private String password ="passed1";
		private boolean singleToken = true;
		private boolean userForWS = false;
		private boolean addCookie = true;
		private boolean addExpiration = true;
		
		private String firstName ="DevTest";
		private String lastName ="Sample";
		private String email ="devtestsample706@juno1.com";
		private List<String> userType=new ArrayList<String>();
		private String userName ="devtestsample706@juno1.com";
		private String schoolId ="11842396";
		
		//Update User (Password less than 7 characters)
		private String password1 ="passed";
		//Update User (Password only 7 characters)
		private String password2 ="passedx";
		//Update User (Password only 7 Numbers)
		private String password3 ="1234567";
		//Update User (Password more than 30 Alpha-Numeric Characters)
		private String password4 ="passpasspapasspasspapasspassp123";
		//Update User (Password 30 Alpha-Numeric Characters)
		private String password5 ="passpasspapasspasspapasspassp1";
		//Update User (Password 7 Alpha-Numeric Characters)
		private String password6 ="passed1";
		//Update User (Password Special Characters)
		private String password7 ="passed1>{/%]";
		
		//End Points
		private static final String ENDPOINT_USER_UPDATE="/spsuser/{userId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_USER_LOGIN="spsuser/login?clientId=SPSFacadeAPI";
		
		@Test
		public void updateUserPasswordWithLessThan7CharactersTest() 
		{	
			System.out.println("##################################################################");
			System.out.println("********** Update User Password Less Than 7 Characters  **********");
			System.out.println("##################################################################");
			
			String userId="1838020";
			ExtractableResponse<Response> updateUserPasswordLessThan7CharactersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordLessthan7CharactersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(400)
								.body("errorMessages",hasItem("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
								.body("errorStatus",equalTo("BUSINESS_FAILURE"))
								.extract();
			
			System.out.println(updateUserPasswordLessThan7CharactersResponse.asString());
				
		}

		@Test
		public void updateUserPasswordWithOnly7CharactersTest() 
		{	
			System.out.println("##################################################################");
			System.out.println("********** Update User Password with only 7 Characters  **********");
			System.out.println("##################################################################");
			
			String userId="1838020";
			
			ExtractableResponse<Response> updateUserPasswordWithOnly7CharactersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordWithOnly7CharactersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(400)
								.body("errorMessages",hasItem("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
								.body("errorStatus",equalTo("BUSINESS_FAILURE"))
								.extract();
			
			System.out.println(updateUserPasswordWithOnly7CharactersResponse.asString());
							
		}
		
		@Test
		public void updateUserPasswordWithOnly7NumbersTest() 
		{	
			System.out.println("##################################################################");
			System.out.println("********** Update User Password with only 7 Numbers  **********");
			System.out.println("##################################################################");
			
			String userId="1838020";
			
			ExtractableResponse<Response> updateUserPasswordWithOnly7NumbersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordWithOnly7NumbersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(400)
								.body("errorMessages",hasItem("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
								.body("errorStatus",equalTo("BUSINESS_FAILURE"))
								.extract();
			
			System.out.println(updateUserPasswordWithOnly7NumbersResponse.asString());
				
		}
		
		@Test
		public void updateUserPasswordWithMoreThan30AlphaNumericCharactersTest() 
		{	
			System.out.println("######################################################################################");
			System.out.println("********** Update User Password with More Than 30 Alpha-numeric Characters  **********");
			System.out.println("######################################################################################");
			
			String userId="1838020";
			
			ExtractableResponse<Response> updateUserPasswordWithMoreThan30AlphaNumericCharactersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordWithMoreThan30AlphaNumericCharactersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(400)
								.body("errorMessages",hasItems("Password can't exceed 30 characters.", "Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
								.body("errorStatus",equalTo("BUSINESS_FAILURE"))
								.extract();
			
			System.out.println(updateUserPasswordWithMoreThan30AlphaNumericCharactersResponse.asString());
				
		}
		
		@Test
		public void updateUserPasswordWith30AlphaNumericCharactersTest() 
		{	
			System.out.println("############################################################################");
			System.out.println("********** Update User Password with 30 Alpha-numeric Characters  **********");
			System.out.println("############################################################################");
			
			String userId="1838020";
			
			ExtractableResponse<Response> updateUserPasswordWith30AlphaNumericCharactersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordWith30AlphaNumericCharactersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(200)
								.body("ucn",equalTo("607957631"))
								.body("schoolUcn",equalTo("600202587"))
								.body("userName",equalTo("devtestsample706@juno1.com"))
								.extract();
			
			System.out.println("@@@@@@@@@@@@@@@ User Info for Updated Password (30 Alpha-Numeric Characters)" +updateUserPasswordWith30AlphaNumericCharactersResponse.asString());
			
			//Authentication with the updated password
			ExtractableResponse<Response> loginWithTheUpdatedPasswordResponse=
					given()
							.contentType("application/json")
							.body(createLogInWithUpdatedPasswordPayload()).
					when()
							.post(ENDPOINT_USER_LOGIN).
					then()
							.statusCode(200)
							//.body("message",equalTo("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							//.body("success",equalTo("false"))
							//.body("userId",equalTo("89601279"))
							.extract();
			
		System.out.println("@@@@@@@@@@@@@@@@@ Login with updated password (30 Alpha-Numeric Characters): " +loginWithTheUpdatedPasswordResponse.asString());
				
		}
		
		@Test
		public void updateUserPasswordWith7AlphaNumericCharactersTest() 
		{	
			System.out.println("############################################################################");
			System.out.println("********** Update User Password with 7 Alpha-numeric Characters  **********");
			System.out.println("############################################################################");
			
			String userId="1838020";
			
			ExtractableResponse<Response> updateUserPasswordWith7AlphaNumericCharactersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordWith7AlphaNumericCharactersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(200)
								.body("ucn",equalTo("607957631"))
								.body("schoolUcn",equalTo("600202587"))
								.body("userName",equalTo("devtestsample706@juno1.com"))
								.extract();
			
			System.out.println("@@@@@@@@@@@@@@@ User Info for Updated Password (7 Alpha-Numeric Characters)" +updateUserPasswordWith7AlphaNumericCharactersResponse.asString());
			
			//Authentication with the updated password
			ExtractableResponse<Response> loginWithTheUpdatedPassword7AlphaNumericCharactersResponse=
					given()
							.contentType("application/json")
							.body(createLogInWithUpdatedPassword7AlphaNumericCharactersPayload()).
					when()
							.post(ENDPOINT_USER_LOGIN).
					then()
							.statusCode(200)
							//.body("message",equalTo("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							//.body("success",equalTo("false"))
							//.body("userId",equalTo("89601279"))
							.extract();
			
		System.out.println("@@@@@@@@@@@@@@@@@ Login with updated password (7 Alpha-Numeric Characters): " +loginWithTheUpdatedPassword7AlphaNumericCharactersResponse.asString());
				
		}
		
		@Test
		public void updateUserPasswordWithSpecialCharactersTest()

		{	
			System.out.println("###################################################################");
			System.out.println("********** Update User Password with Special Characters  **********");
			System.out.println("###################################################################");
			
			String userId="1838020";
			
			ExtractableResponse<Response> updateUserPasswordWithSpecialCharactersResponse=
						given()
								.contentType("application/json")
								.body(updateUserPasswordWithSpecialCharactersPayload()).
						when()
								.put(ENDPOINT_USER_UPDATE, userId).
						then()
								.statusCode(200)
								.body("ucn",equalTo("607957631"))
								.body("schoolUcn",equalTo("600202587"))
								.body("userName",equalTo("devtestsample706@juno1.com"))
								.extract();
			
			System.out.println("@@@@@@@@@@@@@@@ User Info for Updated Password (Special Characters)" +updateUserPasswordWithSpecialCharactersResponse.asString());
			
			//Authentication with the updated password
			ExtractableResponse<Response> loginWithTheUpdatedPasswordWithSpecialCharactersResponse=
					given()
							.contentType("application/json")
							.body(createLogInWithUpdatedPasswordWithSpecialCharactersPayload()).
					when()
							.post(ENDPOINT_USER_LOGIN).
					then()
							.statusCode(200)
							//.body("message",equalTo("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							//.body("success",equalTo("false"))
							//.body("userId",equalTo("89601279"))
							.extract();
			
		System.out.println("@@@@@@@@@@@@@@@@@ Login with updated password (Special Characters): " +loginWithTheUpdatedPasswordWithSpecialCharactersResponse.asString());
				
		}

		private Map<String, Object> updateUserPasswordLessthan7CharactersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password1);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}
		
		private Map<String, Object> updateUserPasswordWithOnly7CharactersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password2);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}
		
		private Map<String, Object> updateUserPasswordWithOnly7NumbersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password3);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}
		
		private Map<String, Object> updateUserPasswordWithMoreThan30AlphaNumericCharactersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password4);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}
		
		private Map<String, Object> updateUserPasswordWith30AlphaNumericCharactersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password5);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}

		private Map<String, Object> createLogInWithUpdatedPasswordPayload()
		{
			Map<String, Object> createLogInInfo = new HashMap<String, Object>();
			createLogInInfo.put("user",user );
			createLogInInfo.put("password",password5);
			createLogInInfo.put("singleToken",singleToken);
			createLogInInfo.put("userForWS", userForWS);
			createLogInInfo.put("addCookie",addCookie);
			createLogInInfo.put("addExpiration",addExpiration);
			return createLogInInfo;

		}
		
		private Map<String, Object> updateUserPasswordWith7AlphaNumericCharactersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password6);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}

		private Map<String, Object> createLogInWithUpdatedPassword7AlphaNumericCharactersPayload()
		{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password6);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;

		}
		
		private Map<String, Object> updateUserPasswordWithSpecialCharactersPayload()
		
		{
			Map<String, Object> userUpdateInfo = new HashMap<String, Object>();
			userUpdateInfo.put("firstName",firstName);
			userUpdateInfo.put("lastName",lastName);
			userUpdateInfo.put("email",email);
			userType.add("TEACHER");
			userUpdateInfo.put("userType", userType);
			userUpdateInfo.put("password",password7);
			userUpdateInfo.put("userName",userName);
			userUpdateInfo.put("schoolId",schoolId);
			return userUpdateInfo;
			
		}

		private Map<String, Object> createLogInWithUpdatedPasswordWithSpecialCharactersPayload()
		{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password7);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;

		}
}
