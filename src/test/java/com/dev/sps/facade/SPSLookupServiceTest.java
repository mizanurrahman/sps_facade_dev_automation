package com.dev.sps.facade;

/***************************************************************************************************************************
 ***  @@Author
 ***  Mohammed Rahman
 ***  This Tests Will Be Using To Test Facade Lookup Services.
 ***************************************************************************************************************************/

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

//import java.security.SecureRandom;
//import java.util.HashMap;
//import java.util.Map;
//import java.util.UUID;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

public class SPSLookupServiceTest extends BaseTest
{

	//End Points
	private static final String ENDPOINT_ALLSTATE="/lookup/states";	
	private static final String ENDPOINT_ALLCITY="/lookup/states/NY/cities";
	private static final String ENDPOINT_ALLCOUNTRY="/lookup/countries";
	private static final String ENDPOINT_COUNTRYSCHOOLS="/lookup/countries/BD/school";
	private static final String ENDPOINT_ALLTEACHERS="/lookup/sps/school/{schoolId}/educator?fat=&"; 
	//private static final String ENDPOINT_ALTERNATETEACHER="/lookup/sps/school/{schoolId}/educator/alternate";
	private static final String ENDPOINT_SCHOOLBYZIPCODE="/lookup/zipcode/{zipCode}/school?forceClient=&";
	private static final String ENDPOINT_SCHOOLBYCITYSATATE="/lookup/states/NY/cities/Andes/school?forceClient=&";
	private static final String ENDPOINT_EXPOSECONSUMER="/lookup/user?type=*&userType=consumer&";
	private static final String ENDPOINT_EXPOSEEDUCATOR="/lookup/user?type=*&userType=educator&";
	private static final String ENDPOINT_PRIMARYSCHOOLADDRESS="/lookup/user/{userId}/organization";
	private static final String ENDPOINT_EXSISTINGSHIPPINGADDRESS="/spsaddressbook/{userId}/";
	private static final String ENDPOINT_ONLYTEACHERS="/lookup/sps/school/{schoolId}/educator?fat=true&";
	private static final String ENDPOINT_ALLGRADECLASSSIZE="/spsuser/{userId}/gradeclasssize";
	private static final String ENDPOINT_CRIDTCARDSWALLET="/spsuser/{userId}/cybersource/creditcard";
	private static final String ENDPOINT_EXISTINGASSOCIATEDSOCIALACCOUNTS="/spssocial/{userId}";
	private static final String ENDPOINT_CHILDUSERS="/spschilduser/{childUserId}";
	
  @Test
	public void getAllStateTest()
	{	
		System.out.println("########################################################");
		System.out.println("******************** Get All States ********************");
		System.out.println("########################################################");
		ExtractableResponse<Response> getAllStateResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ALLSTATE).
								then()
										.statusCode(200)
										.spec(getAllStateResponseValidator())
										.extract();	
		System.out.println(getAllStateResponse.asString());
	}
  
  @Test
	public void getAllCityTest() 
	{
		System.out.println("#################################################################");
		System.out.println("******************** Get All Cities by State ********************");
		System.out.println("#################################################################");				
		ExtractableResponse<Response> getAllCityResponse=
								given()
										
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ALLCITY).
								then()
										.statusCode(200)
										.spec(getAllCityResponseValidator())
										.extract();	
		System.out.println(getAllCityResponse.asString());
	}
  
  @Test
	public void getAllCountryTest() 
	{
		System.out.println("###########################################################");
		System.out.println("******************** Get All Countries ********************");
		System.out.println("###########################################################");
		
		ExtractableResponse<Response> getAllCountryResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ALLCOUNTRY).
						        then()
						        		.statusCode(200)
						        		.spec(getAllCountryResponseValidator())
						        		.extract();		
		System.out.println(getAllCountryResponse.asString());
	}
  
  @Test
	public void getCountrySchoolsTest() 
	{
		System.out.println("####################################################################");
		System.out.println("******************** Get All Schools by Country ********************");
		System.out.println("####################################################################");
		
		ExtractableResponse<Response> getCountrySchoolsResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_COUNTRYSCHOOLS).
								then()
										.statusCode(200)
										.spec(getCountrySchoolsResponseValidator())
										.extract();		
		System.out.println(getCountrySchoolsResponse.asString());
	}
  
  @Test
	public void getAllTeachersTest()
	{
		System.out.println("####################################################################################");
		System.out.println("******************** Get All Teachers not logged in Readig Clubs********************");
		System.out.println("####################################################################################");
		
		String schoolId="148551";
		ExtractableResponse<Response> getAllTeachersResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ALLTEACHERS, schoolId).
								then()
										.statusCode(200)
										.spec(getAllTeachersResponseValidator())
										.extract();				
		System.out.println(getAllTeachersResponse.asString());
	}
  
 /*@Test
	public void getAlternateTeacherTest() 
	{
		System.out.println("################################################################################");
		System.out.println("******************** Get All Alternate Teachers in a School ********************");
		System.out.println("################################################################################");	
		
		String schoolId="1901078";
		
		ExtractableResponse<Response> getAlternateTeacherResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ALTERNATETEACHER, schoolId).
								then()
										.statusCode(200)
										//.spec(getAlternateTeacherResponseValidator())
										.extract();				
		System.out.println(getAlternateTeacherResponse.asString());
			
	}*/
  
  @Test
	public void getSchoolByZipcodeTest()
	{
		System.out.println("#################################################################");
		System.out.println("******************** Get Schools by Zip Code ********************");
		System.out.println("#################################################################");
		
		String zipCode="11431";
		ExtractableResponse<Response> getSchoolByZipcode=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_SCHOOLBYZIPCODE, zipCode).
								then()
										.statusCode(200)
										.spec(getSchoolByZipcodeResponseValidator())
										.extract();		
		System.out.println(getSchoolByZipcode.asString());
	}
  
  @Test
	public void getSchoolByCityStateTest() 
	{
		System.out.println("#######################################################################");
		System.out.println("******************** Get Schools by State and City ********************");
		System.out.println("#######################################################################");	
		
		ExtractableResponse<Response> getSchoolByCityState=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_SCHOOLBYCITYSATATE).
								then()
										.statusCode(200)
										.spec(getSchoolByCityStateResponseValidator())
										.extract();		
		System.out.println(getSchoolByCityState.asString());
	}
  
  @Test
	public void getExposeConsumerTest()
	{
		System.out.println("##############################################################");
		System.out.println("******************** Expose Educator Data ********************");
		System.out.println("##############################################################");	
		
		ExtractableResponse<Response> getExsposeConsumerResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_EXPOSECONSUMER).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getExsposeConsumerResponse.asString());
	}
  
  @Test
	public void getExposeEducatorTest()
	{
		System.out.println("##############################################################");
		System.out.println("******************** Expose Consumer Data ********************");
		System.out.println("##############################################################");	
		
		ExtractableResponse<Response> getExposeEducatorResponse=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_EXPOSEEDUCATOR).
								then()
										.statusCode(200)
										.extract();		
		System.out.println(getExposeEducatorResponse.asString());
	}
  
  @Test
	public void getPrimarySchoolAddressTest() 
	{
		System.out.println("####################################################################");
		System.out.println("******************** Get Primary School Address ********************");
		System.out.println("####################################################################");
		
		String userId="84305751";
		ExtractableResponse<Response> getPrimarySchoolAddress=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_PRIMARYSCHOOLADDRESS, userId).
								then()
										.statusCode(200)
										.spec(getPrimarySchoolAddressResponseValidator())
										.extract();		
		System.out.println(getPrimarySchoolAddress.asString());
	}
  
  @Test
	public void getExistingShippingAddressTest() 
	{
		System.out.println("#######################################################################");
		System.out.println("******************** Get Existing Shipping Address ********************");
		System.out.println("#######################################################################");
		
		String userId="84780649";
		ExtractableResponse<Response> getExistingShippingAddress=
									
								given()
										.log().all()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_EXSISTINGSHIPPINGADDRESS, userId).
								then()
										.statusCode(200)
										.spec(getExistingShippingAddressResponseValidator())
										.extract();		
		System.out.println(getExistingShippingAddress.asString());
	}
  
  @Test
	public void getOnlyTeachersTest()
	{
		System.out.println("###################################################################################");
		System.out.println("******************** Get Only Teachers Logged in Redading Club ********************");
		System.out.println("###################################################################################");
		
		String schoolId="411785";
		ExtractableResponse<Response> getOnlyTeachers=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ONLYTEACHERS, schoolId).
								then()
										.statusCode(200)
										.spec(getOnlyTeachersResponseValidator())
										.extract();				
		System.out.println(getOnlyTeachers.asString());
	}
  
  @Test
	public void getAllGradeClassSizeTest()
	{
		System.out.println("########################################################################");
		System.out.println("******************** List All Grades and Class Sizes ********************");
		System.out.println("########################################################################");
		
		String userId="84907629";
		ExtractableResponse<Response> getAllGradeClassSize=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_ALLGRADECLASSSIZE, userId).
								then()
										.statusCode(200)
										.spec(getAllGradeClassSizeResponseValidator())
										.extract();				
		System.out.println(getAllGradeClassSize.asString());
	}
  
  @Test
	public void getCreditCardsWalletTest() 
	{
		System.out.println("################################################################");
		System.out.println("******************** Get Credit Card Wallet ********************");
		System.out.println("################################################################");
		
		String userId="84780649";
		
		
		ExtractableResponse<Response> getCreditCardsWallet=
								given()
										.log().all()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_CRIDTCARDSWALLET, userId).
								then()
										.statusCode(200)
										.spec(getCreditCardWalletResponseValidator())
										.extract();		
		System.out.println(getCreditCardsWallet.asString());
	}
  
  @Test
	public void getExistingAssociatedSocialAccountsTest() 
	{
		System.out.println("#######################################################################################");
		System.out.println("******************** Get Existing Associated Social Media Accounts ********************");
		System.out.println("#######################################################################################");
		
		String userId="84780649";
		ExtractableResponse<Response> getExistingAssociatedSocialAccounts=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_EXISTINGASSOCIATEDSOCIALACCOUNTS, userId).
								then()
										.statusCode(200)
										.spec(getExistingAssociatedSocialAccountsResponseValidator())
										.extract();		
		System.out.println(getExistingAssociatedSocialAccounts.asString());
	}
  
  @Test
	public void getChildUsersTest() 
	{
		System.out.println("########################################################");
		System.out.println("******************** Get Child User ********************");
		System.out.println("########################################################");	
		
		String childUserId="156551";
		ExtractableResponse<Response> getChildUsers=
								given()
										.param("clientId","SPSFacadeAPI").
								when()
										.get(ENDPOINT_CHILDUSERS, childUserId).
								then()
										.statusCode(200)
										.spec(getChildUsersResponseValidator())
										.extract();		
		System.out.println(getChildUsers.asString());
	}
  
  	private ResponseSpecification createEnumResponseValidator(String[][] keyvalues )
	{
		ResponseSpecBuilder respec=new ResponseSpecBuilder()
			.expectStatusCode(200)
			.expectContentType(ContentType.JSON);
			addKeyValueValidations(respec,keyvalues);
			return respec.build();
	}
  
  	private static void addKeyValueValidations(ResponseSpecBuilder specBuilder,	String[][] keyValues) 
	{
		specBuilder.expectBody("size()", equalTo(keyValues.length));
		for (int i = 0; i < keyValues.length; i++) 
		{
			specBuilder.expectBody("get(" + i + ").key",
			equalTo(keyValues[i][0]));
			specBuilder.expectBody("get(" + i + ").name",
			equalTo(keyValues[i][1]));
		}
	}
  	
  	private ResponseSpecification getAllStateResponseValidator()
	{
  		final String[][] keyValues = 
		{ 
			{ "AK", "AK - Alaska" }, { "AL", "AL - Alabama" }, { "AR", "AR - Arkansas" },
			{ "AS", "AS - American Samoa" }, { "AZ", "AZ - Arizona" },{"CA","CA - California" }, { "CO", "CO - Colorado" },
			{ "CT", "CT - Connecticut" }, { "DC", "DC - District of Columbia" }, { "DE", "DE - Delaware" }, 
			{ "FL", "FL - Florida" }, { "FM", "FM - Micronesia" }, { "GA", "GA - Georgia" }, { "GU", "GU - Guam" },
			{ "HI", "HI - Hawaii" }, { "IA", "IA - Iowa" }, { "ID", "ID - Idaho" }, { "IL", "IL - Illinois" }, 
			{ "IN", "IN - Indiana" }, { "KS", "KS - Kansas" }, { "KY", "KY - Kentucky" }, { "LA", "LA - Louisiana"}, 
			{ "MA", "MA - Massachusetts" }, { "MD", "MD - Maryland" }, { "ME", "ME - Maine" }, { "MH", "MH - Marshall Isl"},
			{ "MI", "MI - Michigan" }, { "MN", "MN - Minnesota" }, { "MO", "MO - Missouri" }, { "MP", "MP - Northern Mariana Isls" },
			{ "MS", "MS - Mississippi" }, { "MT", "MT - Montana" }, { "NC", "NC - North Carolina" }, { "ND", "ND - North Dakota" },
			{ "NE", "NE - Nebraska" }, { "NH", "NH - New Hampshire" }, { "NJ", "NJ - New Jersey" }, { "NM", "NM - New Mexico" },
			{ "NV", "NV - Nevada" }, { "NY", "NY - New York" }, { "OH", "OH - Ohio" }, { "OK", "OK - Oklahoma" }, { "OR", "OR - Oregon" },
			{ "PA", "PA - Pennsylvania" }, { "PR", "PR - Puerto Rico" }, { "PW", "PW - Palau" }, { "RI", "RI - Rhode Island" }, 
			{ "SC", "SC - South Carolina" }, { "SD", "SD - South Dakota" }, { "TN", "TN - Tennessee" }, { "TX", "TX - Texas" }, 
			{ "UM", "UM - US Minor Outlying Isls" }, { "UT", "UT - Utah" }, { "VA", "VA - Virginia" }, { "VI", "VI - Virgin Islands" },
			{ "VT", "VT - Vermont" }, { "WA", "WA - Washington" }, { "WI", "WI - Wisconsin" }, { "WV", "WV - West Virginia" }, { "WY", "WY - Wyoming" }
		};

  		return createEnumResponseValidator(keyValues);
	}
  	
  	private ResponseSpecification getAllCityResponseValidator()
	{
  		final String[][] keyValues = 
		{ 
  				{ "Accord", "Accord" }, { "Acra", "Acra" }, { "Adams", "Adams" }, { "Adams Basin", "Adams Basin" }, { "Adams Center", "Adams Center" }, 
  			    { "Addisleigh Park", "Addisleigh Park" }, { "Addison", "Addison" }, { "Adirondack", "Adirondack" }, { "Afton", "Afton" }, { "Airmont", "Airmont" },
  			    { "Akron", "Akron" }, { "Akwesasne", "Akwesasne" }, { "Alabama", "Alabama" }, { "Albany", "Albany" }, { "Albertson", "Albertson" }, 
  			    { "Albion", "Albion" }, { "Alcove", "Alcove" }, { "Alden", "Alden" }, { "Alden Manor", "Alden Manor" }, { "Alder Creek", "Alder Creek" },
  			    { "Alexander", "Alexander" }, { "Alexandria Bay", "Alexandria Bay" }, { "Alfred", "Alfred" }, { "Alfred Station", "Alfred Station" },
  			    { "Allegany", "Allegany" }, { "Allentown", "Allentown" }, { "Alma", "Alma" }, { "Almond", "Almond" }, { "Alpine", "Alpine" }, { "Alplaus", "Alplaus" },
  			    { "Altamont", "Altamont" }, { "Altmar", "Altmar" }, { "Alton", "Alton" }, { "Altona", "Altona" }, { "Amagansett", "Amagansett" }, { "Amawalk", "Amawalk" },
  			    { "Amenia", "Amenia" }, { "Ames", "Ames" }, { "Amherst", "Amherst" }, { "Amity Harbor", "Amity Harbor" }, { "Amityville", "Amityville" }, 
  			    { "Amsterdam", "Amsterdam" }, { "Ancram", "Ancram" }, { "Ancramdale", "Ancramdale" }, { "Andes", "Andes" }, { "Andover", "Andover" },
  			    { "Angelica", "Angelica" }, { "Angola", "Angola" }, { "Annandale", "Annandale" }, { "Annandale On Hudson", "Annandale On Hudson" }, { "Antwerp", "Antwerp" },
  			    { "Apalachin", "Apalachin" }, { "Appleton", "Appleton" }, { "Apulia Station", "Apulia Station" }, { "Aquebogue", "Aquebogue" }, { "Arcade", "Arcade" },
  			    { "Arden", "Arden" }, { "Ardsley", "Ardsley" }, { "Ardsley On Hudson", "Ardsley On Hudson" }, { "Argyle", "Argyle" }, { "Arkport", "Arkport" },
  			    { "Arkville", "Arkville" }, { "Arlington", "Arlington" }, { "Armonk", "Armonk" }, { "Arverne", "Arverne" }, { "Ashland", "Ashland" }, { "Ashville", "Ashville" },
  			    { "Astoria", "Astoria" }, { "Athens", "Athens" }, { "Athol", "Athol" }, { "Athol Springs", "Athol Springs" }, { "Atlanta", "Atlanta" }, 
  			    { "Atlantic Beach", "Atlantic Beach" }, { "Attica", "Attica" }, { "Au Sable Chasm", "Au Sable Chasm" }, { "Au Sable Forks", "Au Sable Forks" },
  			    { "Auburn", "Auburn" }, { "Auburndale", "Auburndale" }, { "Auriesville", "Auriesville" }, { "Aurora", "Aurora" }, { "Ausable Chasm", "Ausable Chasm" },
  			    { "Austerlitz", "Austerlitz" }, { "Ava", "Ava" }, { "Averill Park", "Averill Park" }, { "Avoca", "Avoca" }, { "Avon", "Avon" }, { "Babylon", "Babylon" },
  			    { "Bainbridge", "Bainbridge" }, { "Baiting Hollow", "Baiting Hollow" }, { "Bakers Mills", "Bakers Mills" }, { "Baldwin", "Baldwin" },
  			    { "Baldwin Place", "Baldwin Place" }, { "Baldwinsville", "Baldwinsville" }, { "Ballston Lake", "Ballston Lake" }, { "Ballston Spa", "Ballston Spa" },
  			    { "Balmat", "Balmat" }, { "Bangall", "Bangall" }, { "Bangor", "Bangor" }, { "Bardonia", "Bardonia" }, { "Barker", "Barker" },
  			    { "Barnes Corners", "Barnes Corners" }, { "Barneveld", "Barneveld" }, { "Barrytown", "Barrytown" }, { "Barryville", "Barryville" },
  			    { "Barton", "Barton" }, { "Basom", "Basom" }, { "Batavia", "Batavia" }, { "Bath", "Bath" }, { "Bay Shore", "Bay Shore" }, { "Bayberry", "Bayberry" },
  			    { "Bayport", "Bayport" }, { "Bayside", "Bayside" }, { "Bayside Hills", "Bayside Hills" }, { "Bayville", "Bayville" }, { "Beacon", "Beacon" }, 
  			    { "Bear Mountain", "Bear Mountain" }, { "Bearsville", "Bearsville" }, { "Beaver Dams", "Beaver Dams" }, { "Beaver Falls", "Beaver Falls" }, 
  			    { "Beaver River", "Beaver River" }, { "Bedford", "Bedford" }, { "Bedford Corners", "Bedford Corners" }, { "Bedford Hills", "Bedford Hills" }, 
  			    { "Beechhurst", "Beechhurst" }, { "Belfast", "Belfast" }, { "Belle Harbor", "Belle Harbor" }, { "Bellerose", "Bellerose" }, 
  			    { "Bellerose Manor", "Bellerose Manor" }, { "Bellerose Village", "Bellerose Village" }, { "Belleville", "Belleville" }, { "Bellmore", "Bellmore" },
  			    { "Bellona", "Bellona" }, { "Bellport", "Bellport" }, { "Bellvale", "Bellvale" }, { "Belmont", "Belmont" }, { "Bemus Point", "Bemus Point" },
  			    { "Bergen", "Bergen" }, { "Berkshire", "Berkshire" }, { "Berlin", "Berlin" }, { "Berne", "Berne" }, { "Bernhards Bay", "Bernhards Bay" }, 
  			    { "Bethel", "Bethel" }, { "Bethpage", "Bethpage" }, { "Bible School Park", "Bible School Park" }, { "Big Flats", "Big Flats" },
  			    { "Big Indian", "Big Indian" }, { "Billings", "Billings" }, { "Binghamton", "Binghamton" }, { "Black Creek", "Black Creek" },
  			    { "Black River", "Black River" }, { "Blasdell", "Blasdell" }, { "Blauvelt", "Blauvelt" }, { "Bliss", "Bliss" }, { "Blodgett Mills", "Blodgett Mills" },
  			    { "Bloomfield", "Bloomfield" }, { "Blooming Grove", "Blooming Grove" }, { "Bloomingburg", "Bloomingburg" }, { "Bloomingdale", "Bloomingdale" },
  			    { "Bloomington", "Bloomington" }, { "Bloomville", "Bloomville" }, { "Blossvale", "Blossvale" }, { "Blue Mountain Lake", "Blue Mountain Lake" },
  			    { "Blue Point", "Blue Point" }, { "Bluff Point", "Bluff Point" }, { "Bohemia", "Bohemia" }, { "Boiceville", "Boiceville" }, { "Bolivar", "Bolivar" },
  			    { "Bolton Landing", "Bolton Landing" }, { "Bombay", "Bombay" }, { "Boonville", "Boonville" }, { "Boston", "Boston" }, { "Bouckville", "Bouckville" },
  			    { "Bovina Center", "Bovina Center" }, { "Bowmansville", "Bowmansville" }, { "Bradford", "Bradford" }, { "Brainard", "Brainard" }, 
  			    { "Brainardsville", "Brainardsville" }, { "Branchport", "Branchport" }, { "Brant", "Brant" }, { "Brant Lake", "Brant Lake" },
  			    { "Brantingham", "Brantingham" }, { "Brasher Falls", "Brasher Falls" }, { "Breesport", "Breesport" }, { "Breezy Point", "Breezy Point" },
  			    { "Brentwood", "Brentwood" }, { "Brewerton", "Brewerton" }, { "Brewster", "Brewster" }, { "Briarcliff", "Briarcliff" }, 
  			    { "Briarcliff Manor", "Briarcliff Manor" }, { "Briarwood", "Briarwood" }, { "Bridgehampton", "Bridgehampton" }, { "Bridgeport", "Bridgeport" },
  			    { "Bridgewater", "Bridgewater" }, { "Brier Hill", "Brier Hill" }, { "Brighton", "Brighton" }, { "Brightwaters", "Brightwaters" }, { "Brisben", "Brisben" },
  			    { "Broad Channel", "Broad Channel" }, { "Broadalbin", "Broadalbin" }, { "Brockport", "Brockport" }, { "Brocton", "Brocton" }, { "Bronx", "Bronx" },
  			    { "Bronxville", "Bronxville" }, { "Brookfield", "Brookfield" }, { "Brookhaven", "Brookhaven" }, { "Brooklyn", "Brooklyn" },
  			    { "Brooklyn Heights", "Brooklyn Heights" }, { "Brooktondale", "Brooktondale" }, { "Brookview", "Brookview" }, { "Brownville", "Brownville" }, 
  			    { "Brushton", "Brushton" }, { "Buchanan", "Buchanan" }, { "Buffalo", "Buffalo" }, { "Bullville", "Bullville" }, { "Burdett", "Burdett" },
  			    { "Burke", "Burke" }, { "Burlingham", "Burlingham" }, { "Burlington Flats", "Burlington Flats" }, { "Burnt Hills", "Burnt Hills" }, { "Burt", "Burt" },
  			    { "Buskirk", "Buskirk" }, { "Byron", "Byron" }, { "Cadosia", "Cadosia" }, { "Cadyville", "Cadyville" }, { "Cairo", "Cairo" }, { "Calcium", "Calcium" },
  			    { "Caledonia", "Caledonia" }, { "Callicoon", "Callicoon" }, { "Callicoon Center", "Callicoon Center" }, { "Calverton", "Calverton" }, 
  			    { "Cambria Heights", "Cambria Heights" }, { "Cambridge", "Cambridge" }, { "Camden", "Camden" }, { "Cameron", "Cameron" }, { "Cameron Mills", "Cameron Mills" },
  			    { "Camillus", "Camillus" }, { "Campbell", "Campbell" }, { "Campbell Hall", "Campbell Hall" }, { "Canaan", "Canaan" }, { "Canajoharie", "Canajoharie" },
  			    { "Canandaigua", "Canandaigua" }, { "Canaseraga", "Canaseraga" }, { "Canastota", "Canastota" }, { "Candor", "Candor" }, { "Caneadea", "Caneadea" },
  			    { "Canisteo", "Canisteo" }, { "Canton", "Canton" }, { "Cape Vincent", "Cape Vincent" }, { "Captree Island", "Captree Island" }, 
  			    { "Carle Place", "Carle Place" }, { "Carlisle", "Carlisle" }, { "Carmel", "Carmel" }, { "Caroga Lake", "Caroga Lake" }, { "Carthage", "Carthage" },
  			    { "Cassadaga", "Cassadaga" }, { "Cassville", "Cassville" }, { "Castile", "Castile" }, { "Castle Creek", "Castle Creek" }, { "Castle Point", "Castle Point" },
  			    { "Castleton", "Castleton" }, { "Castleton On Hudson", "Castleton On Hudson" }, { "Castorland", "Castorland" }, { "Cato", "Cato" }, { "Catskill", "Catskill" },
  			    { "Cattaraugus", "Cattaraugus" }, { "Cayuga", "Cayuga" }, { "Cayuta", "Cayuta" }, { "Cazenovia", "Cazenovia" }, { "Cedarhurst", "Cedarhurst" },
  			    { "Celoron", "Celoron" }, { "Cementon", "Cementon" }, { "Center Moriches", "Center Moriches" }, { "Centereach", "Centereach" }, 
  			    { "Centerport", "Centerport" }, { "Centerville", "Centerville" }, { "Central Bridge", "Central Bridge" }, { "Central Islip", "Central Islip" },
  			    { "Central Square", "Central Square" }, { "Central Valley", "Central Valley" }, { "Ceres", "Ceres" }, { "Chadwicks", "Chadwicks" },
  			    { "Chaffee", "Chaffee" }, { "Champlain", "Champlain" }, { "Chappaqua", "Chappaqua" }, { "Charlotteville", "Charlotteville" }, 
  			    { "Charlton", "Charlton" }, { "Chase Mills", "Chase Mills" }, { "Chateaugay", "Chateaugay" }, { "Chatham", "Chatham" }, { "Chaumont", "Chaumont" },
  			    { "Chautauqua", "Chautauqua" }, { "Chazy", "Chazy" }, { "Cheektowaga", "Cheektowaga" }, { "Chelsea", "Chelsea" }, { "Chemung", "Chemung" },
  			    { "Chenango Bridge", "Chenango Bridge" }, { "Chenango Forks", "Chenango Forks" }, { "Cherry Creek", "Cherry Creek" }, { "Cherry Grove", "Cherry Grove" },
  			    { "Cherry Plain", "Cherry Plain" }, { "Cherry Valley", "Cherry Valley" }, { "Chester", "Chester" }, { "Chestertown", "Chestertown" },
  			    { "Chestnut Ridge", "Chestnut Ridge" }, { "Chichester", "Chichester" }, { "Childwold", "Childwold" }, { "Chippewa Bay", "Chippewa Bay" },
  			    { "Chittenango", "Chittenango" }, { "Churchville", "Churchville" }, { "Churubusco", "Churubusco" }, { "Cicero", "Cicero" }, 
  			    { "Cincinnatus", "Cincinnatus" }, { "Circleville", "Circleville" }, { "Clarence", "Clarence" }, { "Clarence Center", "Clarence Center" }, 
  			    { "Clarendon", "Clarendon" }, { "Clark Mills", "Clark Mills" }, { "Clarkson", "Clarkson" }, { "Clarksville", "Clarksville" },
  			    { "Claryville", "Claryville" }, { "Claverack", "Claverack" }, { "Clay", "Clay" }, { "Clayton", "Clayton" }, { "Clayville", "Clayville" }, 
  			    { "Clemons", "Clemons" }, { "Cleveland", "Cleveland" }, { "Cleverdale", "Cleverdale" }, { "Clifton", "Clifton" }, { "Clifton Park", "Clifton Park" },
  			    { "Clifton Springs", "Clifton Springs" }, { "Climax", "Climax" }, { "Clinton", "Clinton" }, { "Clinton Corners", "Clinton Corners" }, 
  			    { "Clintondale", "Clintondale" }, { "Clintonville", "Clintonville" }, { "Clockville", "Clockville" }, { "Clyde", "Clyde" },
  			    { "Clymer", "Clymer" }, { "Cobleskill", "Cobleskill" }, { "Cochecton", "Cochecton" }, { "Cochecton Center", "Cochecton Center" },
  			    { "Coeymans", "Coeymans" }, { "Coeymans Hollow", "Coeymans Hollow" }, { "Cohocton", "Cohocton" }, { "Cohoes", "Cohoes" }, { "Cold Brook", "Cold Brook" },
  			    { "Cold Spring", "Cold Spring" }, { "Cold Spring Harbor", "Cold Spring Harbor" }, { "Colden", "Colden" }, { "College Point", "College Point" },
  			    { "Colliersville", "Colliersville" }, { "Collins", "Collins" }, { "Collins Center", "Collins Center" }, { "Colonie", "Colonie" }, 
  			    { "Colton", "Colton" }, { "Columbiaville", "Columbiaville" }, { "Commack", "Commack" }, { "Comstock", "Comstock" }, { "Concord", "Concord" },
  			    { "Conesus", "Conesus" }, { "Conewango Valley", "Conewango Valley" }, { "Congers", "Congers" }, { "Conklin", "Conklin" }, { "Connelly", "Connelly" },
  			    { "Constable", "Constable" }, { "Constableville", "Constableville" }, { "Constantia", "Constantia" }, { "Coopers Plains", "Coopers Plains" },
  			    { "Cooperstown", "Cooperstown" }, { "Copake", "Copake" }, { "Copake Falls", "Copake Falls" }, { "Copenhagen", "Copenhagen" }, { "Copiague", "Copiague" },
  			    { "Coram", "Coram" }, { "Corbettsville", "Corbettsville" }, { "Corfu", "Corfu" }, { "Corinth", "Corinth" }, { "Corning", "Corning" }, { "Cornwall", "Cornwall" },
  			    { "Cornwall On Hudson", "Cornwall On Hudson" }, { "Cornwallville", "Cornwallville" }, { "Corona", "Corona" }, { "Cortland", "Cortland" }, 
  			    { "Cortlandt Manor", "Cortlandt Manor" }, { "Cossayuna", "Cossayuna" }, { "Cottekill", "Cottekill" }, { "Cowlesville", "Cowlesville" },
  			    { "Coxsackie", "Coxsackie" }, { "Cragsmoor", "Cragsmoor" }, { "Cranberry Lake", "Cranberry Lake" }, { "Craryville", "Craryville" },
  			    { "Crittenden", "Crittenden" }, { "Croghan", "Croghan" }, { "Crompond", "Crompond" }, { "Cropseyville", "Cropseyville" }, { "Cross River", "Cross River" },
  			    { "Croton Falls", "Croton Falls" }, { "Croton On Hudson", "Croton On Hudson" }, { "Crown Point", "Crown Point" }, { "Crugers", "Crugers" },
  			    { "Cuba", "Cuba" }, { "Cuddebackville", "Cuddebackville" }, { "Cutchogue", "Cutchogue" }, { "Cuyler", "Cuyler" }, { "Dale", "Dale" }, 
  			    { "Dalton", "Dalton" }, { "Dannemora", "Dannemora" }, { "Dansville", "Dansville" }, { "Darien Center", "Darien Center" }, 
  			    { "Davenport", "Davenport" }, { "Davenport Center", "Davenport Center" }, { "Davis Park", "Davis Park" }, { "Dayton", "Dayton" }, 
  			    { "De Kalb Junction", "De Kalb Junction" }, { "De Peyster", "De Peyster" }, { "De Ruyter", "De Ruyter" }, { "De Witt", "De Witt" }, 
  			    { "Deansboro", "Deansboro" }, { "Deer Park", "Deer Park" }, { "Deer River", "Deer River" }, { "Deerfield", "Deerfield" },
  			    { "Deferiet", "Deferiet" }, { "Degrasse", "Degrasse" }, { "Delancey", "Delancey" }, { "Delanson", "Delanson" },
  			    { "Delevan", "Delevan" }, { "Delhi", "Delhi" }, { "Delmar", "Delmar" }, { "Delphi Falls", "Delphi Falls" }, { "Denmark", "Denmark" },
  			    { "Denver", "Denver" }, { "Depauville", "Depauville" }, { "Depew", "Depew" }, { "Deposit", "Deposit" }, { "Derby", "Derby" }, 
  			    { "Dewittville", "Dewittville" }, { "Dexter", "Dexter" }, { "Diamond Point", "Diamond Point" }, { "Dickinson Center", "Dickinson Center" },
  			    { "Dix Hills", "Dix Hills" }, { "Dobbs Ferry", "Dobbs Ferry" }, { "Dolgeville", "Dolgeville" }, { "Dormansville", "Dormansville" }, 
  			    { "Douglaston", "Douglaston" }, { "Dover Plains", "Dover Plains" }, { "Downsville", "Downsville" }, { "Dresden", "Dresden" }, 
  			    { "Dryden", "Dryden" }, { "Duanesburg", "Duanesburg" }, { "Dundee", "Dundee" }, { "Dunkirk", "Dunkirk" }, { "Durham", "Durham" },
  			    { "Durhamville", "Durhamville" }, { "Eagle Bay", "Eagle Bay" }, { "Eagle Bridge", "Eagle Bridge" }, { "Eagle Harbor", "Eagle Harbor" },
  			    { "Earlton", "Earlton" }, { "Earlville", "Earlville" }, { "East Amherst", "East Amherst" }, { "East Atlantic Beach", "East Atlantic Beach" },
  			    { "East Aurora", "East Aurora" }, { "East Berne", "East Berne" }, { "East Bethany", "East Bethany" }, { "East Bloomfield", "East Bloomfield" },
  			    { "East Branch", "East Branch" }, { "East Chatham", "East Chatham" }, { "East Concord", "East Concord" }, { "East Durham", "East Durham" },
  			    { "East Elmhurst", "East Elmhurst" }, { "East Fishkill", "East Fishkill" }, { "East Freetown", "East Freetown" }, { "East Greenbush", "East Greenbush" },
  			    { "East Greenwich", "East Greenwich" }, { "East Hampton", "East Hampton" }, { "East Homer", "East Homer" }, { "East Islip", "East Islip" },
  			    { "East Jewett", "East Jewett" }, { "East Marion", "East Marion" }, { "East Meadow", "East Meadow" }, { "East Meredith", "East Meredith" },
  			    { "East Moriches", "East Moriches" }, { "East Nassau", "East Nassau" }, { "East Northport", "East Northport" }, { "East Norwich", "East Norwich" },
  			    { "East Otto", "East Otto" }, { "East Palmyra", "East Palmyra" }, { "East Patchogue", "East Patchogue" }, { "East Pembroke", "East Pembroke" },
  			    { "East Pharsalia", "East Pharsalia" }, { "East Quogue", "East Quogue" }, { "East Randolph", "East Randolph" }, { "East Rochester", "East Rochester" },
  			    { "East Rockaway", "East Rockaway" }, { "East Schodack", "East Schodack" }, { "East Setauket", "East Setauket" }, { "East Springfield", "East Springfield" },
  			    { "East Syracuse", "East Syracuse" }, { "East Williamson", "East Williamson" }, { "East Williston", "East Williston" }, { "East Windham", "East Windham" },
  			    { "East Worcester", "East Worcester" }, { "East Yaphank", "East Yaphank" }, { "Eastchester", "Eastchester" }, { "Eastport", "Eastport" }, { "Eaton", "Eaton" },
  			    { "Eddyville", "Eddyville" }, { "Eden", "Eden" }, { "Edgemere", "Edgemere" }, { "Edgewood", "Edgewood" }, { "Edmeston", "Edmeston" }, { "Edwards", "Edwards" },
  			    { "Eggertsville", "Eggertsville" }, { "Elba", "Elba" }, { "Elbridge", "Elbridge" }, { "Eldred", "Eldred" }, { "Elizabethtown", "Elizabethtown" },
  			    { "Elizaville", "Elizaville" }, { "Elka Park", "Elka Park" }, { "Ellenburg", "Ellenburg" }, { "Ellenburg Center", "Ellenburg Center" },
  			    { "Ellenburg Depot", "Ellenburg Depot" }, { "Ellenville", "Ellenville" }, { "Ellicottville", "Ellicottville" }, { "Ellington", "Ellington" },
  			    { "Ellisburg", "Ellisburg" }, { "Elma", "Elma" }, { "Elmhurst", "Elmhurst" }, { "Elmira", "Elmira" }, { "Elmira Heights", "Elmira Heights" },
  			    { "Elmont", "Elmont" }, { "Elmsford", "Elmsford" }, { "Elwood", "Elwood" }, { "Endicott", "Endicott" }, { "Endwell", "Endwell" },
  			    { "Erieville", "Erieville" }, { "Erin", "Erin" }, { "Esopus", "Esopus" }, { "Esperance", "Esperance" }, { "Essex", "Essex" },
  			    { "Etna", "Etna" }, { "Evans Mills", "Evans Mills" }, { "Fabius", "Fabius" }, { "Fair Harbor", "Fair Harbor" }, { "Fair Haven",
  			    "Fair Haven" }, { "Fairport", "Fairport" }, { "Falconer", "Falconer" }, { "Fallsburg", "Fallsburg" }, { "Fancher", "Fancher" },
  			    { "Far Rockaway", "Far Rockaway" }, { "Farmersville Station", "Farmersville Station" }, { "Farmingdale", "Farmingdale" },
  			    { "Farmington", "Farmington" }, { "Farmingville", "Farmingville" }, { "Farnham", "Farnham" }, { "Fayette", "Fayette" },
  			    { "Fayetteville", "Fayetteville" }, { "Felts Mills", "Felts Mills" }, { "Ferndale", "Ferndale" }, { "Feura Bush", "Feura Bush" },
  			    { "Fillmore", "Fillmore" }, { "Findley Lake", "Findley Lake" }, { "Fine", "Fine" }, { "Fineview", "Fineview" }, { "Fire Island Pines", "Fire Island Pines" },
  			    { "Fishers", "Fishers" }, { "Fishers Island", "Fishers Island" }, { "Fishers Landing", "Fishers Landing" }, { "Fishkill", "Fishkill" },
  			    { "Fishs Eddy", "Fishs Eddy" }, { "Flanders", "Flanders" }, { "Fleetwood", "Fleetwood" }, { "Fleischmanns", "Fleischmanns" }, 
  			    { "Floral Park", "Floral Park" }, { "Florida", "Florida" }, { "Flushing", "Flushing" }, { "Fly Creek", "Fly Creek" }, { "Fonda", "Fonda" },
  			    { "Forest Hills", "Forest Hills" }, { "Forestburgh", "Forestburgh" }, { "Forestport", "Forestport" }, { "Forestville", "Forestville" },
  			    { "Fort Ann", "Fort Ann" }, { "Fort Covington", "Fort Covington" }, { "Fort Drum", "Fort Drum" }, { "Fort Edward", "Fort Edward" },
  			    { "Fort Hamilton", "Fort Hamilton" }, { "Fort Hunter", "Fort Hunter" }, { "Fort Jackson", "Fort Jackson" }, { "Fort Johnson", "Fort Johnson" },
  			    { "Fort Montgomery", "Fort Montgomery" }, { "Fort Plain", "Fort Plain" }, { "Fort Salonga", "Fort Salonga" }, { "Fort Tilden", "Fort Tilden" },
  			    { "Frankfort", "Frankfort" }, { "Franklin", "Franklin" }, { "Franklin Springs", "Franklin Springs" }, { "Franklin Square", "Franklin Square" },
  			    { "Franklinville", "Franklinville" }, { "Fredonia", "Fredonia" }, { "Freedom", "Freedom" }, { "Freehold", "Freehold" }, { "Freeport", "Freeport" },
  			    { "Freeville", "Freeville" }, { "Fremont Center", "Fremont Center" }, { "Fresh Meadows", "Fresh Meadows" }, { "Frewsburg", "Frewsburg" },
  			    { "Friendship", "Friendship" }, { "Frontenac", "Frontenac" }, { "Fulton", "Fulton" }, { "Fultonham", "Fultonham" }, { "Fultonville", "Fultonville" },
  			    { "Gabriels", "Gabriels" }, { "Gainesville", "Gainesville" }, { "Gallupville", "Gallupville" }, { "Galway", "Galway" }, { "Gansevoort", "Gansevoort" },
  			    { "Garden City", "Garden City" }, { "Garden City Park", "Garden City Park" }, { "Garden City South", "Garden City South" }, { "Gardiner", "Gardiner" },
  			    { "Garnerville", "Garnerville" }, { "Garrattsville", "Garrattsville" }, { "Garrison", "Garrison" }, { "Gasport", "Gasport" }, { "Gates", "Gates" },
  			    { "Geneseo", "Geneseo" }, { "Geneva", "Geneva" }, { "Genoa", "Genoa" }, { "Georgetown", "Georgetown" }, { "Germantown", "Germantown" }, { "Gerry", "Gerry" },
  			    { "Getzville", "Getzville" }, { "Ghent", "Ghent" }, { "Gilbertsville", "Gilbertsville" }, { "Gilboa", "Gilboa" }, { "Gilgo Beach", "Gilgo Beach" },
  			    { "Glasco", "Glasco" }, { "Glen Aubrey", "Glen Aubrey" }, { "Glen Cove", "Glen Cove" }, { "Glen Head", "Glen Head" }, { "Glen Oaks", "Glen Oaks" },
  			    { "Glen Park", "Glen Park" }, { "Glen Spey", "Glen Spey" }, { "Glen Wild", "Glen Wild" }, { "Glendale", "Glendale" }, { "Glenfield", "Glenfield" },
  			    { "Glenford", "Glenford" }, { "Glenham", "Glenham" }, { "Glenmont", "Glenmont" }, { "Glens Falls", "Glens Falls" }, { "Glenville", "Glenville" },
  			    { "Glenwood", "Glenwood" }, { "Glenwood Landing", "Glenwood Landing" }, { "Gloversville", "Gloversville" }, { "Godeffroy", "Godeffroy" },
  			    { "Goldens Brg", "Goldens Brg" }, { "Goldens Bridge", "Goldens Bridge" }, { "Gorham", "Gorham" }, { "Goshen", "Goshen" }, { "Gouverneur", "Gouverneur" },
  			    { "Gowanda", "Gowanda" }, { "Grafton", "Grafton" }, { "Grahamsville", "Grahamsville" }, { "Grand Gorge", "Grand Gorge" },
  			    { "Grand Island", "Grand Island" }, { "Grandview On Hudson", "Grandview On Hudson" }, { "Granite Springs", "Granite Springs" },
  			    { "Granville", "Granville" }, { "Great Bend", "Great Bend" }, { "Great Neck", "Great Neck" }, { "Great Neck Plaza", "Great Neck Plaza" },
  			    { "Great River", "Great River" }, { "Great Valley", "Great Valley" }, { "Greece", "Greece" }, { "Green Island", "Green Island" }, { "Greene", "Greene" },
  			    { "Greenfield Center", "Greenfield Center" }, { "Greenfield Park", "Greenfield Park" }, { "Greenhurst", "Greenhurst" }, { "Greenlawn", "Greenlawn" },
  			    { "Greenport", "Greenport" }, { "Greenvale", "Greenvale" }, { "Greenville", "Greenville" }, { "Greenwich", "Greenwich" }, { "Greenwood", "Greenwood" },
  			    { "Greenwood Lake", "Greenwood Lake" }, { "Greig", "Greig" }, { "Grenell", "Grenell" }, { "Groton", "Groton" }, { "Groveland", "Groveland" },
  			    { "Guilderland", "Guilderland" }, { "Guilderland Center", "Guilderland Center" }, { "Guilford", "Guilford" }, { "Hadley", "Hadley" }, { "Hagaman",
  			    "Hagaman" }, { "Hague", "Hague" }, { "Hailesboro", "Hailesboro" }, { "Haines Falls", "Haines Falls" }, { "Halcott Center", "Halcott Center" },
  			    { "Halcottsville", "Halcottsville" }, { "Halesite", "Halesite" }, { "Halfmoon", "Halfmoon" }, { "Hall", "Hall" }, { "Hamburg", "Hamburg" },
  			    { "Hamden", "Hamden" }, { "Hamilton", "Hamilton" }, { "Hamlin", "Hamlin" }, { "Hammond", "Hammond" }, { "Hammondsport", "Hammondsport" },
  			    { "Hampton", "Hampton" }, { "Hampton Bays", "Hampton Bays" }, { "Hancock", "Hancock" }, { "Hankins", "Hankins" }, { "Hannacroix", "Hannacroix" },
  			    { "Hannawa Falls", "Hannawa Falls" }, { "Hannibal", "Hannibal" }, { "Harford", "Harford" }, { "Harford Mills", "Harford Mills" },
  			    { "Harpersfield", "Harpersfield" }, { "Harpursville", "Harpursville" }, { "Harriman", "Harriman" }, { "Harris", "Harris" }, { "Harrison", "Harrison" },
  			    { "Harrisville", "Harrisville" }, { "Hartford", "Hartford" }, { "Hartsdale", "Hartsdale" }, { "Hartwick", "Hartwick" },
  			    { "Hartwick Seminary", "Hartwick Seminary" }, { "Hastings", "Hastings" }, { "Hastings On Hudson", "Hastings On Hudson" },
  			    { "Hauppauge", "Hauppauge" }, { "Haverstraw", "Haverstraw" }, { "Hawthorne", "Hawthorne" }, { "Hayt Corners", "Hayt Corners" },
  			    { "Heathcote", "Heathcote" }, { "Hector", "Hector" }, { "Helena", "Helena" }, { "Hemlock", "Hemlock" }, { "Hempstead", "Hempstead" },
  			    { "Henderson", "Henderson" }, { "Henderson Harbor", "Henderson Harbor" }, { "Henrietta", "Henrietta" }, { "Hensonville", "Hensonville" },
  			    { "Herkimer", "Herkimer" }, { "Hermon", "Hermon" }, { "Heuvelton", "Heuvelton" }, { "Hewlett", "Hewlett" }, { "Hicksville", "Hicksville" },
  			    { "High Falls", "High Falls" }, { "Highland", "Highland" }, { "Highland Falls", "Highland Falls" }, { "Highland Lake", "Highland Lake" },
  			    { "Highland Mills", "Highland Mills" }, { "Highmount", "Highmount" }, { "Hillburn", "Hillburn" }, { "Hillsdale", "Hillsdale" },
  			    { "Hillside Manor", "Hillside Manor" }, { "Hilton", "Hilton" }, { "Himrod", "Himrod" }, { "Hinckley", "Hinckley" }, { "Hinsdale", "Hinsdale" },
  			    { "Hobart", "Hobart" }, { "Hoffmeister", "Hoffmeister" }, { "Hofstra University", "Hofstra University" }, { "Hogansburg", "Hogansburg" },
  			    { "Holbrook", "Holbrook" }, { "Holland", "Holland" }, { "Holland Patent", "Holland Patent" }, { "Holley", "Holley" }, { "Hollis", "Hollis" },
  			    { "Hollis Hills", "Hollis Hills" }, { "Hollowville", "Hollowville" }, { "Holmes", "Holmes" }, { "Holtsville", "Holtsville" }, { "Homer", "Homer" },
  			    { "Honeoye", "Honeoye" }, { "Honeoye Falls", "Honeoye Falls" }, { "Hoosick", "Hoosick" }, { "Hoosick Falls", "Hoosick Falls" }, { "Hopewell", "Hopewell" },
  			    { "Hopewell Junction", "Hopewell Junction" }, { "Hopkinton", "Hopkinton" }, { "Hornell", "Hornell" }, { "Horseheads", "Horseheads" },
  			    { "Hortonville", "Hortonville" }, { "Houghton", "Houghton" }, { "Howard Beach", "Howard Beach" }, { "Howells", "Howells" },
  			    { "Howes Cave", "Howes Cave" }, { "Hubbardsville", "Hubbardsville" }, { "Hudson", "Hudson" }, { "Hudson Falls", "Hudson Falls" },
  			    { "Hughsonville", "Hughsonville" }, { "Huguenot", "Huguenot" }, { "Hulberton", "Hulberton" }, { "Huletts Landing", "Huletts Landing" },
  			    { "Hume", "Hume" }, { "Hunt", "Hunt" }, { "Hunter", "Hunter" }, { "Huntington", "Huntington" }, { "Huntington Station", "Huntington Station" },
  			    { "Hurley", "Hurley" }, { "Hurleyville", "Hurleyville" }, { "Hyde Park", "Hyde Park" }, { "Ilion", "Ilion" }, { "Indian Lake", "Indian Lake" },
  			    { "Industry", "Industry" }, { "Inlet", "Inlet" }, { "Interlaken", "Interlaken" }, { "Inwood", "Inwood" }, { "Ionia", "Ionia" }, { "Irondequoit",
  			    "Irondequoit" }, { "Irving", "Irving" }, { "Irvington", "Irvington" }, { "Ischua", "Ischua" }, { "Island Park", "Island Park" }, { "Islandia", "Islandia" },
  			    { "Islip", "Islip" }, { "Islip Terrace", "Islip Terrace" }, { "Ithaca", "Ithaca" }, { "Jackson Heights", "Jackson Heights" }, { "Jacksonville", "Jacksonville" },
  			    { "Jamaica", "Jamaica" }, { "Jamesport", "Jamesport" }, { "Jamestown", "Jamestown" }, { "Jamesville", "Jamesville" }, { "Jasper", "Jasper" },
  			    { "Java Center", "Java Center" }, { "Java Village", "Java Village" }, { "Jay", "Jay" }, { "Jefferson", "Jefferson" },
  			    { "Jefferson Valley", "Jefferson Valley" }, { "Jeffersonville", "Jeffersonville" }, { "Jericho", "Jericho" }, { "Jewett", "Jewett" },
  			    { "Jfk Airport", "Jfk Airport" }, { "John F Kennedy Airport", "John F Kennedy Airport" }, { "Johnsburg", "Johnsburg" }, { "Johnson", "Johnson" },
  			    { "Johnson City", "Johnson City" }, { "Johnsonville", "Johnsonville" }, { "Johnstown", "Johnstown" }, { "Jordan", "Jordan" }, { "Jordanville", "Jordanville" },
  			    { "Kanona", "Kanona" }, { "Kaser", "Kaser" }, { "Katonah", "Katonah" }, { "Kattskill Bay", "Kattskill Bay" }, { "Kauneonga Lake", "Kauneonga Lake" },
  			    { "Keene", "Keene" }, { "Keene Valley", "Keene Valley" }, { "Keeseville", "Keeseville" }, { "Kendall", "Kendall" }, { "Kenmore", "Kenmore" },
  			    { "Kennedy", "Kennedy" }, { "Kenoza Lake", "Kenoza Lake" }, { "Kent", "Kent" }, { "Kent Cliffs", "Kent Cliffs" }, { "Kent Lakes", "Kent Lakes" },
  			    { "Kerhonkson", "Kerhonkson" }, { "Keuka Park", "Keuka Park" }, { "Kew Gardens", "Kew Gardens" }, { "Kew Gardens Hills", "Kew Gardens Hills" },
  			    { "Kiamesha Lake", "Kiamesha Lake" }, { "Kill Buck", "Kill Buck" }, { "Killawog", "Killawog" }, { "Kinderhook", "Kinderhook" },
  			    { "King Ferry", "King Ferry" }, { "Kings Park", "Kings Park" }, { "Kings Point", "Kings Point" }, { "Kingston", "Kingston" }, { "Kirkville", "Kirkville" },
  			    { "Kirkwood", "Kirkwood" }, { "Kiryas Joel", "Kiryas Joel" }, { "Kismet", "Kismet" }, { "Knapp Creek", "Knapp Creek" }, { "Knowlesville", "Knowlesville" },
  			    { "Knox", "Knox" }, { "Knoxboro", "Knoxboro" }, { "Krumville", "Krumville" }, { "La Fargeville", "La Fargeville" }, { "La Fayette", "La Fayette" },
  			    { "La Guardia Airport", "La Guardia Airport" }, { "Lackawanna", "Lackawanna" }, { "Lacona", "Lacona" }, { "Lagrangeville", "Lagrangeville" },
  			    { "Lake Clear", "Lake Clear" }, { "Lake George", "Lake George" }, { "Lake Grove", "Lake Grove" }, { "Lake Hill", "Lake Hill" },
  			    { "Lake Huntington", "Lake Huntington" }, { "Lake Katrine", "Lake Katrine" }, { "Lake Lincolndale", "Lake Lincolndale" }, { "Lake Luzerne", "Lake Luzerne" },
  			    { "Lake Peekskill", "Lake Peekskill" }, { "Lake Placid", "Lake Placid" }, { "Lake Pleasant", "Lake Pleasant" }, { "Lake Ronkonkoma", "Lake Ronkonkoma" },
  			    { "Lake Success", "Lake Success" }, { "Lake View", "Lake View" }, { "Lakemont", "Lakemont" }, { "Lakeville", "Lakeville" }, { "Lakewood", "Lakewood" },
  			    { "Lancaster", "Lancaster" }, { "Lanesville", "Lanesville" }, { "Lansing", "Lansing" }, { "Larchmont", "Larchmont" }, { "Latham", "Latham" },
  			    { "Laurel", "Laurel" }, { "Laurelton", "Laurelton" }, { "Laurens", "Laurens" }, { "Lawrence", "Lawrence" }, { "Lawrenceville", "Lawrenceville" },
  			    { "Lawtons", "Lawtons" }, { "Lawyersville", "Lawyersville" }, { "Le Roy", "Le Roy" }, { "Lebanon", "Lebanon" }, { "Lebanon Springs", "Lebanon Springs" },
  			    { "Lee Center", "Lee Center" }, { "Leeds", "Leeds" }, { "Leicester", "Leicester" }, { "Leon", "Leon" }, { "Leonardsville", "Leonardsville" },
  			    { "Levittown", "Levittown" }, { "Lew Beach", "Lew Beach" }, { "Lewis", "Lewis" }, { "Lewiston", "Lewiston" }, { "Lexington", "Lexington" }, 
  			    { "Liberty", "Liberty" }, { "Lido Beach", "Lido Beach" }, { "Lily Dale", "Lily Dale" }, { "Lima", "Lima" }, { "Limerick", "Limerick" },
  			    { "Limestone", "Limestone" }, { "Lincolndale", "Lincolndale" }, { "Lindenhurst", "Lindenhurst" }, { "Lindley", "Lindley" }, { "Linwood", "Linwood" },
  			    { "Lisbon", "Lisbon" }, { "Lisle", "Lisle" }, { "Little Falls", "Little Falls" }, { "Little Genesee", "Little Genesee" }, { "Little Neck", "Little Neck" },
  			    { "Little Valley", "Little Valley" }, { "Little York", "Little York" }, { "Liverpool", "Liverpool" }, { "Livingston", "Livingston" }, 
  			    { "Livingston Manor", "Livingston Manor" }, { "Livonia", "Livonia" }, { "Livonia Center", "Livonia Center" }, { "Lloyd Harbor", "Lloyd Harbor" },
  			    { "Loch Sheldrake", "Loch Sheldrake" }, { "Locke", "Locke" }, { "Lockport", "Lockport" }, { "Lockwood", "Lockwood" }, { "Locust Valley", "Locust Valley" },
  			    { "Lodi", "Lodi" }, { "Loehmanns Plaza", "Loehmanns Plaza" }, { "Long Beach", "Long Beach" }, { "Long Eddy", "Long Eddy" }, 
  			    { "Long Island City", "Long Island City" }, { "Long Lake", "Long Lake" }, { "Loon Lake", "Loon Lake" }, { "Lorraine", "Lorraine" },
  			    { "Loudonville", "Loudonville" }, { "Lowman", "Lowman" }, { "Lowville", "Lowville" }, { "Lycoming", "Lycoming" }, { "Lynbrook", "Lynbrook" }, 
  			    { "Lyndonville", "Lyndonville" }, { "Lyon Mountain", "Lyon Mountain" }, { "Lyons", "Lyons" }, { "Lyons Falls", "Lyons Falls" }, { "Lysander", "Lysander" },
  			    { "Mac Dougall", "Mac Dougall" }, { "Macedon", "Macedon" }, { "Machias", "Machias" }, { "Madison", "Madison" }, { "Madrid", "Madrid" }, { "Mahopac", "Mahopac" },
  			    { "Mahopac Falls", "Mahopac Falls" }, { "Maine", "Maine" }, { "Malba", "Malba" }, { "Malden Bridge", "Malden Bridge" }, { "Malden Hudson", "Malden Hudson" },
  			    { "Malden On Hudson", "Malden On Hudson" }, { "Mallory", "Mallory" }, { "Malone", "Malone" }, { "Malta", "Malta" }, { "Malverne", "Malverne" },
  			    { "Mamaroneck", "Mamaroneck" }, { "Manchester", "Manchester" }, { "Manhasset", "Manhasset" }, { "Manhasset Hills", "Manhasset Hills" },
  			    { "Manlius", "Manlius" }, { "Mannsville", "Mannsville" }, { "Manorville", "Manorville" }, { "Maple Springs", "Maple Springs" }, 
  			    { "Maple View", "Maple View" }, { "Maplecrest", "Maplecrest" }, { "Marathon", "Marathon" }, { "Marcellus", "Marcellus" }, { "Marcy", "Marcy" },
  			    { "Margaretville", "Margaretville" }, { "Marietta", "Marietta" }, { "Marilla", "Marilla" }, { "Marion", "Marion" }, { "Marlboro", "Marlboro" },
  			    { "Martinsburg", "Martinsburg" }, { "Martville", "Martville" }, { "Maryknoll", "Maryknoll" }, { "Maryland", "Maryland" }, { "Masonville", "Masonville" },
  			    { "Maspeth", "Maspeth" }, { "Massapequa", "Massapequa" }, { "Massapequa Park", "Massapequa Park" }, { "Massawepie", "Massawepie" },
  			    { "Massena", "Massena" }, { "Mastic", "Mastic" }, { "Mastic Beach", "Mastic Beach" }, { "Mattituck", "Mattituck" }, { "Mattydale", "Mattydale" },
  			    { "Maybrook", "Maybrook" }, { "Mayfield", "Mayfield" }, { "Mayville", "Mayville" }, { "Mcconnellsville", "Mcconnellsville" }, { "Mcdonough", "Mcdonough" },
  			    { "Mcgraw", "Mcgraw" }, { "Mclean", "Mclean" }, { "Meacham", "Meacham" }, { "Mechanicville", "Mechanicville" }, { "Mecklenburg", "Mecklenburg" },
  			    { "Medford", "Medford" }, { "Medina", "Medina" }, { "Medusa", "Medusa" }, { "Mellenville", "Mellenville" }, { "Melrose", "Melrose" },
  			    { "Melville", "Melville" }, { "Memphis", "Memphis" }, { "Menands", "Menands" }, { "Mendon", "Mendon" }, { "Meredith", "Meredith" },
  			    { "Meridale", "Meridale" }, { "Meridian", "Meridian" }, { "Merrick", "Merrick" }, { "Merrill", "Merrill" }, { "Mexico", "Mexico" },
  			    { "Mid Hudson", "Mid Hudson" }, { "Middle Falls", "Middle Falls" }, { "Middle Granville", "Middle Granville" }, { "Middle Grove", "Middle Grove" },
  			    { "Middle Island", "Middle Island" }, { "Middle Village", "Middle Village" }, { "Middleburgh", "Middleburgh" }, { "Middleport", "Middleport" },
  			    { "Middlesex", "Middlesex" }, { "Middletown", "Middletown" }, { "Middleville", "Middleville" }, { "Milan", "Milan" }, { "Milford", "Milford" },
  			    { "Mill Neck", "Mill Neck" }, { "Millbrook", "Millbrook" }, { "Miller Place", "Miller Place" }, { "Millerton", "Millerton" }, { "Millport", "Millport" },
  			    { "Millwood", "Millwood" }, { "Milton", "Milton" }, { "Mineola", "Mineola" }, { "Minerva", "Minerva" }, { "Minetto", "Minetto" }, 
  			    { "Mineville", "Mineville" }, { "Minoa", "Minoa" }, { "Model City", "Model City" }, { "Modena", "Modena" }, { "Mohawk", "Mohawk" },
  			    { "Mohegan Lake", "Mohegan Lake" }, { "Moira", "Moira" }, { "Mongaup Valley", "Mongaup Valley" }, { "Monroe", "Monroe" }, 
  			    { "Monsey", "Monsey" }, { "Montauk", "Montauk" }, { "Montebello", "Montebello" }, { "Montezuma", "Montezuma" }, { "Montgomery", "Montgomery" }, 
  			    { "Monticello", "Monticello" }, { "Montour Falls", "Montour Falls" }, { "Montrose", "Montrose" }, { "Mooers", "Mooers" }, { "Mooers Forks", "Mooers Forks" },
  			    { "Moravia", "Moravia" }, { "Moriah", "Moriah" }, { "Moriah Center", "Moriah Center" }, { "Moriches", "Moriches" }, { "Morris", "Morris" },
  			    { "Morrisonville", "Morrisonville" }, { "Morristown", "Morristown" }, { "Morrisville", "Morrisville" }, { "Morton", "Morton" },
  			    { "Mottville", "Mottville" }, { "Mount Kisco", "Mount Kisco" }, { "Mount Marion", "Mount Marion" }, { "Mount Morris", "Mount Morris" },
  			    { "Mount Sinai", "Mount Sinai" }, { "Mount Tremper", "Mount Tremper" }, { "Mount Upton", "Mount Upton" }, { "Mount Vernon", "Mount Vernon" },
  			    { "Mount Vision", "Mount Vision" }, { "Mountain Dale", "Mountain Dale" }, { "Mountainville", "Mountainville" }, { "Mumford", "Mumford" },
  			    { "Munnsville", "Munnsville" }, { "Murray Isle", "Murray Isle" }, { "Nanuet", "Nanuet" }, { "Napanoch", "Napanoch" }, { "Naples", "Naples" },
  			    { "Narrowsburg", "Narrowsburg" }, { "Nassau", "Nassau" }, { "Natural Bridge", "Natural Bridge" }, { "Nedrow", "Nedrow" }, { "Nelliston", "Nelliston" },
  			    { "Nelsonville", "Nelsonville" }, { "Neponsit", "Neponsit" }, { "Nesconset", "Nesconset" }, { "Neversink", "Neversink" },
  			    { "New Baltimore", "New Baltimore" }, { "New Berlin", "New Berlin" }, { "New City", "New City" }, { "New Hamburg", "New Hamburg" },
  			    { "New Hampton", "New Hampton" }, { "New Hartford", "New Hartford" }, { "New Haven", "New Haven" }, { "New Hyde Park", "New Hyde Park" },
  			    { "New Kingston", "New Kingston" }, { "New Lebanon", "New Lebanon" }, { "New Lisbon", "New Lisbon" }, { "New Milford", "New Milford" }, 
  			    { "New Paltz", "New Paltz" }, { "New Rochelle", "New Rochelle" }, { "New Russia", "New Russia" }, { "New Square", "New Square" },
  			    { "New Suffolk", "New Suffolk" }, { "New Windsor", "New Windsor" }, { "New Woodstock", "New Woodstock" }, { "New York", "New York" },
  			    { "New York Mills", "New York Mills" }, { "Newark", "Newark" }, { "Newark Valley", "Newark Valley" }, { "Newburgh", "Newburgh" }, 
  			    { "Newcomb", "Newcomb" }, { "Newfane", "Newfane" }, { "Newfield", "Newfield" }, { "Newport", "Newport" }, { "Newton Falls", "Newton Falls" },
  			    { "Newtonville", "Newtonville" }, { "Niagara Falls", "Niagara Falls" }, { "Niagara University", "Niagara University" }, { "Nichols", "Nichols" },
  			    { "Nicholville", "Nicholville" }, { "Nineveh", "Nineveh" }, { "Niobe", "Niobe" }, { "Niskayuna", "Niskayuna" }, { "Niverville", "Niverville" },
  			    { "Norfolk", "Norfolk" }, { "North Babylon", "North Babylon" }, { "North Baldwin", "North Baldwin" }, { "North Bangor", "North Bangor" },
  			    { "North Bay", "North Bay" }, { "North Bellmore", "North Bellmore" }, { "North Blenheim", "North Blenheim" }, { "North Boston", "North Boston" },
  			    { "North Branch", "North Branch" }, { "North Brookfield", "North Brookfield" }, { "North Castle", "North Castle" }, { "North Chatham", "North Chatham" },
  			    { "North Chili", "North Chili" }, { "North Cohocton", "North Cohocton" }, { "North Collins", "North Collins" }, { "North Creek", "North Creek" },
  			    { "North Evans", "North Evans" }, { "North Granville", "North Granville" }, { "North Greece", "North Greece" }, { "North Hills", "North Hills" }, 
  			    { "North Hoosick", "North Hoosick" }, { "North Hornell", "North Hornell" }, { "North Hudson", "North Hudson" }, { "North Java", "North Java" },
  			    { "North Lawrence", "North Lawrence" }, { "North Massapequa", "North Massapequa" }, { "North Merrick", "North Merrick" }, 
  			    { "North New Hyde Park", "North New Hyde Park" }, { "North Norwich", "North Norwich" }, { "North Pitcher", "North Pitcher" },
  			    { "North River", "North River" }, { "North Rose", "North Rose" }, { "North Salem", "North Salem" }, { "North Syracuse", "North Syracuse" },
  			    { "North Tarrytown", "North Tarrytown" }, { "North Tonawanda", "North Tonawanda" }, { "Northport", "Northport" }, { "Northville", "Northville" },
  			    { "Norton Hill", "Norton Hill" }, { "Norwich", "Norwich" }, { "Norwood", "Norwood" }, { "Nunda", "Nunda" }, { "Nyack", "Nyack" }, 
  			    { "Oak Beach", "Oak Beach" }, { "Oak Hill", "Oak Hill" }, { "Oak Island", "Oak Island" }, { "Oakdale", "Oakdale" },
  			    { "Oakfield", "Oakfield" }, { "Oakland Gardens", "Oakland Gardens" }, { "Oaks Corners", "Oaks Corners" }, { "Obernburg", "Obernburg" }, 
  			    { "Ocean Beach", "Ocean Beach" }, { "Oceanside", "Oceanside" }, { "Odessa", "Odessa" }, { "Ogdensburg", "Ogdensburg" }, { "Ohio", "Ohio" },
  			    { "Olcott", "Olcott" }, { "Old Bethpage", "Old Bethpage" }, { "Old Chatham", "Old Chatham" }, { "Old Forge", "Old Forge" }, { "Old Westbury", "Old Westbury" },
  			    { "Olean", "Olean" }, { "Olivebridge", "Olivebridge" }, { "Oliverea", "Oliverea" }, { "Olmstedville", "Olmstedville" }, 
  			    { "Onchiota", "Onchiota" }, { "Oneida", "Oneida" }, { "Oneonta", "Oneonta" }, { "Ontario", "Ontario" }, { "Ontario Center", "Ontario Center" },
  			    { "Orangeburg", "Orangeburg" }, { "Orchard Park", "Orchard Park" }, { "Orient", "Orient" }, { "Oriskany", "Oriskany" }, { "Oriskany Falls", "Oriskany Falls" },
  			    { "Orwell", "Orwell" }, { "Ossining", "Ossining" }, { "Oswegatchie", "Oswegatchie" }, { "Oswego", "Oswego" }, { "Otego", "Otego" },
  			    { "Otisville", "Otisville" }, { "Otto", "Otto" }, { "Ouaquaga", "Ouaquaga" }, { "Ovid", "Ovid" }, { "Owasco", "Owasco" },
  			    { "Owego", "Owego" }, { "Owls Head", "Owls Head" }, { "Oxbow", "Oxbow" }, { "Oxford", "Oxford" }, { "Oyster Bay", "Oyster Bay" }, 
  			    { "Ozone Park", "Ozone Park" }, { "Painted Post", "Painted Post" }, { "Palatine Bridge", "Palatine Bridge" }, { "Palenville", "Palenville" },
  			    { "Palisades", "Palisades" }, { "Palmyra", "Palmyra" }, { "Panama", "Panama" }, { "Panorama", "Panorama" }, { "Paradox", "Paradox" },
  			    { "Paris", "Paris" }, { "Parish", "Parish" }, { "Parishville", "Parishville" }, { "Parksville", "Parksville" }, { "Patchogue", "Patchogue" },
  			    { "Patterson", "Patterson" }, { "Pattersonville", "Pattersonville" }, { "Paul Smiths", "Paul Smiths" }, { "Pavilion", "Pavilion" }, 
  			    { "Pawling", "Pawling" }, { "Pearl River", "Pearl River" }, { "Peconic", "Peconic" }, { "Peekskill", "Peekskill" }, { "Pelham", "Pelham" },
  			    { "Penfield", "Penfield" }, { "Penn Yan", "Penn Yan" }, { "Pennellville", "Pennellville" }, { "Perkinsville", "Perkinsville" }, { "Perry", "Perry" },
  			    { "Perrysburg", "Perrysburg" }, { "Perryville", "Perryville" }, { "Peru", "Peru" }, { "Peterboro", "Peterboro" }, { "Petersburg", "Petersburg" },
  			    { "Petersburgh", "Petersburgh" }, { "Phelps", "Phelps" }, { "Philadelphia", "Philadelphia" }, { "Phillipsport", "Phillipsport" }, { "Philmont", "Philmont" },
  			    { "Phoenicia", "Phoenicia" }, { "Phoenix", "Phoenix" }, { "Piercefield", "Piercefield" }, { "Piermont", "Piermont" }, 
  			    { "Pierrepont Manor", "Pierrepont Manor" }, { "Piffard", "Piffard" }, { "Pike", "Pike" }, { "Pilot Knob", "Pilot Knob" }, 
  			    { "Pine Bush", "Pine Bush" }, { "Pine City", "Pine City" }, { "Pine Hill", "Pine Hill" }, { "Pine Island", "Pine Island" }, 
  			    { "Pine Plains", "Pine Plains" }, { "Pine Valley", "Pine Valley" }, { "Piseco", "Piseco" }, { "Pitcher", "Pitcher" }, 
  			    { "Pittsford", "Pittsford" }, { "Plainview", "Plainview" }, { "Plainville", "Plainville" }, { "Plandome", "Plandome" }, 
  			    { "Plattekill", "Plattekill" }, { "Plattsburgh", "Plattsburgh" }, { "Pleasant Valley", "Pleasant Valley" }, { "Pleasantville", "Pleasantville" },
  			    { "Plessis", "Plessis" }, { "Plymouth", "Plymouth" }, { "Poestenkill", "Poestenkill" }, { "Point Lookout", "Point Lookout" }, 
  			    { "Point O Woods", "Point O Woods" }, { "Point Vivian", "Point Vivian" }, { "Poland", "Poland" }, { "Pomona", "Pomona" }, 
  			    { "Pompey", "Pompey" }, { "Pond Eddy", "Pond Eddy" }, { "Poolville", "Poolville" }, { "Poplar Ridge", "Poplar Ridge" },
  			    { "Port Byron", "Port Byron" }, { "Port Chester", "Port Chester" }, { "Port Crane", "Port Crane" }, { "Port Ewen", "Port Ewen" }, 
  			    { "Port Gibson", "Port Gibson" }, { "Port Henry", "Port Henry" }, { "Port Jefferson", "Port Jefferson" }, 
  			    { "Port Jefferson Station", "Port Jefferson Station" }, { "Port Jervis", "Port Jervis" }, { "Port Kent", "Port Kent" },
  			    { "Port Leyden", "Port Leyden" }, { "Port Washington", "Port Washington" }, { "Portageville", "Portageville" }, { "Porter Corners", "Porter Corners" },
  			    { "Portland", "Portland" }, { "Portlandville", "Portlandville" }, { "Portville", "Portville" }, { "Potsdam", "Potsdam" }, { "Pottersville", "Pottersville" },
  			    { "Poughkeepsie", "Poughkeepsie" }, { "Poughquag", "Poughquag" }, { "Pound Ridge", "Pound Ridge" }, { "Pratts Hollow", "Pratts Hollow" }, 
  			    { "Prattsburgh", "Prattsburgh" }, { "Prattsville", "Prattsville" }, { "Preble", "Preble" }, { "Preston Hollow", "Preston Hollow" },
  			    { "Prospect", "Prospect" }, { "Pulaski", "Pulaski" }, { "Pulteney", "Pulteney" }, { "Pultneyville", "Pultneyville" }, { "Purchase", "Purchase" },
  			    { "Purdys", "Purdys" }, { "Purling", "Purling" }, { "Putnam Station", "Putnam Station" }, { "Putnam Valley", "Putnam Valley" },
  			    { "Pyrites", "Pyrites" }, { "Quaker Street", "Quaker Street" }, { "Queens Village", "Queens Village" }, { "Queensbury", "Queensbury" },
  			    { "Quogue", "Quogue" }, { "Rainbow Lake", "Rainbow Lake" }, { "Randolph", "Randolph" }, { "Ransomville", "Ransomville" },
  			    { "Raquette Lake", "Raquette Lake" }, { "Ravena", "Ravena" }, { "Ray Brook", "Ray Brook" }, { "Raymondville", "Raymondville" },
  			    { "Reading Center", "Reading Center" }, { "Red Creek", "Red Creek" }, { "Red Hook", "Red Hook" }, { "Redfield", "Redfield" },
  			    { "Redford", "Redford" }, { "Redwood", "Redwood" }, { "Rego Park", "Rego Park" }, { "Remsen", "Remsen" }, { "Remsenburg", "Remsenburg" },
  			    { "Rensselaer", "Rensselaer" }, { "Rensselaer Falls", "Rensselaer Falls" }, { "Rensselaerville", "Rensselaerville" }, { "Retsof", "Retsof" },
  			    { "Rexford", "Rexford" }, { "Rexville", "Rexville" }, { "Rhinebeck", "Rhinebeck" }, { "Rhinecliff", "Rhinecliff" }, { "Richburg", "Richburg" },
  			    { "Richfield Springs", "Richfield Springs" }, { "Richford", "Richford" }, { "Richland", "Richland" }, { "Richmond Hill", "Richmond Hill" },
  			    { "Richmondville", "Richmondville" }, { "Richville", "Richville" }, { "Ridge", "Ridge" }, { "Ridgemont", "Ridgemont" }, { "Ridgewood", "Ridgewood" },
  			    { "Rifton", "Rifton" }, { "Riparius", "Riparius" }, { "Ripley", "Ripley" }, { "Riverhead", "Riverhead" }, { "Rochdale Village", "Rochdale Village" },
  			    { "Rochester", "Rochester" }, { "Rock City Falls", "Rock City Falls" }, { "Rock Glen", "Rock Glen" }, { "Rock Hill", "Rock Hill" },
  			    { "Rock Stream", "Rock Stream" }, { "Rock Tavern", "Rock Tavern" }, { "Rockaway Beach", "Rockaway Beach" }, { "Rockaway Park", "Rockaway Park" },
  			    { "Rockaway Point", "Rockaway Point" }, { "Rockville Centre", "Rockville Centre" }, { "Rocky Point", "Rocky Point" }, { "Rodman", "Rodman" },
  			    { "Roessleville", "Roessleville" }, { "Rome", "Rome" }, { "Romulus", "Romulus" }, { "Ronkonkoma", "Ronkonkoma" }, { "Roosevelt", "Roosevelt" },
  			    { "Roosevelt Island", "Roosevelt Island" }, { "Rooseveltown", "Rooseveltown" }, { "Roscoe", "Roscoe" }, { "Rose", "Rose" }, { "Roseboom", "Roseboom" },
  			    { "Rosedale", "Rosedale" }, { "Rosendale", "Rosendale" }, { "Roslyn", "Roslyn" }, { "Roslyn Heights", "Roslyn Heights" }, { "Rossburg", "Rossburg" },
  			    { "Rotterdam", "Rotterdam" }, { "Rotterdam Junction", "Rotterdam Junction" }, { "Round Lake", "Round Lake" }, { "Round Top", "Round Top" },
  			    { "Rouses Point", "Rouses Point" }, { "Roxbury", "Roxbury" }, { "Ruby", "Ruby" }, { "Rush", "Rush" }, { "Rushford", "Rushford" },
  			    { "Rushville", "Rushville" }, { "Russell", "Russell" }, { "Rye", "Rye" }, { "Rye Brook", "Rye Brook" }, { "Sabael", "Sabael" },
  			    { "Sackets Harbor", "Sackets Harbor" }, { "Sag Harbor", "Sag Harbor" }, { "Sagaponack", "Sagaponack" }, { "Saint Albans", "Saint Albans" },
  			    { "Saint Bonaventure", "Saint Bonaventure" }, { "Saint Huberts", "Saint Huberts" }, { "Saint James", "Saint James" }, 
  			    { "Saint Johnsville", "Saint Johnsville" }, { "Saint Regis Falls", "Saint Regis Falls" }, { "Saint Remy", "Saint Remy" }, 
  			    { "Salamanca", "Salamanca" }, { "Salem", "Salem" }, { "Salisbury Center", "Salisbury Center" }, { "Salisbury Mills", "Salisbury Mills" },
  			    { "Salt Point", "Salt Point" }, { "Saltaire", "Saltaire" }, { "Sanborn", "Sanborn" }, { "Sand Lake", "Sand Lake" }, { "Sands Point", "Sands Point" },
  			    { "Sandusky", "Sandusky" }, { "Sandy Creek", "Sandy Creek" }, { "Sangerfield", "Sangerfield" }, { "Sanitaria Springs", "Sanitaria Springs" },
  			    { "Saranac", "Saranac" }, { "Saranac Lake", "Saranac Lake" }, { "Saratoga Springs", "Saratoga Springs" }, { "Sardinia", "Sardinia" }, 
  			    { "Saugerties", "Saugerties" }, { "Sauquoit", "Sauquoit" }, { "Savannah", "Savannah" }, { "Savona", "Savona" }, { "Sayville", "Sayville" },
  			    { "Scarborough", "Scarborough" }, { "Scarsdale", "Scarsdale" }, { "Schaghticoke", "Schaghticoke" }, { "Schenectady", "Schenectady" }, 
  			    { "Schenevus", "Schenevus" }, { "Schodack Landing", "Schodack Landing" }, { "Schoharie", "Schoharie" }, { "Schroon Lake", "Schroon Lake" },
  			    { "Schuyler", "Schuyler" }, { "Schuyler Falls", "Schuyler Falls" }, { "Schuyler Lake", "Schuyler Lake" }, { "Schuylerville", "Schuylerville" },
  			    { "Scio", "Scio" }, { "Scipio Center", "Scipio Center" }, { "Scotchtown", "Scotchtown" }, { "Scotia", "Scotia" }, { "Scottsburg", "Scottsburg" },
  			    { "Scottsville", "Scottsville" }, { "Sea Cliff", "Sea Cliff" }, { "Seaford", "Seaford" }, { "Selden", "Selden" }, { "Selkirk", "Selkirk" },
  			    { "Seneca Castle", "Seneca Castle" }, { "Seneca Falls", "Seneca Falls" }, { "Setauket", "Setauket" }, { "Severance", "Severance" }, { "Shady", "Shady" },
  			    { "Shandaken", "Shandaken" }, { "Sharon Springs", "Sharon Springs" }, { "Shelter Island", "Shelter Island" }, 
  			    { "Shelter Island Heights", "Shelter Island Heights" }, { "Shenorock", "Shenorock" }, { "Sherburne", "Sherburne" }, { "Sheridan", "Sheridan" },
  			    { "Sherman", "Sherman" }, { "Sherrill", "Sherrill" }, { "Shinhopple", "Shinhopple" }, { "Shirley", "Shirley" }, { "Shokan", "Shokan" },
  			    { "Shoreham", "Shoreham" }, { "Shortsville", "Shortsville" }, { "Shrub Oak", "Shrub Oak" }, { "Shushan", "Shushan" }, { "Sidney", "Sidney" },
  			    { "Sidney Center", "Sidney Center" }, { "Siena", "Siena" }, { "Silver Bay", "Silver Bay" }, { "Silver Creek", "Silver Creek" },
  			    { "Silver Lake", "Silver Lake" }, { "Silver Springs", "Silver Springs" }, { "Sinclairville", "Sinclairville" }, { "Skaneateles", "Skaneateles" },
  			    { "Skaneateles Falls", "Skaneateles Falls" }, { "Slate Hill", "Slate Hill" }, { "Slaterville Springs", "Slaterville Springs" },
  			    { "Sleepy Hollow", "Sleepy Hollow" }, { "Slingerlands", "Slingerlands" }, { "Sloan", "Sloan" }, { "Sloansville", "Sloansville" }, 
  			    { "Sloatsburg", "Sloatsburg" }, { "Smallwood", "Smallwood" }, { "Smith Point", "Smith Point" }, { "Smithboro", "Smithboro" }, 
  			    { "Smithtown", "Smithtown" }, { "Smithville", "Smithville" }, { "Smithville Flats", "Smithville Flats" }, { "Smyrna", "Smyrna" }, 
  			    { "Snyder", "Snyder" }, { "Sodus", "Sodus" }, { "Sodus Center", "Sodus Center" }, { "Sodus Point", "Sodus Point" }, { "Solsville", "Solsville" }, 
  			    { "Solvay", "Solvay" }, { "Somers", "Somers" }, { "Sonyea", "Sonyea" }, { "Sound Beach", "Sound Beach" }, { "South Bethlehem", "South Bethlehem" },
  			    { "South Butler", "South Butler" }, { "South Byron", "South Byron" }, { "South Cairo", "South Cairo" }, { "South Cheektowaga", "South Cheektowaga" }, 
  			    { "South Colton", "South Colton" }, { "South Corning", "South Corning" }, { "South Dayton", "South Dayton" }, { "South Edmeston", "South Edmeston" },
  			    { "South Fallsburg", "South Fallsburg" }, { "South Farmingdale", "South Farmingdale" }, { "South Floral Park", "South Floral Park" }, 
  			    { "South Glens Falls", "South Glens Falls" }, { "South Hempstead", "South Hempstead" }, { "South Huntington", "South Huntington" }, 
  			    { "South Jamesport", "South Jamesport" }, { "South Kortright", "South Kortright" }, { "South Lima", "South Lima" }, 
  			    { "South New Berlin", "South New Berlin" }, { "South Otselic", "South Otselic" }, { "South Ozone Park", "South Ozone Park" }, 
  			    { "South Plymouth", "South Plymouth" }, { "South Richmond Hill", "South Richmond Hill" }, { "South Rutland", "South Rutland" }, { "South Salem", "South Salem" },
  			    { "South Schodack", "South Schodack" }, { "South Setauket", "South Setauket" }, { "South Wales", "South Wales" }, { "South Westerlo", "South Westerlo" },
  			    { "Southampton", "Southampton" }, { "Southfields", "Southfields" }, { "Southold", "Southold" }, { "Sparkill", "Sparkill" }, { "Sparrow Bush", "Sparrow Bush" },
  			    { "Sparrowbush", "Sparrowbush" }, { "Speculator", "Speculator" }, { "Spencer", "Spencer" }, { "Spencerport", "Spencerport" }, { "Spencertown", "Spencertown" },
  			    { "Speonk", "Speonk" }, { "Sprakers", "Sprakers" }, { "Spring Brook", "Spring Brook" }, { "Spring Glen", "Spring Glen" }, { "Spring Valley", "Spring Valley" },
  			    { "Springfield Center", "Springfield Center" }, { "Springfield Gardens", "Springfield Gardens" }, { "Springville", "Springville" }, 
  			    { "Springwater", "Springwater" }, { "Staatsburg", "Staatsburg" }, { "Stafford", "Stafford" }, { "Stamford", "Stamford" }, 
  			    { "Stanfordville", "Stanfordville" }, { "Stanley", "Stanley" }, { "Star Lake", "Star Lake" }, { "Staten Island", "Staten Island" }, 
  			    { "Steamburg", "Steamburg" }, { "Stella Niagara", "Stella Niagara" }, { "Stephentown", "Stephentown" }, { "Sterling", "Sterling" },
  			    { "Sterling Forest", "Sterling Forest" }, { "Stewart Manor", "Stewart Manor" }, { "Stillwater", "Stillwater" }, { "Stittville", "Stittville" },
  			    { "Stockton", "Stockton" }, { "Stone Ridge", "Stone Ridge" }, { "Stony Brook", "Stony Brook" }, { "Stony Creek", "Stony Creek" },
  			    { "Stony Point", "Stony Point" }, { "Stormville", "Stormville" }, { "Stottville", "Stottville" }, { "Stow", "Stow" }, { "Stratford", "Stratford" }, 
  			    { "Strykersville", "Strykersville" }, { "Stuyvesant", "Stuyvesant" }, { "Stuyvesant Falls", "Stuyvesant Falls" }, { "Suffern", "Suffern" },
  			    { "Sugar Loaf", "Sugar Loaf" }, { "Summit", "Summit" }, { "Summitville", "Summitville" }, { "Sundown", "Sundown" }, { "Sunnyside", "Sunnyside" },
  			    { "Surprise", "Surprise" }, { "Swain", "Swain" }, { "Swan Lake", "Swan Lake" }, { "Swormville", "Swormville" }, { "Sylvan Beach", "Sylvan Beach" },
  			    { "Syosset", "Syosset" }, { "Syracuse", "Syracuse" }, { "Taberg", "Taberg" }, { "Taconic Lake", "Taconic Lake" }, { "Taghkanic", "Taghkanic" },
  			    { "Tahawus", "Tahawus" }, { "Tallman", "Tallman" }, { "Tannersville", "Tannersville" }, { "Tappan", "Tappan" }, { "Tarrytown", "Tarrytown" },
  			    { "Thendara", "Thendara" }, { "Theresa", "Theresa" }, { "Thiells", "Thiells" }, { "Thompson Ridge", "Thompson Ridge" }, { "Thompsonville", "Thompsonville" },
  			    { "Thomson", "Thomson" }, { "Thornwood", "Thornwood" }, { "Thousand Island Park", "Thousand Island Park" }, { "Three Mile Bay", "Three Mile Bay" },
  			    { "Thurman", "Thurman" }, { "Ticonderoga", "Ticonderoga" }, { "Tillson", "Tillson" }, { "Tioga Center", "Tioga Center" }, { "Tivoli", "Tivoli" },
  			    { "Tomkins Cove", "Tomkins Cove" }, { "Tonawanda", "Tonawanda" }, { "Town Of Tonawanda", "Town Of Tonawanda" }, { "Treadwell", "Treadwell" },
  			    { "Tribes Hill", "Tribes Hill" }, { "Troupsburg", "Troupsburg" }, { "Trout Creek", "Trout Creek" }, { "Troy", "Troy" }, { "Trumansburg", "Trumansburg" },
  			    { "Truxton", "Truxton" }, { "Tuckahoe", "Tuckahoe" }, { "Tully", "Tully" }, { "Tunnel", "Tunnel" }, { "Tupper Lake", "Tupper Lake" }, 
  			    { "Turin", "Turin" }, { "Tuscarora", "Tuscarora" }, { "Tuxedo Park", "Tuxedo Park" }, { "Tyrone", "Tyrone" }, { "Ulster Park", "Ulster Park" },
  			    { "Unadilla", "Unadilla" }, { "Union Hill", "Union Hill" }, { "Union Springs", "Union Springs" }, { "Uniondale", "Uniondale" }, 
  			    { "Unionville", "Unionville" }, { "Upper Jay", "Upper Jay" }, { "Upper Saint Regis", "Upper Saint Regis" }, { "Upton", "Upton" }, 
  			    { "Utica", "Utica" }, { "Vails Gate", "Vails Gate" }, { "Valatie", "Valatie" }, { "Valhalla", "Valhalla" }, { "Valley Cottage", "Valley Cottage" },
  			    { "Valley Falls", "Valley Falls" }, { "Valley Stream", "Valley Stream" }, { "Valois", "Valois" }, { "Van Buren Bay", "Van Buren Bay" },
  			    { "Van Buren Point", "Van Buren Point" }, { "Van Etten", "Van Etten" }, { "Van Hornesville", "Van Hornesville" }, { "Varysburg", "Varysburg" },
  			    { "Venice Center", "Venice Center" }, { "Verbank", "Verbank" }, { "Vermontville", "Vermontville" }, { "Vernon", "Vernon" }, { "Vernon Center",
  			    "Vernon Center" }, { "Verona", "Verona" }, { "Verona Beach", "Verona Beach" }, { "Verplanck", "Verplanck" }, { "Versailles", "Versailles" },
  			    { "Vestal", "Vestal" }, { "Veterans Administration", "Veterans Administration" }, { "Victor", "Victor" }, { "Victory Mills", "Victory Mills" },
  			    { "Village Of Garden City", "Village Of Garden City" }, { "Voorheesville", "Voorheesville" }, { "Waccabuc", "Waccabuc" }, { "Waddington", "Waddington" }, 
  			    { "Wadhams", "Wadhams" }, { "Wading River", "Wading River" }, { "Wadsworth", "Wadsworth" }, { "Wainscott", "Wainscott" }, { "Walden", "Walden" },
  			    { "Wales Center", "Wales Center" }, { "Walker Valley", "Walker Valley" }, { "Wallace", "Wallace" }, { "Wallkill", "Wallkill" }, { "Walton", "Walton" },
  			    { "Walworth", "Walworth" }, { "Wampsville", "Wampsville" }, { "Wanakena", "Wanakena" }, { "Wantagh", "Wantagh" }, { "Wappingers Falls", "Wappingers Falls" },
  			    { "Warners", "Warners" }, { "Warnerville", "Warnerville" }, { "Warrensburg", "Warrensburg" }, { "Warsaw", "Warsaw" }, { "Warwick", "Warwick" }, 
  			    { "Washington Mills", "Washington Mills" }, { "Washingtonville", "Washingtonville" }, { "Wassaic", "Wassaic" }, { "Water Mill", "Water Mill" }, 
  			    { "Waterford", "Waterford" }, { "Waterloo", "Waterloo" }, { "Waterport", "Waterport" }, { "Watertown", "Watertown" }, { "Waterville", "Waterville" },
  			    { "Watervliet", "Watervliet" }, { "Watkins Glen", "Watkins Glen" }, { "Wave Crest", "Wave Crest" }, { "Waverly", "Waverly" }, { "Wawarsing", "Wawarsing" },
  			    { "Wayland", "Wayland" }, { "Wayne", "Wayne" }, { "Webster", "Webster" }, { "Webster Crossing", "Webster Crossing" }, { "Weedsport", "Weedsport" }, 
  			    { "Wellesley Island", "Wellesley Island" }, { "Wells", "Wells" }, { "Wells Bridge", "Wells Bridge" }, { "Wellsburg", "Wellsburg" }, 
  			    { "Wellsville", "Wellsville" }, { "West Amherst", "West Amherst" }, { "West Babylon", "West Babylon" }, { "West Bangor", "West Bangor" }, 
  			    { "West Bloomfield", "West Bloomfield" }, { "West Brentwood", "West Brentwood" }, { "West Burlington", "West Burlington" }, { "West Camp", "West Camp" },
  			    { "West Charlton", "West Charlton" }, { "West Chazy", "West Chazy" }, { "West Clarksville", "West Clarksville" }, { "West Coxsackie", "West Coxsackie" },
  			    { "West Danby", "West Danby" }, { "West Davenport", "West Davenport" }, { "West Eaton", "West Eaton" }, { "West Edmeston", "West Edmeston" }, 
  			    { "West Ellicott", "West Ellicott" }, { "West Exeter", "West Exeter" }, { "West Falls", "West Falls" }, { "West Fishkill", "West Fishkill" }, 
  			    { "West Fulton", "West Fulton" }, { "West Gilgo Beach", "West Gilgo Beach" }, { "West Harrison", "West Harrison" }, { "West Haverstraw", "West Haverstraw" },
  			    { "West Hempstead", "West Hempstead" }, { "West Henrietta", "West Henrietta" }, { "West Hurley", "West Hurley" }, { "West Islip", "West Islip" }, 
  			    { "West Kill", "West Kill" }, { "West Lebanon", "West Lebanon" }, { "West Leyden", "West Leyden" }, { "West Monroe", "West Monroe" },
  			    { "West Nyack", "West Nyack" }, { "West Oneonta", "West Oneonta" }, { "West Park", "West Park" }, { "West Point", "West Point" }, 
  			    { "West Rush", "West Rush" }, { "West Sand Lake", "West Sand Lake" }, { "West Sayville", "West Sayville" }, { "West Seneca", "West Seneca" },
  			    { "West Shokan", "West Shokan" }, { "West Stockholm", "West Stockholm" }, { "West Valley", "West Valley" }, { "West Windsor", "West Windsor" },
  			    { "West Winfield", "West Winfield" }, { "Westbrookville", "Westbrookville" }, { "Westbury", "Westbury" }, { "Westdale", "Westdale" }, 
  			    { "Westerlo", "Westerlo" }, { "Westernville", "Westernville" }, { "Westfield", "Westfield" }, { "Westford", "Westford" }, { "Westgate", "Westgate" },
  			    { "Westhampton", "Westhampton" }, { "Westhampton Beach", "Westhampton Beach" }, { "Westmoreland", "Westmoreland" }, { "Westons Mills", "Westons Mills" }, 
  			    { "Westport", "Westport" }, { "Westtown", "Westtown" }, { "Wevertown", "Wevertown" }, { "Whallonsburg", "Whallonsburg" }, 
  			    { "Wheatley Heights", "Wheatley Heights" }, { "Whippleville", "Whippleville" }, { "White Creek", "White Creek" }, { "White Lake", "White Lake" },
  			    { "White Plains", "White Plains" }, { "White Sulphur Springs", "White Sulphur Springs" }, { "Whiteface Mountain", "Whiteface Mountain" },
  			    { "Whitehall", "Whitehall" }, { "Whitesboro", "Whitesboro" }, { "Whitestone", "Whitestone" }, { "Whitesville", "Whitesville" },
  			    { "Whitney Point", "Whitney Point" }, { "Wiccopee", "Wiccopee" }, { "Willard", "Willard" }, { "Willet", "Willet" }, { "Williamson", "Williamson" },
  			    { "Williamstown", "Williamstown" }, { "Williamsville", "Williamsville" }, { "Williston Park", "Williston Park" }, { "Willow", "Willow" }, 
  			    { "Willsboro", "Willsboro" }, { "Willseyville", "Willseyville" }, { "Wilmington", "Wilmington" }, { "Wilson", "Wilson" }, { "Wilton", "Wilton" },
  			    { "Windham", "Windham" }, { "Windsor", "Windsor" }, { "Wingdale", "Wingdale" }, { "Winthrop", "Winthrop" }, { "Witherbee", "Witherbee" },
  			    { "Wolcott", "Wolcott" }, { "Woodbourne", "Woodbourne" }, { "Woodbury", "Woodbury" }, { "Woodgate", "Woodgate" }, { "Woodhaven", "Woodhaven" },
  			    { "Woodhull", "Woodhull" }, { "Woodmere", "Woodmere" }, { "Woodridge", "Woodridge" }, { "Woodside", "Woodside" }, { "Woodstock", "Woodstock" },
  			    { "Woodville", "Woodville" }, { "Worcester", "Worcester" }, { "Wurtsboro", "Wurtsboro" }, { "Wyandanch", "Wyandanch" }, { "Wykagyl", "Wykagyl" },
  			    { "Wynantskill", "Wynantskill" }, { "Wyoming", "Wyoming" }, { "Yaphank", "Yaphank" }, { "Yonkers", "Yonkers" }, { "York", "York" }, { "Yorkshire",
  			    "Yorkshire" }, { "Yorktown Heights", "Yorktown Heights" }, { "Yorkville", "Yorkville" }, { "Youngstown", "Youngstown" }, { "Youngsville", "Youngsville" },
  			    { "Yulan", "Yulan"}
		};

		return createEnumResponseValidator(keyValues);
	}
  	
  	private ResponseSpecification getAllCountryResponseValidator()
	{
  		final String[][] keyValues = 
  		{ 
			 { "AD", "Andorra" }, { "AE", "United Arab Emirates" }, { "AF", "Afghanistan" }, { "AG", "Antigua And Barbuda" }, { "AI", "Anguilla" },
			 { "AL", "Albania" }, { "AM", "Armenia" }, { "AN", "Netherlands Antilles" }, { "AO", "Angola" }, { "AQ", "Antarctica" }, { "AR", "Argentina" },
			 { "AT", "Austria" }, { "AU", "Australia" }, { "AW", "Aruba" }, { "AX", "And Islands" }, { "AZ", "Azerbaijan" }, { "BA", "Bosnia And Herzegovina" },
			 { "BB", "Barbados" }, { "BD", "Bangladesh" }, { "BE", "Belgium" }, { "BF", "Burkina Faso" }, { "BG", "Bulgaria" }, { "BH", "Bahrain" }, 
			 { "BI", "Burundi" }, { "BJ", "Benin" }, { "BM", "Bermuda" }, { "BN", "Brunei Darussalam" }, { "BO", "Bolivia" }, { "BR", "Brazil" }, 
			 { "BS", "Bahamas" }, { "BT", "Bhutan" }, { "BV", "Bouvet Island" }, { "BW", "Botswana" }, { "BY", "Belarus" }, { "BZ", "Belize" },
			 { "CA", "Canada" }, { "CC", "Cocos (keeling) Islands" }, { "CD", "Congo, Democratic Rep Of" }, { "CF", "Central African Republic" },
			 { "CG", "Congo" }, { "CH", "Switzerland" }, { "CI", "Cote D'ivoire" }, { "CK", "Cook Islands" }, { "CL", "Chile" }, { "CM", "Cameroon" },
			 { "CN", "China" }, { "CO", "Colombia" }, { "CR", "Costa Rica" }, { "CS", "Serbia And Montenegro" }, { "CV", "Cape Verde" }, { "CX", "Christmas Island" },
			 { "CY", "Cyprus" }, { "CZ", "Czech Republic" }, { "DE", "Germany" }, { "DJ", "Djibouti" }, { "DK", "Denmark" }, { "DM", "Dominica" }, 
			 { "DO", "Dominican Republic" }, { "DZ", "Algeria" }, { "EC", "Ecuador" }, { "EE", "Estonia" }, { "EG", "Egypt" }, { "EH", "Western Sahara" },
			 { "ER", "Eritrea" }, { "ES", "Spain" }, { "ET", "Ethiopia" }, { "FI", "Finland" }, { "FJ", "Fiji" }, { "FK", "Falkland Islands" }, 
			 { "FM", "Micronesia, Fed States Of" }, { "FO", "Faroe Islands" }, { "FR", "France" }, { "GA", "Gabon" }, { "GB", "United Kingdom" }, 
			 { "GD", "Grenada" }, { "GE", "Georgia" }, { "GF", "French Guiana" }, { "GH", "Ghana" }, { "GI", "Gibraltar" }, { "GL", "Greenland" }, 
			 { "GM", "Gambia" }, { "GN", "Guinea" }, { "GP", "Guadeloupe" }, { "GQ", "Equatorial Guinea" }, { "GR", "Greece" }, { "GS", "S Georgia-s Sandwich Isls" },
			 { "GT", "Guatemala" }, { "GW", "Guinea-bissau" }, { "GY", "Guyana" }, { "HK", "Hong Kong" }, { "HM", "Heard Isl-mcdonald Isl" }, 
			 { "HN", "Honduras" }, { "HR", "Croatia" }, { "HT", "Haiti" }, { "HU", "Hungary" }, { "ID", "Indonesia" }, { "IE", "Ireland" },
			 { "IL", "Israel" }, { "IN", "India" }, { "IO", "British Indian Ocean Terr" }, { "IQ", "Iraq" }, { "IS", "Iceland" }, { "IT", "Italy" }, { "JM", "Jamaica" },
			 { "JO", "Jordan" }, { "JP", "Japan" }, { "KE", "Kenya" }, { "KG", "Kyrgyzstan" }, { "KH", "Cambodia" }, { "KI", "Kiribati" }, { "KM", "Comoros" }, 
			 { "KN", "Saint Kitts And Nevis" }, { "KR", "Korea, Republic Of" }, { "KW", "Kuwait" }, { "KY", "Cayman Islands" }, { "KZ", "Kazakhstan" }, 
			 { "LA", "Lao People's Dem Rep" }, { "LB", "Lebanon" }, { "LC", "Saint Lucia" }, { "LI", "Liechtenstein" }, { "LK", "Sri Lanka" }, { "LR", "Liberia" }, 
			 { "LS", "Lesotho" }, { "LT", "Lithuania" }, { "LU", "Luxembourg" }, { "LV", "Latvia" }, { "MA", "Morocco" }, { "MC", "Monaco" }, { "MD", "Moldova, Republic Of" },
			 { "ME", "Montenegro" }, { "MG", "Madagascar" },  { "MH", "Marshall Islands" }, { "MK", "Macedonia, The Fyr Of" }, { "ML", "Mali" }, { "MN", "Mongolia" }, 
			 { "MO", "Macao" }, { "MQ", "Martinique" },  { "MR", "Mauritania" }, { "MS", "Montserrat" }, { "MT", "Malta" }, { "MU", "Mauritius" }, { "MV", "Maldives" },
			 { "MW", "Malawi" }, { "MX", "Mexico" }, { "MY", "Malaysia" }, { "MZ", "Mozambique" }, { "NA", "Namibia" }, { "NC", "New Caledonia" }, { "NE", "Niger" }, 
			 { "NF", "Norfolk Island" }, { "NG", "Nigeria" }, { "NI", "Nicaragua" }, { "NL", "Netherlands" }, { "NO", "Norway" }, { "NP", "Nepal" }, { "NR", "Nauru" },
			 { "NU", "Niue" }, { "NZ", "New Zealand" }, { "OM", "Oman" }, { "PA", "Panama" }, { "PE", "Peru" }, { "PF", "French Polynesia" }, { "PG", "Papua New Guinea" }, 
			 { "PH", "Philippines" }, { "PK", "Pakistan" }, { "PL", "Poland" }, { "PM", "Saint Pierre And Miquelon" }, { "PN", "Pitcairn" }, { "PR", "Puerto Rico" }, 
			 { "PS", "Palestinian Territory" }, { "PT", "Portugal" }, { "PW", "Palau" }, { "PY", "Paraguay" }, { "QA", "Qatar" }, { "RE", "Reunion" }, { "RO", "Romania" }, 
			 { "RS", "Serbia" }, { "RU", "Russian Federation" }, { "RW", "Rwanda" }, { "SA", "Saudi Arabia" }, { "SB", "Solomon Islands" }, { "SC", "Seychelles" }, 
			 { "SE", "Sweden" }, { "SG", "Singapore" }, { "SH", "Saint Helena" }, { "SI", "Slovenia" }, { "SJ", "Svalbard And Jan Mayen" }, { "SK", "Slovakia" }, 
			 { "SL", "Sierra Leone" }, { "SM", "San Marino" }, { "SN", "Senegal" }, { "SO", "Somalia" }, { "SR", "Suriname" }, { "ST", "Sao Tome And Principe" }, 
			 { "SV", "El Salvador" }, { "SZ", "Swaziland" }, { "TC", "Turks And Caicos Islands" }, { "TD", "Chad" }, { "TF", "French Southern Terr" }, { "TG", "Togo" },
			 { "TH", "Thailand" }, { "TJ", "Tajikistan" }, { "TK", "Tokelau" }, { "TL", "Timor-leste" }, { "TM", "Turkmenistan" }, { "TN", "Tunisia" }, { "TO", "Tonga" },
			 { "TR", "Turkey" }, { "TT", "Trinidad And Tobago" }, { "TV", "Tuvalu" }, { "TW", "Taiwan" }, { "TZ", "Tanzania, United Rep Of" }, { "UA", "Ukraine" },
			 { "UG", "Uganda" }, { "US", "United States Of America" }, { "UY", "Uruguay" }, { "UZ", "Uzbekistan" }, { "VA", "Holy See (vatican)" }, 
			 { "VC", "St Vincent-grenadines" }, { "VE", "Venezuela" }, { "VG", "Virgin Islands, British" }, { "VN", "Viet Nam" }, { "VU", "Vanuatu" }, 
			 { "WF", "Wallis And Futuna" }, { "WS", "Samoa" }, { "YE", "Yemen" }, { "YT", "Mayotte" }, { "ZA", "South Africa" }, { "ZM", "Zambia" }, { "ZW", "Zimbabwe" } 
  		};	
  		return createEnumResponseValidator(keyValues);
	}
  	
  	private ResponseSpecification getCountrySchoolsResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("schoolUCN[0]",equalTo("600222805"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("I_CD"))
		.expectBody("address1[0]",equalTo("PO BOX 6106"))
		.expectBody("name[0]",equalTo("AMER INTL SCH DHAKA"))
		.expectBody("spsId[0]",equalTo("22915979"))

		.expectBody("schoolUCN[1]",equalTo("600340507"))
		.expectBody("schoolStatus[1]",equalTo("1"))
		.expectBody("groupType[1]",equalTo("I_CD"))
		.expectBody("address1[1]",equalTo("39 FAZLUL KADER RD"))
		.expectBody("name[1]",equalTo("CHITTAGONG GRAMMAR SCHOOL"))
		.expectBody("spsId[1]",equalTo("22939847"));
		return rspec.build();			
	}
  	
  	private ResponseSpecification getAllTeachersResponseValidator()
	{
  		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
  		.expectBody("password[0]",equalTo("eGUxJRHmNXc=\n"))
		.expectBody("grades[0]",hasItem("03"))
		.expectBody("schoolId[0]",equalTo("148551"))
		.expectBody("userType[0]",hasItem("TEACHER"))
		.expectBody("isEducator[0]",equalTo("1"))
		.expectBody("ucn[0]",equalTo("604157945"))
		.expectBody("schoolUcn[0]",equalTo("600078934"))
		.expectBody("enterpriseEmail[0]",equalTo("SHERAL@OPTONLINE.NET"))
		.expectBody("email[0]",equalTo("SLANIN327@AOL.COM"))
		.expectBody("firstName[0]",equalTo("SHERRY"))
		.expectBody("lastName[0]",equalTo("LANIN"))
		.expectBody("orgZip[0]",equalTo("11004"))
		.expectBody("zip[0]",equalTo("11004"))
		.expectBody("positions[0]",hasItem("10000"))
		.expectBody("level1RegistrationDate[0]",equalTo("Oct 27, 2005, 0:05 PM (EDT)"))
		.expectBody("level2RegistrationDate[0]",equalTo("Oct 27, 2005, 0:05 PM (EDT)"))
		.expectBody("lastmodifiedDate[0]",equalTo("Mar 01, 2007, 7:41 PM (EST)"))
		.expectBody("isEnabled[0]",equalTo("1"))
		.expectBody("subscriptionEducator[0]",equalTo("Y"))
		.expectBody("bonusPointsYtd[0]",equalTo("133"))
		.expectBody("bonusPoints[0]",equalTo("680"))
		.expectBody("delivStatus[0]",equalTo("000"))
		.expectBody("userName[0]",equalTo("Lanin"))
		.expectBody("bcoe[0]",equalTo("0525601258"))
		.expectBody("spsId[0]",equalTo("1910382"))
		.expectBody("note[0]" ,hasItems("Lanin|myAccountApp|Oct 27, 2005, 0:05:34 PM (EDT)|SYS: Creation", 
										"Lanin|messageApp|Oct 27, 2005, 0:06:21 PM (EDT)|SYS: Modification: E-mail Address\n\t\t\t\n\t\t",
										"Lanin|CSIMigration|Dec 22, 2005, 3:15:44 AM (EST)|SYS: User Info, Org Info", 
										"1910382|csiMessageApp|Feb 20, 2006, 0:40:54 AM (EST)|SYS: Modification: Enterprise E-mail\n\t\t",
										"1910382|csiMessageApp|Feb 20, 2006, 0:40:59 AM (EST)|SYS: Modification: Enterprise E-mail\n\t\t",
										"1910382|scsToolsApp|Aug 18, 2006, 1:55:43 AM (EDT)|SYS: Modification:<br>User Type",
										"1910382|scsToolsApp|Feb 27, 2007, 4:37:27 PM (EST)|SYS: Modification:<br>Bonus Points<br>Bonus Points YTD<br>Corporate Status",
										"1910382|scsToolsApp|Mar 01, 2007, 1:40:06 AM (EST)|SYS: Modification:<br>Bonus Points<br>Bonus Points YTD",
										"1910382|scsToolsApp|Mar 01, 2007, 7:41:40 PM (EST)|SYS: Modification:<br>Bonus Points<br>Bonus Points YTD"));
										
		
		return rspec.build();
	}
  	
  	/*private ResponseSpecification getAlternateTeacherResponseValidator()
	{
		
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("modifiedDate[0]",equalTo("20141215"))
		.expectBody("password[0]",equalTo("iexecxGaERY="))
		.expectBody("readingLevel[0]",equalTo("20"))
		.expectBody("salutation[0]",equalTo("MR"))
		.expectBody("grades[0]",hasItems("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "98", "99"))
		.expectBody("schoolId[0]",equalTo("348537"))
		.expectBody("cac[0]",equalTo("LVPZ6"))
		.expectBody("userType[0]",hasItem("TEACHER"))
		.expectBody("isEducator[0]",equalTo("1"))
		.expectBody("cacId[0]",equalTo("8418468"))
		.expectBody("isIdUsed[0]",equalTo("2"))
		.expectBody("termsConditionsAcceptedVersion[0]",equalTo("1.2"))
		.expectBody("termsConditionsAcceptedTimestamp[0]",equalTo("Jul 23, 2014, 3:38 PM (EDT)"))
		.expectBody("mentorTeacher[0]",equalTo("0"))
		.expectBody("usedBookclubs[0]",equalTo("0"))
		.expectBody("gradeClasssizeId[0]",hasItems("775051", "775060", "775062", "775061", "662341", "775059", "775058", "775057", "775056", "775055", "775063", "775054", "775053", "775052"))
		.expectBody("firstLoginSchoolYear[0]",equalTo("1"))
		.expectBody("ucn[0]",equalTo("632162938"))
		.expectBody("schoolUcn[0]",equalTo("600078822"))
		.expectBody("email[0]",equalTo("jtoad@juno1.com"))
		.expectBody("firstName[0]",equalTo("James"))
		.expectBody("lastName[0]",equalTo("Toad"))
		.expectBody("corpStatus[0]",isEmptyString())
		.expectBody("privacyPolicyAcceptedVersion[0]",equalTo("1.2"))
		.expectBody("orgZip[0]",equalTo("11372"))
		.expectBody("positions[0]",hasItem("10025"))
		.expectBody("schlAppUserType[0]",hasItem("2"))
		.expectBody("emailCaptureDate[0]",equalTo("2014-07-23"))
		.expectBody("level1RegistrationDate[0]",equalTo("Jul 23, 2014, 3:38 PM (EDT)"))
		.expectBody("lastmodifiedDate[0]",equalTo("Dec 15, 2014, 4:34 PM (EST)"))
		.expectBody("altTeacher[0]",equalTo("1"))
		.expectBody("bcoe[0]",equalTo("1391586128"))
		.expectBody("enterpriseEmail[0]",equalTo("JTOAD@JUNO1.COM"))
		.expectBody("isEnabled[0]",equalTo("1"))
		.expectBody("newTeacher[0]",equalTo("0"))
		.expectBody("registrationDate[0]",equalTo("Jul 23, 2014, 3:38 PM (EDT)"))
		.expectBody("privacyPolicyAcceptedTimestamp[0]",equalTo("Jul 23, 2014, 3:38 PM (EDT)"))
		.expectBody("userName[0]",equalTo("jtoad@juno1.com"))
		.expectBody("spsId[0]",equalTo("84247169"))

		.expectBody("modifiedDate[1]",equalTo("20140731"))
		.expectBody("password[1]",equalTo("iexecxGaERY="))
		.expectBody("readingLevel[1]",equalTo("20"))
		.expectBody("salutation[1]",equalTo("MR"))
		.expectBody("grades[1]",hasItems("98", "04", "05", "99", "01", "02", "03"))
		.expectBody("schoolId[1]",equalTo("348537"))
		.expectBody("cac[1]",equalTo("LX8YV"))
		.expectBody("userType[1]",hasItem("TEACHER"))
		.expectBody("isEducator[1]",equalTo("1"))
		.expectBody("cacId[1]",equalTo("8448349"))
		.expectBody("isIdUsed[1]",equalTo("2"))
		.expectBody("termsConditionsAcceptedVersion[1]",equalTo("1.2"))
		.expectBody("termsConditionsAcceptedTimestamp[1]",equalTo("Jul 31, 2014, 0:33 PM (EDT)"))
		.expectBody("alsoParent[1]",equalTo("1"))
		.expectBody("mentorTeacher[1]",equalTo("1"))
		.expectBody("usedBookclubs[1]",equalTo("1"))
		.expectBody("gradeClasssizeId[1]",hasItems("693160", "693161", "693156", "693157", "693155", "693158", "693159"))
		.expectBody("firstLoginSchoolYear[1]",equalTo("1"))
		.expectBody("ucn[1]",equalTo("632192109"))
		.expectBody("schoolUcn[1]",equalTo("600078822"))
		.expectBody("email[1]",equalTo("jplavani@juno1.com"))
		.expectBody("firstName[1]",equalTo("James"))
		.expectBody("lastName[1]",equalTo("Plavani"))
		.expectBody("corpStatus[1]",isEmptyString())
		.expectBody("privacyPolicyAcceptedVersion[1]",equalTo("1.2"))
		.expectBody("birthMonth[1]",equalTo("03"))
		.expectBody("birthDay[1]",equalTo("17"))
		.expectBody("orgZip[1]",equalTo("11372"))
		.expectBody("positions[1]",hasItems("10027", "10028", "10025", "10026", "10023", "10024", "10029"))
		.expectBody("schlAppUserType[1]",hasItems("2", "1"))
		.expectBody("emailCaptureDate[1]",equalTo("2014-07-31"))
		.expectBody("level1RegistrationDate[1]",equalTo("Jul 31, 2014, 0:33 PM (EDT)"))
		.expectBody("lastmodifiedDate[1]",equalTo("Jul 31, 2014, 1:53 PM (EDT)"))
		.expectBody("altTeacher[1]",equalTo("1"))
		.expectBody("bcoe[1]",equalTo("1393826423"))
		.expectBody("enterpriseEmail[1]",equalTo("JPLAVANI@JUNO1.COM"))
		.expectBody("isEnabled[1]",equalTo("1"))
		.expectBody("newTeacher[1]",equalTo("1"))
		.expectBody("registrationDate[1]",equalTo("Jul 31, 2014, 0:33 PM (EDT)"))
		.expectBody("privacyPolicyAcceptedTimestamp[1]",equalTo("Jul 31, 2014, 0:33 PM (EDT)"))
		.expectBody("userName[1]",equalTo("jplavani@juno1.com"))
		.expectBody("spsId[1]",equalTo("84283978"));
		return rspec.build();
	}*/
  	
  	private ResponseSpecification getSchoolByZipcodeResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("creditCardState[0]",equalTo("NY"))
		.expectBody("zipcode[0]",equalTo("11431"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("EC_PS"))
		.expectBody("address1[0]",equalTo("PO BOX 311215"))
		.expectBody("city[0]",equalTo("JAMAICA"))
		.expectBody("schoolUCN[0]",equalTo("610166776"))
		.expectBody("name[0]",equalTo("EARLY SUNRISE PRE-SCHOOL AND KINDERGARTEN II"))
		.expectBody("state[0]",equalTo("NY"))
		.expectBody("spsId[0]",equalTo("22948476"));
		return rspec.build();
	}
  	
  	private ResponseSpecification getSchoolByCityStateResponseValidator()
	
	{ 
  		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("creditCardState[0]",equalTo("NY"))
		.expectBody("zipcode[0]",equalTo("13731"))
		.expectBody("schoolStatus[0]",equalTo("1"))
		.expectBody("groupType[0]",equalTo("S_EH"))
		.expectBody("address1[0]",equalTo("85 DELEWARE AVE"))
		.expectBody("city[0]",equalTo("ANDES"))
		.expectBody("schoolUCN[0]",equalTo("600077058"))
		.expectBody("name[0]",equalTo("ANDES CENTRAL ELEM-HIGH SCHOOL"))
		.expectBody("state[0]",equalTo("NY"))
		.expectBody("spsId[0]",equalTo("159324"))
		
		.expectBody("creditCardState[1]",equalTo("NY"))
		.expectBody("zipcode[1]",equalTo("13731"))
		.expectBody("schoolStatus[1]",equalTo("1"))
		.expectBody("groupType[1]",equalTo("D_SL"))
		.expectBody("address1[1]",equalTo("85 DELAWARE AVE"))
		.expectBody("city[1]",equalTo("ANDES"))
		.expectBody("schoolUCN[1]",equalTo("600009891"))
		.expectBody("name[1]",equalTo("ANDES CENTRAL SCHOOL DISTRICT"))
		.expectBody("state[1]",equalTo("NY"))
		.expectBody("spsId[1]",equalTo("416842"));
		return rspec.build();
	}
  	
  	private ResponseSpecification getPrimarySchoolAddressResponseValidator()
		
	{ 
  		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("addressTypeCode",equalTo("05"))
		.expectBody("uspsTypeCode",equalTo("2"))
		.expectBody("borderId",equalTo("11372050"))
		.expectBody("creditCardState",equalTo("NY"))
		.expectBody("zipcode",equalTo("11372"))
		//.expectBody("poBox",equalTo("0"))
		.expectBody("schoolStatus",equalTo("1"))
		.expectBody("groupType",equalTo("S_6E"))
		.expectBody("internatIonalDomestIcCode",equalTo("01"))
		.expectBody("publicPrivateKey",equalTo("01"))
		.expectBody("reportingSchoolType",equalTo("0"))
		.expectBody("createDate",equalTo("2004-05-22 10:20:37.371186"))
		.expectBody("createdSource",equalTo("DATA DUMP"))
		.expectBody("address1",equalTo("7702 37TH AVE"))
		.expectBody("phone",equalTo("7184247700|0"))
		.expectBody("city",equalTo("JACKSON HTS"))
		.expectBody("country",equalTo("US"))
		.expectBody("schoolUCN",equalTo("600078822"))
		.expectBody("name",equalTo("PS 069 JACKSON HEIGHTS SCHOOL"))
		.expectBody("state",equalTo("NY"))
		.expectBody("spsId",equalTo("348537"));
		return rspec.build();
	}
  	
  	private ResponseSpecification getExistingShippingAddressResponseValidator(  )
		
	{ 
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("spsId",equalTo("84780649"))
		.rootPath("objectList")
		.expectBody("id[0]",equalTo("1"))
		.expectBody("addressBookZip[0]",equalTo("11372"))
		.expectBody("isHome[0]",equalTo("0"))
		.expectBody("addressBookNickName[0]",equalTo("Home"))
		.expectBody("addressBookAddress1[0]",equalTo("74-12 35th Street"))
		.expectBody("addressBookAddress2[0]",isEmptyString())
		.expectBody("addressBookAddress3[0]",isEmptyString())
		.expectBody("addressBookAddress4[0]",isEmptyString())
		.expectBody("addressBookAddress5[0]",isEmptyString())
		.expectBody("addressBookFirstName[0]",equalTo("Laura"))
		.expectBody("addressBookLastName[0]",equalTo("Bronson"))
		.expectBody("addressBookCity[0]",equalTo("Jackson Heights"))
		.expectBody("addressBookState[0]",equalTo("NY"))
		.expectBody("addressBookPhone[0]",equalTo("7189916585|"))
		.expectBody("addressBookCountry[0]",equalTo("US"));
		return rspec.build();
						
	}

  	
  	private ResponseSpecification getOnlyTeachersResponseValidator()
	
	{
  		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("[0].modifiedDate",equalTo("20160510"))
		.expectBody("password[0]",equalTo("kgGL+xsw4gg="))
		.expectBody("readingLevel[0]",equalTo("20"))
		.expectBody("salutation[0]",equalTo("MR"))
		.expectBody("grades[0]",hasItem("04"))
		.expectBody("schoolId[0]",equalTo("411785"))
		.expectBody("cac[0]",equalTo("KVBTZ"))
		.expectBody("userType[0]",hasItem("TEACHER"))
		.expectBody("isEducator[0]",equalTo("1"))
		.expectBody("cacId[0]",equalTo("7878896"))
		.expectBody("isIdUsed[0]",equalTo("2"))
		.expectBody("termsConditionsAcceptedVersion[0]",equalTo("1.4"))
		.expectBody("termsConditionsAcceptedTimestamp[0]",equalTo("May 10, 2016, 2:57 PM (EDT)"))
		.expectBody("mentorTeacher[0]",equalTo("1"))
		.expectBody("usedBookclubs[0]",equalTo("0"))
		.expectBody("gradeClasssizeId[0]",hasItem("168946"))
		.expectBody("bcoe[0]",isEmptyString())
		.expectBody("schlAppUserType[0]",hasItem("2"))
		.expectBody("schoolUcn[0]",equalTo("600079371"))
		.expectBody("email[0]",equalTo("lgreen@juno1.com"))
		.expectBody("firstName[0]",equalTo("Larry"))
		.expectBody("lastName[0]",equalTo("Green"))
		.expectBody("birthMonth[0]",equalTo("04"))
		.expectBody("birthDay[0]",equalTo("18"))
		.expectBody("privacyPolicyAcceptedVersion[0]",equalTo("1.4"))
		.expectBody("orgZip[0]",equalTo("11426"))
		.expectBody("positions[0]",hasItem("10028"))
		.expectBody("emailCaptureDate[0]",equalTo("2016-05-10"))
		.expectBody("level1RegistrationDate[0]",equalTo("May 10, 2016, 2:57 PM (EDT)"))
		.expectBody("lastmodifiedDate[0]",equalTo("May 10, 2016, 3:12 PM (EDT)"))
		.expectBody("isEnabled[0]",equalTo("1"))
		.expectBody("newTeacher[0]",equalTo("0"))
		.expectBody("registrationDate[0]",equalTo("May 10, 2016, 2:57 PM (EDT)"))
		.expectBody("privacyPolicyAcceptedTimestamp[0]",equalTo("May 10, 2016, 2:57 PM (EDT)"))
		.expectBody("userName[0]",equalTo("lgreen@juno1.com"))
		.expectBody("spsId[0]",equalTo("84907629"));
		return rspec.build();
	}
  	
  	private ResponseSpecification getAllGradeClassSizeResponseValidator()
  	{
  		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("spsUserId",hasItem("84907629"))
		.expectBody("spsId",hasItem("168946"))
		.expectBody("classSize",hasItem("22"))
		.expectBody("grade",hasItem("04"));		
		return rspec.build();
	}
  	
  	private ResponseSpecification getCreditCardWalletResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("count",equalTo(1))
		.rootPath("wallet")
		.expectBody("id[0]",equalTo("0"))
		.expectBody("ccmonth[0]",equalTo("12"))
		.expectBody("cbspt[0]",equalTo("0109744799771111"))
		.expectBody("cnm[0]",equalTo("xxxxxxxxxxxx1111"))
		.expectBody("phoneExtNumber[0]",isEmptyString())
		.expectBody("email[0]",isEmptyString())
		.expectBody("zip[0]",equalTo("11372"))
		.expectBody("firstName[0]",equalTo("Laura"))
		.expectBody("address1[0]",equalTo("74-12 35th Street"))
		.expectBody("address2[0]",isEmptyString())
		.expectBody("ccBrand[0]",equalTo("V"))
		.expectBody("phoneNumber[0]",equalTo("7186616565"))
		.expectBody("lastName[0]",equalTo("Bronson"))
		.expectBody("ccnumber[0]",equalTo("xxxxxxxxxxxx1111"))
		.expectBody("city[0]",equalTo("Jackson Heights"))
		.expectBody("success[0]",equalTo("test"))
		.expectBody("default[0]",equalTo("false"))
		.expectBody("state[0]",equalTo("NY")); 
		return rspec.build();
	}
  	
  	private ResponseSpecification getExistingAssociatedSocialAccountsResponseValidator()
		
	{
  		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("[0]",equalTo("facebook"))
		.expectBody("[1]",equalTo("twitter"));
		return rspec.build();
	}
  	
  	private ResponseSpecification getChildUsersResponseValidator()
	
	{
		ResponseSpecBuilder rspec=new ResponseSpecBuilder()
		.expectBody("birthYear",equalTo("2007"))
		.expectBody("isLinkedToALtTeacher",equalTo("0"))
		.expectBody("firstName",equalTo("Ron"))
		.expectBody("lastName",equalTo("West"))
		.expectBody("parentIds",hasItem("84907630"))
		.expectBody("teacherIds",hasItem(""))
		.expectBody("birthMonth",equalTo("04"))
		.expectBody("classroomTeacher",isEmptyString())
		.expectBody("isFat",equalTo("1"))
		.expectBody("gender",equalTo("M"))
		.expectBody("grade",equalTo("04"))
		.expectBody("spsId",equalTo("156551"));
		return rspec.build();
	}


		
}
