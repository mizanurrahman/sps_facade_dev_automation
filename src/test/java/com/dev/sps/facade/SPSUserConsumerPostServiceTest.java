package com.dev.sps.facade;

/***************************************************************************************************************************
 ***  @@Author
 ***  Mohammed Rahman
 ***  This Tests Will Be Using To Test Facade Consumer Services.
 ***************************************************************************************************************************/

import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;


public class SPSUserConsumerPostServiceTest extends BaseTest 
{
		//private String title="MR";
		private String firstName="Bill";
		private String lastName="White";
		private String email;
		private String password="passed1";
		private String childfirstName="Alan";
		private String childlastName="White";
		private String childbirthYear="2005";
		private String childbirthMonth="08";
		private String childbirthDay="26";
		private String childGrade="05";
		private String schoolId="163392";
		private String childSPSID;
		private boolean firstYearTeaching = true;
		private List<String> teacherIds=new ArrayList<String>();
		private List<String> parentIds=new ArrayList<String>();
		private List<String> userType=new ArrayList<String>();
		private String consumerSPSID;
		private String displayName="TukTukBoy";
		private List<String> interesParent=new ArrayList<String>();
		private List<String> readinglevelIds=new ArrayList<String>();
		private List<String> interestIds=new ArrayList<String>();
		private String parentPrimaryRole="2";
		
		//End Points
		private static final String ENDPOINT_CONSUMER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CONSUMER_GET="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CONSUMER_UPDATE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CONSUMER_DELETE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_TEACHER_DELETE="/spsuser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CONSUMER_ADDCHILD="/spschilduser/?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CONSUMER_GETCHILD="/spschilduser/{spsId}?clientId=SPSFacadeAPI";
		private static final String ENDPOINT_CONSUMER_UPGRADETEACHER="/spsuser/{spsId}/upgradetoeducator?clientId=SCSToolsApp";
		
		
	@Test
		public void createConsumerGetTest()
		{
			System.out.println("#####################################################################################");
			System.out.println("******************** Light Consumer Registration and Get Consumer********************");
			System.out.println("#####################################################################################");
		
		
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		

			consumerSPSID=createConsumerResponse.path("spsId");	
			System.out.println(consumerSPSID);
		
			// Get Consumer
			ExtractableResponse<Response> getConsumerResponse=
									given()
											.pathParam("spsId",consumerSPSID).
									when()
											.get(ENDPOINT_CONSUMER_GET).
									then()
											.statusCode(200)
											.extract();		
			System.out.println(getConsumerResponse.asString());
		}
	
	@Test
		public void createConsumerUpdateGetTest()
		{			
			System.out.println("##################################################################################################");
			System.out.println("********************  Light Consumer Registration plus Update and Get Consumer********************");
			System.out.println("##################################################################################################");
					
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		
			
			consumerSPSID=createConsumerResponse.path("spsId");
			System.out.println(consumerSPSID);
	
			// Update Consumer 
									given()
											.log().all()
											.pathParam("spsId",consumerSPSID)
											.contentType("application/json")
											.body(updateConsumerPayload()).
									when()
											.put(ENDPOINT_CONSUMER_UPDATE).
									then()
											.statusCode(200)
											.extract();	
		
			// Get Consumer
			ExtractableResponse<Response> getConsumerResponse=
									given()
											.pathParam("spsId",consumerSPSID).
									when()
											.get(ENDPOINT_CONSUMER_GET).
									then()
											.statusCode(200)
											.spec(createConsumerUpdateResponseValidator())
											.extract();		
			System.out.println(getConsumerResponse.asString());
		}
	
	@Test
		public void createConsumerDeleteTest()
		{	
			System.out.println("########################################################################################");
			System.out.println("******************** Light Consumer Registration and Delete Consumer********************");
			System.out.println("########################################################################################");
		

			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		

			consumerSPSID=createConsumerResponse.path("spsId");	
			System.out.println(consumerSPSID);
		
			// Delete Consumer 
			ExtractableResponse<Response> deleteConsumerResponse=
									given()
											.pathParam("spsId",consumerSPSID).
									when()
											.delete(ENDPOINT_CONSUMER_DELETE).
									then()
											.statusCode(200)
											.extract();		
			System.out.println(deleteConsumerResponse.asString());
		}
	
	@Test
		public void createConsumerAddChildGetChildTest()
	
		{	
			System.out.println("##################################################################################################");
			System.out.println("******************** Light Consumer Registration, Add Child and Get Child Info********************");
			System.out.println("##################################################################################################");
		
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");	
			System.out.println(consumerSPSID);
		
			// Add Child
			ExtractableResponse<Response> createAddChildResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(addChildPayload()).
									when()
											.post(ENDPOINT_CONSUMER_ADDCHILD).
									then()
											.statusCode(201)
											.spec(addChildResponseValidator())
											.extract();	
			childSPSID=createAddChildResponse.path("spsId");	
			System.out.println(childSPSID);
	
			// Get Child
			ExtractableResponse<Response> getChildResponse=
									given()
											.pathParam("spsId",childSPSID).
									when()
											.get(ENDPOINT_CONSUMER_GETCHILD).
									then()
											.statusCode(200)
											.extract();		
			System.out.println(getChildResponse.asString());
		
		}
	
	@Test
		public void createConsumerAddChildDeleteTest()
		{
			System.out.println("###################################################################################################");
			System.out.println("******************** Light Consumer Registration, Add Child and Delete Consumer********************");
			System.out.println("###################################################################################################");
			

			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");	
			System.out.println(consumerSPSID);
		
			// Add Child
			ExtractableResponse<Response> createAddChildResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(addChildPayload()).
									when()
											.post(ENDPOINT_CONSUMER_ADDCHILD).
									then()
											.statusCode(201)
											.spec(addChildResponseValidator())
											.extract();	
			childSPSID=createAddChildResponse.path("spsId");	
			System.out.println(childSPSID);	
		
			// Delete Consumer with the Child 
			ExtractableResponse<Response> deleteConsumerResponse=
									given()
											.pathParam("spsId",consumerSPSID).
									when()
											.delete(ENDPOINT_CONSUMER_DELETE).
									then()
											.statusCode(200)
											.extract();		
			System.out.println(deleteConsumerResponse.asString());
		
		}
	
	@Test
		public void createConsumerUpgradeToTeacherandGetTest()
		{
			System.out.println("############################################################################################################");
			System.out.println("******************** Light Consumer Registration plus Upgrade to Teacher and Get Teacher********************");
			System.out.println("############################################################################################################");
					
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");
			System.out.println(consumerSPSID);
	
			// Upgrade Consumer to Teacher
			ExtractableResponse<Response> upgradeConsumerResponse=
									given()
											.log().all()
											.pathParam("spsId",consumerSPSID)
											.contentType("application/json")
											.body(upgradeConsumerToEducatorPayload()).
									when()
											.put(ENDPOINT_CONSUMER_UPGRADETEACHER).
									then()
											.statusCode(200)
											.extract();	
		
			System.out.println("@@@@@"+upgradeConsumerResponse.asString());

			// Get Teacher
			ExtractableResponse<Response> getTeacherResponse=
									given()
											.pathParam("spsId",consumerSPSID).
									when()
											.get(ENDPOINT_CONSUMER_GET).
									then()
											.statusCode(200)
											.spec(createConsumerUpgradeResponseValidator())
											.extract();		
			System.out.println(getTeacherResponse.asString());
		}
	
	@Test
		public void createConsumerUpgradeToTeacherandDeleteTest()
		{
			System.out.println("###############################################################################################################");
			System.out.println("******************** Light Consumer Registration plus Upgrade to Teacher and Delete Teacher********************");
			System.out.println("###############################################################################################################");
		
		
			// Light Consumer Registration
			ExtractableResponse<Response> createConsumerResponse=
									given()
											.log().all()
											.contentType("application/json")
											.body(createConsumerPayload()).
									when()
											.post(ENDPOINT_CONSUMER_REGISTRATION).
									then()
											.statusCode(201)
											.spec(createConsumerResponseValidator())
											.extract();		
			consumerSPSID=createConsumerResponse.path("spsId");
			System.out.println(consumerSPSID);
	
			// Upgrade Consumer to Teacher
			ExtractableResponse<Response> upgradeConsumerResponse=
									given()
											.log().all()
											.pathParam("spsId",consumerSPSID)
											.contentType("application/json")
											.body(upgradeConsumerToEducatorPayload()).
									when()
											.put(ENDPOINT_CONSUMER_UPGRADETEACHER).
									then()
											//.statusCode(200)
											//.spec(createConsumerUpgradeResponseValidator())
											.extract();	
		
			System.out.println("@@@@@"+upgradeConsumerResponse.asString());

				
			// Delete Teacher
			ExtractableResponse<Response> deleteTeacherResponse=
									given()
											.pathParam("spsId",consumerSPSID).
									when()
											.delete(ENDPOINT_TEACHER_DELETE).
									then()
											.statusCode(200)
											.extract();		
		System.out.println(deleteTeacherResponse.asString());
		}
	
		private Map<String, Object> createConsumerPayload()
	
		{
			Map<String, Object> consumerRegistrationInfo = new HashMap<String, Object>();
			consumerRegistrationInfo.put("firstName",firstName);
			consumerRegistrationInfo.put("lastName",lastName);
			consumerRegistrationInfo.put("email",getEmail());
			userType.add("CONSUMER");
			consumerRegistrationInfo.put("userType", userType);
			consumerRegistrationInfo.put("password",password);
			consumerRegistrationInfo.put("userName",email);
			return consumerRegistrationInfo;
		}
	
		private Map<String, Object> upgradeConsumerToEducatorPayload()
	
		{
			Map<String, Object> upgradeConsumerToEducatorInfo = new HashMap<String, Object>();
			upgradeConsumerToEducatorInfo.put("schoolId",schoolId);
			Map<String, Object> gradesSize = new HashMap<String, Object>();
			gradesSize.put("98", "19");
			gradesSize.put("05", "21");
			upgradeConsumerToEducatorInfo.put("gradesSize", gradesSize);
			upgradeConsumerToEducatorInfo.put("firstYearTeaching", firstYearTeaching);
			return upgradeConsumerToEducatorInfo;
		}
	
		private Map<String, Object> addChildPayload()
	
		{
			Map<String, Object> addChildInfo = new HashMap<String, Object>();
			addChildInfo.put("firstName",childfirstName);
			addChildInfo.put("lastName",childlastName);
			addChildInfo.put("birthYear",childbirthYear);
			addChildInfo.put("birthMonth",childbirthMonth);
			addChildInfo.put("birthDay",childbirthDay);
			addChildInfo.put("grade",childGrade);
			teacherIds.add("84908157");
			addChildInfo.put("teacherIds", teacherIds);
			parentIds.add(consumerSPSID);
			addChildInfo.put("parentIds", parentIds);
			readinglevelIds.add("100");
			readinglevelIds.add("200");
			addChildInfo.put("readingLevelIds", readinglevelIds);
			interestIds.add("10");
			addChildInfo.put("interestIds", interestIds);
			return addChildInfo;
		}
		private Map<String, Object> updateConsumerPayload()

		{
			Map<String, Object> updateConsumerInfo = new HashMap<String, Object>();
			updateConsumerInfo.put("displayName", displayName);
			interesParent.add("1");
			interesParent.add("2");
			updateConsumerInfo.put("interesParent", interesParent);
			updateConsumerInfo.put("parentPrimaryRole", parentPrimaryRole);
			return updateConsumerInfo;
		}
	
		private ResponseSpecification createConsumerResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("userName",equalTo(email))
			.expectBody("userType",equalTo(userType))
			.expectBody("email",equalTo(email))
			.expectBody("firstName",equalTo(firstName))
			.expectBody("lastName",equalTo(lastName))
			//.expectBody("password",equalTo(password))
			.expectBody("spsId",is(not(empty())));
			return rspec.build();		
		}
	
		private ResponseSpecification createConsumerUpdateResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("spsId",is(not(empty())))
			.expectBody("displayName",equalTo(displayName))
			.expectBody("interesParent",equalTo(interesParent))
			.expectBody("parentPrimaryRole",equalTo(parentPrimaryRole))
			.expectBody("userName",equalTo(email));
			return rspec.build();		
		}
	
		private ResponseSpecification addChildResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("birthYear",equalTo(childbirthYear))
			//.expectBody("modifiedDate",equalTo(""))
			//.expectBody("registrationDate",equalTo(new Date()))
			.expectBody("cac",is(not(empty())))
			.expectBody("firstName",equalTo(childfirstName))
			.expectBody("lastName",equalTo(childlastName))
			.expectBody("lastName",equalTo(childlastName))
			.expectBody("birthMonth",equalTo(childbirthMonth))
			.expectBody("birthDay",equalTo(childbirthDay))
			.expectBody("birthYear",equalTo(childbirthYear))
			.expectBody("grade",equalTo(childGrade))
			//.expectBody("readingLevelIds",hasItems("100", "200"))
			//.expectBody("interestIds",hasItem("10"))
			.expectBody("spsId",is(not(empty())));
			return rspec.build();		
		}	


		private ResponseSpecification createConsumerUpgradeResponseValidator()
	
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("userName",equalTo(email))
			//.expectBody("modifiedDate",equalTo(""))
			//.expectBody("registrationDate",equalTo(new Date()))
			.expectBody("schoolId",equalTo(schoolId))
			.expectBody("orgZip",is(not(empty())))
			//.expectBody("userType",hasItems(userType))
			.expectBody("isEducator",is(not(empty())))
			.expectBody("cac",is(not(empty())))
			.expectBody("cacId",is(not(empty())))
			.expectBody("isIdUsed",is(not(empty())))
			.expectBody("isEnabled",is(not(empty())))
			.expectBody("schoolUcn",is(not(empty())))
			.expectBody("email",equalTo(email))
			.expectBody("firstName",equalTo(firstName))
			.expectBody("lastName",equalTo(lastName))
			//.expectBody("password",equalTo(password))
			.expectBody("spsId",is(not(empty())));
			return rspec.build();		
		}
		private String getEmail()
		{
			email="sps_dev"+String.valueOf(System.currentTimeMillis())+"@sample.com";
			return email;
		}

}
