package com.dev.sps.facade;

import static com.jayway.restassured.RestAssured.given;

//import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.dev.sps.facade.BaseTest;

import java.util.Map;

import static org.hamcrest.Matchers.*;


public class UserRegistrationServicesTest extends BaseTest
{
	
	//Valid User and Password
	private String user1 ="devtestsample707@juno1.com";
	private String password2 ="passed1";
		
	//Registration (less than 7 characters)
	private String firstName ="Barbara";
	private String lastName ="Hill";
	private String email ="bhill@juno1.com";
	private String password3 ="passed";
	private List<String> userType=new ArrayList<String>();
	private String userName ="bhill@juno1.com";
	private String schoolId ="11820623";
	
	//Registration (Only 7 characters)
	private String password4 ="passedx";
	//Registration (Only 7 numbers)
	private String password5 ="1234567";
	//Registration (More than 30 characters)
	private String password6 ="passpasspapasspasspapasspassp123";
	//Registration (With 30 Alpha-numeric characters)
	private String password7 ="passpasspapasspasspapasspassp1";
	//Registration (With 30 Alpha-numeric characters)
	private String password8 ="passed1";
	
	//End Points
	private static final String ENDPOINT_USER_REGISTRATION="/spsuser/?clientId=SPSFacadeAPI";
	//private static final String ENDPOINT_USER_LOGIN="spsuser/login?clientId=SPSFacadeAPI";
	
	@Test
	public void educatorRegistrationPasswordWithLessThan7CharactersTest() 
	{	
		System.out.println("#################################################################################");
		System.out.println("********** Educator Registration With Password Less Than 7 Characters  **********");
		System.out.println("#################################################################################");
		ExtractableResponse<Response> educatorRegistrationLessThan7CharactersResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorWithPasswordLessthan7charactersPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(400)
							.body("errorMessages",hasItem("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							.body("errorStatus",equalTo("BUSINESS_FAILURE"))
							.extract();
		
		System.out.println(educatorRegistrationLessThan7CharactersResponse.asString());
						
		
	}
	
	@Test
	public void educatorRegistrationPasswordWithOnly7CharactersTest() 
	{	
		System.out.println("###########################################################################");
		System.out.println("********** Educator Registration Password Only With 7 Characters  **********");
		System.out.println("############################################################################");
		ExtractableResponse<Response> educatorRegistrationOnly7CharactersResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorWithPasswordOnly7CharactersPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(400)
							.body("errorMessages",hasItem("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							.body("errorStatus",equalTo("BUSINESS_FAILURE"))
							
							.extract();
		
		System.out.println(educatorRegistrationOnly7CharactersResponse.asString());
						
		
	}
	
	@Test
	public void educatorRegistrationPasswordWithOnly7NumbersTest() 
	{	
		System.out.println("#########################################################################");
		System.out.println("********** Educator Registration Password Only With 7 Numbers  **********");
		System.out.println("#########################################################################");
		ExtractableResponse<Response> educatorRegistrationOnly7NumbersResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorWithPasswordOnly7NumbersPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(400)
							.body("errorMessages",hasItem("Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							.body("errorStatus",equalTo("BUSINESS_FAILURE"))
							
							.extract();
		
		System.out.println(educatorRegistrationOnly7NumbersResponse.asString());
			
	}
	
	@Test
	public void educatorRegistrationWithExsistingUserTest() 
	{	
		System.out.println("###############################################################");
		System.out.println("********** Educator Registration with Existing User  **********");
		System.out.println("###############################################################");
		ExtractableResponse<Response> educatorRegistrationWithExistingUserResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorWithExistingUserPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(400)
							.body("errorMessages",hasItem("Email id is already taken."))
							.body("errorStatus",equalTo("BUSINESS_FAILURE"))
							.extract();
		
		System.out.println(educatorRegistrationWithExistingUserResponse.asString());
			
	}
	@Test
	public void educatorRegistrationPasswordWithMoreThan30CharactersTest() 
	{	
		System.out.println("##################################################################################");
		System.out.println("********** Educator Registration Password With More Than 30 Characters  **********");
		System.out.println("##################################################################################");
		ExtractableResponse<Response> educatorRegistrationPasswordWithMoreThan30CharactersResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorPasswordWithMoreThan30CharactersPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(400)
							.body("errorMessages",hasItems("Password can't exceed 30 characters.", "Password must be minimum of 7 characters, should contain at least an alphabetic letter and a number."))
							.body("errorStatus",equalTo("BUSINESS_FAILURE"))
							.extract();
		
		System.out.println(educatorRegistrationPasswordWithMoreThan30CharactersResponse.asString());
			
	}
	
	@Test
	public void educatorRegistrationPasswordWith30AlphaNumericCharactersTest() 
	{	
		System.out.println("######################################################################################");
		System.out.println("********** Educator Registration Password With 30 Alpha-Numeric Characters  **********");
		System.out.println("######################################################################################");
		ExtractableResponse<Response> educatorRegistrationPasswordWith30AlphaNumericCharactersResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorPasswordWith30AlphaNumericCharactersPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(201)
							.body("schoolUcn",equalTo("600076229"))
							.body("schoolId",equalTo("11820623"))
							.extract();
		
		System.out.println(educatorRegistrationPasswordWith30AlphaNumericCharactersResponse.asString());
			
	}
	
	@Test
	public void educatorRegistrationPasswordWith7AlphaNumericCharactersTest() 
	{	
		System.out.println("######################################################################################");
		System.out.println("********** Educator Registration Password With 7 Alpha-Numeric Characters  **********");
		System.out.println("######################################################################################");
		ExtractableResponse<Response> educatorRegistrationPasswordWith7AlphaNumericCharactersResponse=
					given()
							.log().all()
							.contentType("application/json")
							.body(createEducatorPasswordWith7AlphaNumericCharactersPayload()).
					when()
							.post(ENDPOINT_USER_REGISTRATION).
					then()
							.statusCode(201)
							.body("schoolUcn",equalTo("600076229"))
							.body("schoolId",equalTo("11820623"))
							.extract();
		
		System.out.println(educatorRegistrationPasswordWith7AlphaNumericCharactersResponse.asString());
			
	}
	private String getEmail()
	{
		 String email="sps_qa"+String.valueOf(System.currentTimeMillis())+"@sample.com";
		 return email;
	
	}
	
	
	
	private Map<String, Object> createEducatorWithPasswordLessthan7charactersPayload()
	
	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",email);
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password3);
		teacherRegistrationInfo.put("userName",userName);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
		
	}
	
private Map<String, Object> createEducatorWithPasswordOnly7CharactersPayload()
	
	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",email);
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password4);
		teacherRegistrationInfo.put("userName",userName);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
		
	}

private Map<String, Object> createEducatorWithPasswordOnly7NumbersPayload()

	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",email);
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password5);
		teacherRegistrationInfo.put("userName",userName);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
	
	}

	private Map<String, Object> createEducatorWithExistingUserPayload()

	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",user1);
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password2);
		teacherRegistrationInfo.put("userName",user1);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
	}
	
	private Map<String, Object> createEducatorPasswordWithMoreThan30CharactersPayload()

	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",email);
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password6);
		teacherRegistrationInfo.put("userName",email);
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
	}
	
	private Map<String, Object> createEducatorPasswordWith30AlphaNumericCharactersPayload()

	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",getEmail());
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password7);
		teacherRegistrationInfo.put("userName",getEmail());
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
	}
	
	private Map<String, Object> createEducatorPasswordWith7AlphaNumericCharactersPayload()

	{
		Map<String, Object> teacherRegistrationInfo = new HashMap<String, Object>();
		teacherRegistrationInfo.put("firstName",firstName);
		teacherRegistrationInfo.put("lastName",lastName);
		teacherRegistrationInfo.put("email",getEmail());
		userType.add("TEACHER");
		teacherRegistrationInfo.put("userType", userType);
		teacherRegistrationInfo.put("password",password8);
		teacherRegistrationInfo.put("userName",getEmail());
		teacherRegistrationInfo.put("schoolId",schoolId);
		return teacherRegistrationInfo;
	}

}
