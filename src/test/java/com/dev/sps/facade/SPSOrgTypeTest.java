package com.dev.sps.facade;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import com.jayway.restassured.builder.ResponseSpecBuilder;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.ResponseSpecification;

/**
 * @author Mohammed Rahman
 *
 */

public class SPSOrgTypeTest extends BaseTest
{
	//Home School
		private String h_name ="Home School Inc. 2015";
		private String h_address1 ="568 Broadway";
		private String h_address2 ="";
		private String h_address3 ="";
		private String h_address4 ="";
		private String h_address5 ="";
		private String h_city ="NYC";
		private String h_state ="NY";
		private String h_zipcode ="10012";
		private String h_country ="";
		private String h_poBox ="";
		private String h_county ="";
		private String h_phone ="2123438565";
		private String h_reportingSchoolType ="HOS";
		private String h_schoolSPSID;
		
		//Manual School
		private String m_name ="Manual School Inc. 2016";
		private String m_address1 ="550 Broadway";
		private String m_address2 ="";
		private String m_address3 ="";
		private String m_address4 ="";
		private String m_address5 ="";
		private String m_city ="NYC";
		private String m_state ="NY";
		private String m_zipcode ="10012";
		private String m_country ="";
		private String m_poBox ="";
		private String m_county ="";
		private String m_phone ="2123434535";
		private String m_reportingSchoolType;
		//private String m_schoolSPSID;
		
		//International School
		private String i_name ="International School. 2016";
		private String i_address1 ="550 Main Street";
		private String i_address2 ="";
		private String i_address3 ="";
		private String i_address4 ="";
		private String i_address5 ="";
		private String i_city ="DHAKA";
		private String i_state ="BD";
		private String i_zipcode ="10005";
		private String i_country ="";
		private String i_poBox ="";
		private String i_county ="";
		private String i_phone ="3434345350";
		private String i_reportingSchoolType;
		//private String i_schoolSPSID;
		
		//End points
		private static final String ENDPOINT_GET_LOCATION_TYPE="/lookup/location-types?clientId=NonSPSClient";	
		private static final String ENDPOINT_GET_SCHOOL_TYPE="/lookup/school-types?clientId=NonSPSClient";
		private static final String ENDPOINT_SEARCH_SCHOOLTYPE_BY_LOCATION_TYPE="/lookup/school-types?clientId=NonSPSClient&locationType";
		private static final String ENDPOINT_CREATE_ORG="/spsorganization/{locationType}/org?clientId=NonSPSClient";
		
		
		@Test
		public void getLocationTypeTest()
		{	
			System.out.println("##################################################################");
			System.out.println("******************** Get School Location Type ********************");
			System.out.println("##################################################################");
			ExtractableResponse<Response> getLocationTypeResponse=
									given()
											.log().all()
											.param("clientId","NonSPSClient").
									when()
											.get(ENDPOINT_GET_LOCATION_TYPE).
									then()
											.statusCode(200)
											.spec(getLocationTypeResponseValidator())
											.extract();	
			System.out.println(getLocationTypeResponse.asString());
		}
		@Test
		public void searchSchoolTypeByLocationTypeTest()
		{	
			System.out.println("############################################################################");
			System.out.println("******************** Search School Type By Location Type********************");
			System.out.println("############################################################################");
			// Location Type domestic
			String locationType="domestic";
			
									given()
											.log().all()
											.contentType("application/json")
											.queryParam("locationType", locationType)
											.param("clientId","NonSPSClient").
									when()
											.get(ENDPOINT_SEARCH_SCHOOLTYPE_BY_LOCATION_TYPE).
									then()
											.statusCode(200)
											.spec(searchSchoolTypeByLocationTypeDomesticResponseValidator())
											.extract();	
			// Location Type military
			locationType="military";	
			
									given()
									.log().all()
									.queryParam("locationType", locationType)
									.param("clientId","NonSPSClient").
							when()
									.get(ENDPOINT_GET_SCHOOL_TYPE).
							then()
									.statusCode(200)
									.spec(searchSchoolTypeByLocationTypeMilitaryResponseValidator())
									.extract();	
			// Location Type home
			locationType="home";	
									
									given()
									.log().all()
									.queryParam("locationType", locationType)
									.param("clientId","NonSPSClient").
							when()
									.get(ENDPOINT_GET_SCHOOL_TYPE).
							then()
									.statusCode(200)
									.spec(searchSchoolTypeByLocationTypeHomeResponseValidator())
									.extract();	
			// Location Type intl
			locationType="intl";	
									
									given()
									.log().all()
									.queryParam("locationType", locationType)
									.param("clientId","NonSPSClient").
							when()
									.get(ENDPOINT_GET_SCHOOL_TYPE).
							then()
									.statusCode(200)
									.spec(searchSchoolTypeByLocationTypeIntlResponseValidator())
									.extract();	
		}
		
		@Test
		public void searchSchoolTypeByInvalidOrEmptyLocationTypeTest()
		{	
			System.out.println("#############################################################################################");
			System.out.println("******************** Search School Type By Invalid or Empty Location Type********************");
			System.out.println("#############################################################################################");
			// Location Type Invalid
			String locationType="asdfrght";
			
									given()
											.log().all()
											.queryParam("locationType", locationType)
											.param("clientId","NonSPSClient").
									when()
											.get(ENDPOINT_GET_SCHOOL_TYPE).
									then()
											.statusCode(200)
											.spec(getSchoolTypeResponseValidator())
											.extract();	
									
			locationType="";
									
									given()
											.log().all()
											.queryParam("locationType", locationType)
											.param("clientId","NonSPSClient").
									when()
											.get(ENDPOINT_GET_SCHOOL_TYPE).
									then()
											.statusCode(200)
											.spec(getSchoolTypeResponseValidator())
											.extract();	
		}
		@Test
		public void getSchoolTypeTest()
		{	
			System.out.println("#########################################################");
			System.out.println("******************** Get School Type ********************");
			System.out.println("#########################################################");
			ExtractableResponse<Response> getSchoolTypeResponse=
									given()
											.log().all()
											.param("clientId","NonSPSClient").
									when()
											.get(ENDPOINT_GET_SCHOOL_TYPE).
									then()
											.statusCode(200)
											.spec(getSchoolTypeResponseValidator())
											.extract();	
			System.out.println(getSchoolTypeResponse.asString());
		}
		
		@Test
		public void createHomeSchoolTest()
		{	
			System.out.println("#########################################################");
			System.out.println("******************** Create Home School ********************");
			System.out.println("#########################################################");
			//Create Home School, Reporting Type "HOS"
			String locationType="home";
			ExtractableResponse<Response> createHomeSchoolResponse=
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createHomeSchoolResponseValidator())
											 .extract();	
			h_schoolSPSID=createHomeSchoolResponse.path("spsId");
			System.out.println(createHomeSchoolResponse.asString());
			System.out.println(">>>>>>Home School SPSID is: "+h_schoolSPSID);
		}
		
		@Test
		public void createManualSchoolTest()
		{	
			System.out.println("##############################################################");
			System.out.println("******************** Create Manual School ********************");
			System.out.println("##############################################################");
			//Create Manual School, Reporting Type "PS"
			String locationType="domestic";
			m_reportingSchoolType="PS";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();	
											
			//Create Manual School, Reporting Type "SS"
			locationType="domestic";
			m_reportingSchoolType="SS";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
									
			//Create Manual School, Reporting Type "VS"
			locationType="domestic";
			m_reportingSchoolType="VS";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
									
			//Create Manual School, Reporting Type "EC"
			locationType="domestic";
			m_reportingSchoolType="EC";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
			
			//Create Manual School, Reporting Type "C"
			locationType="domestic";
			m_reportingSchoolType="C";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
			//Create Manual School, Reporting Type "D"
			locationType="domestic";
			m_reportingSchoolType="D";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
				
			//Create Manual School, Reporting Type "L"
			locationType="domestic";
			m_reportingSchoolType="L";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
									
			//Create Manual School, Reporting Type "R"
			locationType="domestic";
			m_reportingSchoolType="R";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
					
			//Create Manual School, Reporting Type "O"
			locationType="domestic";
			m_reportingSchoolType="O";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();
									
			//Create Manual School, Reporting Type "O"
			locationType="military";
			m_reportingSchoolType="AP";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createManualSchoolResponseValidator())
											 .extract();		
			
		}
		
		@Test
		public void createInternationalManualSchoolTest()
		{	
			System.out.println("##############################################################");
			System.out.println("******************** Create Manual School ********************");
			System.out.println("##############################################################");
			//Create Manual School, locationType "intl"
			String locationType="intl";
			i_reportingSchoolType="USI";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createInternationalManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createInternationalManualSchoolResponseValidator())
											 .extract();
									
			//Create Manual School, locationType "intl"
			locationType="intl";
			i_reportingSchoolType="ISI";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createInternationalManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createInternationalManualSchoolResponseValidator())
											 .extract();
									
			//Create Manual School, locationType Type "intl"
			locationType="intl";
			i_reportingSchoolType="HOS";
			
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createInternationalManualSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(201)
											.spec(createInternationalManualSchoolResponseValidator())
											 .extract();	
		}	
		
		@Test
		public void createHomeSchoolWithValidationTest()
		{	
			System.out.println("############################################################################");
			System.out.println("******************** Create Home School With Validation ********************");
			System.out.println("############################################################################");
			//Create Home School without school name
			String locationType="home";
			h_name="";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Name is Required."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			//Create Home School with school name more than 30 Characters
			locationType="home";
			h_name="abcdefghijklmnopqrstuvwxyzqqwwa";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Name can not exceed 30 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
									
			//Create Home School with school name less than 3 Characters
			locationType="home";
			h_name="ab";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Name must be atleast 3 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			h_name="abhuihiugyg";
						
			//Create Home School without school address1
			locationType="home";
			h_address1="";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Address Line 1 is Required."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
									
			//Create Home School with address1 more than 30 Characters
			locationType="home";
			h_address1="abcdefghijklmnopqrstuvwxyzqqwwa";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Address Line 1 can not exceed 30 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			
			//Create Home School with address1less than 3 Characters
			locationType="home";
			h_address1="ab";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Address Line 1 must be atleast 3 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			
			h_address1="abvhggghhhhh";
									
			//Create Home School without school City
			locationType="home";
			h_city="";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("City is Required."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
										
				//Create Home School with city more than 30 Characters
				locationType="home";
				h_city="abcdefghijklmnopqrstuvwxyzqqwwa";
										given()
												.log().all()
												.contentType("application/json")
												.pathParam("locationType", locationType)
												.body(createHomeSchoolPayload()).
										when()
												.post(ENDPOINT_CREATE_ORG).
										then()
												.statusCode(400)
												.body("errorMessages", hasItem("City can not exceed 30 characters."))
												.body("errorStatus", equalTo("BUSINESS_FAILURE"));
				
				//Create Home School with city than 3 Characters
				locationType="home";
				h_city="ab";
										given()
												.log().all()
												.contentType("application/json")
												.pathParam("locationType", locationType)
												.body(createHomeSchoolPayload()).
										when()
												.post(ENDPOINT_CREATE_ORG).
										then()
												.statusCode(400)
												.body("errorMessages", hasItem("City must be atleast 3 characters."))
												.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			
			h_city="abcdefghijk";	
			
			//Create Home School with state more than 30 Characters
			locationType="home";
			h_state="abcdefghijklmnopqrstuvwxyzqqwwa";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("State can not exceed 30 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			
			//Create Home School with state less than 2 Characters
			locationType="home";
			h_state="a";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("State must be atleast 2 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
		h_state="sdsff";
									
			//Create Home School with zip code more than 5 digits
			locationType="home";
			h_zipcode="345678";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Postal/Zip can not exceed 5 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
			
			//Create Home School with state than 3 Characters
			locationType="home";
			h_zipcode="2345";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Postal/Zip must be atleast 5 characters."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
									
			h_zipcode="23451";
			
			//Create Home School without Phone
			locationType="home";
			h_phone="";
									given()
											.log().all()
											.contentType("application/json")
											.pathParam("locationType", locationType)
											.body(createHomeSchoolPayload()).
									when()
											.post(ENDPOINT_CREATE_ORG).
									then()
											.statusCode(400)
											.body("errorMessages", hasItem("Phone is Required."))
											.body("errorStatus", equalTo("BUSINESS_FAILURE"));
										
				//Create Home School with phone more than 10 Characters
				locationType="home";
				h_phone="34567894562";
										given()
												.log().all()
												.contentType("application/json")
												.pathParam("locationType", locationType)
												.body(createHomeSchoolPayload()).
										when()
												.post(ENDPOINT_CREATE_ORG).
										then()
												.statusCode(400)
												.body("errorMessages", hasItem("Phone can not exceed 10 characters."))
												.body("errorStatus", equalTo("BUSINESS_FAILURE"));
				
										
			    h_phone="4567894562";
										
		}
		
		
		private Map<String, Object> createHomeSchoolPayload()
		{
			Map<String, Object> createHomeSchoolInfo = new HashMap<String, Object>();
			createHomeSchoolInfo.put("name",h_name );
			createHomeSchoolInfo.put("address1",h_address1);
			createHomeSchoolInfo.put("address2",h_address2);
			createHomeSchoolInfo.put("address3", h_address3);
			createHomeSchoolInfo.put("address4",h_address4);
			createHomeSchoolInfo.put("address5",h_address5);
			createHomeSchoolInfo.put("city",h_city);
			createHomeSchoolInfo.put("state", h_state);
			createHomeSchoolInfo.put("zipcode",h_zipcode);
			createHomeSchoolInfo.put("country",h_country);
			createHomeSchoolInfo.put("poBox",h_poBox);
			createHomeSchoolInfo.put("county",h_county);
			createHomeSchoolInfo.put("phone", h_phone);
			createHomeSchoolInfo.put("reportingSchoolType",h_reportingSchoolType);
			return createHomeSchoolInfo;
		}
		
		private Map<String, Object> createManualSchoolPayload()
		{
			Map<String, Object> createManualSchoolInfo = new HashMap<String, Object>();
			createManualSchoolInfo.put("name",m_name );
			createManualSchoolInfo.put("address1",m_address1);
			createManualSchoolInfo.put("address2",m_address2);
			createManualSchoolInfo.put("address3", m_address3);
			createManualSchoolInfo.put("address4",m_address4);
			createManualSchoolInfo.put("address5",m_address5);
			createManualSchoolInfo.put("city",m_city);
			createManualSchoolInfo.put("state", m_state);
			createManualSchoolInfo.put("zipcode",m_zipcode);
			createManualSchoolInfo.put("country",m_country);
			createManualSchoolInfo.put("poBox",m_poBox);
			createManualSchoolInfo.put("county",m_county);
			createManualSchoolInfo.put("phone", m_phone);
			createManualSchoolInfo.put("reportingSchoolType",m_reportingSchoolType);
			return createManualSchoolInfo;
		}
		
		private Map<String, Object> createInternationalManualSchoolPayload()
		{
			Map<String, Object> createManualSchoolInfo = new HashMap<String, Object>();
			createManualSchoolInfo.put("name",i_name );
			createManualSchoolInfo.put("address1",i_address1);
			createManualSchoolInfo.put("address2",i_address2);
			createManualSchoolInfo.put("address3", i_address3);
			createManualSchoolInfo.put("address4",i_address4);
			createManualSchoolInfo.put("address5",i_address5);
			createManualSchoolInfo.put("city",i_city);
			createManualSchoolInfo.put("state", i_state);
			createManualSchoolInfo.put("zipcode",i_zipcode);
			createManualSchoolInfo.put("country",i_country);
			createManualSchoolInfo.put("poBox",i_poBox);
			createManualSchoolInfo.put("county",i_county);
			createManualSchoolInfo.put("phone", i_phone);
			createManualSchoolInfo.put("reportingSchoolType",i_reportingSchoolType);
			return createManualSchoolInfo;
		}
		
		private ResponseSpecification createManualSchoolResponseValidator()
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("name",equalTo("Manual School Inc. 2016"))
			.expectBody("address1",equalTo("550 Broadway"))
			.expectBody("address2",isEmptyString())
			.expectBody("address3",isEmptyString())
			.expectBody("address4",isEmptyString())
			.expectBody("address5",isEmptyString())
			.expectBody("city",equalTo("NYC"))
			.expectBody("state",equalTo("NY"))
			.expectBody("country",isEmptyString())
			.expectBody("poBox",isEmptyString())
			.expectBody("phone",equalTo("2123434535"))
			.expectBody("reportingSchoolType",equalTo(m_reportingSchoolType));
			return rspec.build();
		}
		private ResponseSpecification createInternationalManualSchoolResponseValidator()
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("name",equalTo(i_name))
			.expectBody("address1",equalTo(i_address1))
			.expectBody("address2",isEmptyString())
			.expectBody("address3",isEmptyString())
			.expectBody("address4",isEmptyString())
			.expectBody("address5",isEmptyString())
			.expectBody("city",equalTo(i_city))
			.expectBody("state",equalTo(i_state))
			.expectBody("country",isEmptyString())
			.expectBody("poBox",isEmptyString())
			.expectBody("phone",equalTo(i_phone))
			.expectBody("reportingSchoolType",equalTo(i_reportingSchoolType));
			return rspec.build();
		}
		private ResponseSpecification createEnumResponseValidator(String[][] keyvalues )
		{
			ResponseSpecBuilder respec=new ResponseSpecBuilder()
				.expectStatusCode(200)
				.expectContentType(ContentType.JSON);
				addKeyValueValidations(respec,keyvalues);
				return respec.build();
		}
		
		private ResponseSpecification createHomeSchoolResponseValidator()
		{
			ResponseSpecBuilder rspec=new ResponseSpecBuilder()
			.expectBody("name",equalTo("Home School Inc. 2015"))
			.expectBody("address1",equalTo("568 Broadway"))
			.expectBody("address2",isEmptyString())
			.expectBody("address3",isEmptyString())
			.expectBody("address4",isEmptyString())
			.expectBody("address5",isEmptyString())
			.expectBody("city",equalTo("NYC"))
			.expectBody("state",equalTo("NY"))
			.expectBody("country",isEmptyString())
			.expectBody("poBox",isEmptyString())
			.expectBody("phone",equalTo("2123438565"))
			.expectBody("reportingSchoolType",equalTo("HOS"));
			return rspec.build();
		}
		
		private ResponseSpecification getLocationTypeResponseValidator()
		{
		final String[][] keyValues = 
			{ 
				{ "domestic", "United States School (Includes U.S. territories and commonwealths)" }, 
				{ "military", "Military Base (APO/FPO)" }, 
				{ "home", "Home School" },
				{ "intl", "Non-U.S. School" }
			};
		return createEnumResponseValidator(keyValues);
		}
		
		private ResponseSpecification getSchoolTypeResponseValidator()
		{
		final String[][] keyValues = 
			{ 
				{ "o", "Other" }, 
				{ "r", "RIF" }, 
				{ "l", "Library" },
				{ "d", "District" },
				{ "c", "College" }, 
				{ "ec", "Early Childhood" }, 
				{ "vs", "Vocational School" },
				{ "ps", "Primary School" }, 
				{ "ss", "Secondary School" }, 
				{ "ap", "APO/FPO" },
				{ "usi", "US School Abroad" },
				{ "isi", "International School" }, 
				{ "hos", "Home School" } 
			};
		return createEnumResponseValidator(keyValues);
		}
		
		private ResponseSpecification searchSchoolTypeByLocationTypeDomesticResponseValidator()
		{
		final String[][] keyValues = 
			{ 
				{ "o", "Other" }, 
				{ "r", "RIF" }, 
				{ "l", "Library" },
				{ "d", "District" },
				{ "c", "College" }, 
				{ "ec", "Early Childhood" }, 
				{ "vs", "Vocational School" },
				{ "ps", "Primary School" }, 
				{ "ss", "Secondary School" }, 
				{ "ap", "APO/FPO" },
				{ "usi", "US School Abroad" },
				{ "isi", "International School" }, 
				{ "hos", "Home School" } 
			};
		return createEnumResponseValidator(keyValues);
		}
		
		private ResponseSpecification searchSchoolTypeByLocationTypeMilitaryResponseValidator()
		{
		final String[][] keyValues = 
			{ 
				{"ap", "APO/FPO" } 
			};
		return createEnumResponseValidator(keyValues);
		}
		
		private ResponseSpecification searchSchoolTypeByLocationTypeHomeResponseValidator()
		{
		final String[][] keyValues = 
			{ 
				{"hos", "Home School" } 
			};
		return createEnumResponseValidator(keyValues);
		}
		
		private ResponseSpecification searchSchoolTypeByLocationTypeIntlResponseValidator()
		{
		final String[][] keyValues = 
			{ 
				{ "usi", "US School Abroad" },
				{ "isi", "International School" }, 
				{ "hos", "Home School" }  
			};
		return createEnumResponseValidator(keyValues);
		}
		
		private static void addKeyValueValidations(ResponseSpecBuilder specBuilder,	String[][] keyValues) 
		{
			specBuilder.expectBody("size()", equalTo(keyValues.length));
			for (int i = 0; i < keyValues.length; i++) 
			{
				specBuilder.expectBody("get(" + i + ").key",
				equalTo(keyValues[i][0]));
				specBuilder.expectBody("get(" + i + ").value",
				equalTo(keyValues[i][1]));
			}
		}

	}


