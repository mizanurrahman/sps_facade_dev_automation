package com.dev.sps.facade;

/***************************************************************************************************************************
 ***  @@Author
 ***  Mohammed Rahman
 ***  This Tests Will Be Using To Test Facade Payment Services.
 ***************************************************************************************************************************/

import static com.jayway.restassured.RestAssured.given;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import com.jayway.restassured.response.ExtractableResponse;
import com.jayway.restassured.response.Response;

public class SPSPaymentServiceTest extends BaseTest
{

	//Login for Session Id and TSP Id
	private String user ="sps_dev1463080280411@sample.com";
	//private String user1 ="print38sample@scholastic.com";
	private String user1 ="sps_dev1463079832733@sample.com";
	private String password ="passed1";
	private boolean singleToken = true;
	private boolean userForWS = false;
	private boolean addCookie = true;
	private boolean addExpiration = true;
	private String sps_session;
	private String sps_tsp;
	private String userSPSID;
	
	
	/*//Capture Payment Info
	private String reqCardNumber="411111xxxxxx1111";
	private String reqPaymentToken="";
	private String reqCardExpiryDate="03-2017";
	private String reqAccessKey="3613e1d4d11c33dbbf5370ee90d188a0";
	//private String reqBillToEmail="qatestsample@juno1.com";
	private String signedFieldNames="transaction_id,decision,req_access_key,req_profile_id,req_transaction_uuid,req_transaction_type,req_reference_number,req_amount,req_currency,req_locale,req_payment_method,req_payment_token,req_consumer_id,req_bill_to_forename,req_bill_to_surname,req_bill_to_email,req_bill_to_phone,req_bill_to_address_line1,req_bill_to_address_city,req_bill_to_address_state,req_bill_to_address_country,req_bill_to_address_postal_code,req_card_number,req_card_type,req_card_expiry_date,req_merchant_defined_data32,req_merchant_defined_data35,message,reason_code,auth_avs_code,auth_avs_code_raw,auth_response,auth_amount,auth_code,auth_trans_ref_no,auth_time,bill_trans_ref_no,signed_field_names,signed_date_time";
	private String authAvsCode="Y";
	private String decision="ACCEPT";
	private String signature="ys0iaTeyuSt6Eb7uFsWAC/VH6NNkOGwq9Qw24EdNe+Y=";
	private String reqBillToPhone="3024568967";
	private String reasonCode="100";
	private String signedDateTime="2016-05-13T14:57:19Z";
	private String reqCurrency="USD";
	private String message="request+was+processed+succesfully";
	private String authTransRefNo="WWNTOBO0Q5QL";
	private String reqPaymentMethod="card";
	private String transactionId="4362997513055000001MZR";
	private String reqProfileId="MAAWSPSQA1";
	//private String reqConsumerId;
	private String authResponse="00";
	private String reqTransactionUuId="bf1dbc53-6345-4004-aa10-522c986f006a";
	private String reqBillToSurname="Hill";
	private String reqBillTransRefNo="11903606";
	private String reqBillToForename="Top";
	private String reqBillToAddressCountry="US";
	private String reqMerchantDefinedData35="anythingelse";
	private String authTime="2015-07-07T200912Z";
	private String reqTransactionType="sale";
	private String reqLocale="en";
	private String authAmount="3.21";
	private String reqReferenceNumber="SCH_SPS_MA2_1463151439383";
	private String authAvsCodeRaw="Y";
	private String authCode="831000";
	private String reqCardType="001";
	private String reqMerchantDefinedData32="true";
	private String reqAmount="3.21";
	private String reqBillToAddressCity="Westland";
	private String reqBillToAddressLine1="33241 Creston Street";
	private String reqBillToAddressLine2="line2";
	private String reqBillToAddressState="MI";
	private String reqBillToAddressPostalCode="48186";*/
	
	//Sign a Payment
	private String cardNumber="4111111111111111";
	private String paymentPartial="3.21";
	private String partial="partial";
	private String paymentFull="1,133.02";
	
	//Add Credit Card to Subscription
	//private String userSPSID1="83245877";
	private String reqCardNumber1="411111xxxxxx1111";
	private String reqPaymentToken1="01019648212121";
	private String reqCardExpiryDate1="03-2017";
	private String reqAccessKey1="84854545454564564564";
	//private String reqBillToEmail1;
	private String signedFieldNames1="transaction_id,decision,req_access";
	private String authAvsCode1="Y";
	private String decision1="ACCEPT";
	private String reqMerchantDefinedData11="100260033";
	private String reqMerchantDefinedData10="X";
	private String reqMerchantDefinedData19="5016001";
	private String signature1="CC2YUshjdhdsFRdsdds96856565sd656sd5";
	private String requestToken1="dsfdsfsdfsdfdsfcxvcxvcxvcv";
	private String reqBillToPhone1="5163216548";
	private String reasonCode1="100";
	private String signedDateTime1="2015-06-09T21:17:33Z";
	private String reqCurrency1="USD";
	private String message1="request+was+processed+succesfully";
	private String authTransRefNo1="WWNTYDFDFD";
	private String reqPaymentMethod1="card";
	private String transactionId1="4333XXXXXXXXXX";
	private String reqProfileId1="MAAWSQA1";
	//private String reqConsumerId="83245877";
	private String authResponse1="85";
	private String reqTransactionUuId1="b5sdfsdfdsfdsf";
	private String reqBillToSurname1="ROBERT";
	private String reqBillToForename1="KENT";
	private String reqBillToAddressCountry1="US";
	private String reqMerchantDefinedData35a="any data to send";
	private String reqMerchantDefinedData30a="true";
	private String reqMerchantDefinedData9a="101";
	private String reqMerchantDefinedData27a="356";
	private String reqMerchantDefinedData25a="ADD";
	private String reqMerchantDefinedData22a="-1";
	private String reqMerchantDefinedData23a="true";
	private String reqMerchantDefinedData20a="OTC";
	private String reqMerchantDefinedData21a="true";
	private String reqMerchantDefinedData12a="8654438";
	private String authTime1="2015-06XXXXX";
	private String reqTransactionType1="create_payment_token";
	private String reqLocale1="en";
	private String authAmount1="0.00";
	private String authAvsCodeRaw1="Y";
	private String authCode1="831000";
	private String reqCardType1="001";
	private String reqBillToAddressCity1="South Floral Park";
	private String reqBillToAddressLine1a="floral park";
	private String reqBillToAddressLine2a="line2";
	private String reqBillToAddressState1="NY";
	private String reqBillToAddressPostalCode1="11001";
	
	/*//Update Credit Card to Subscription
	private String reqCardExpiryDate2="04-2017";
	private String reqBillToSurname2="GEORGE";
	private String reqBillToForename2="HORN";
	private String reqMerchantDefinedData25b="EDIT";
	private String reqBillToAddressCity2="JACKSON HEIGHTS";
	private String reqBillToAddressLine1b="74-22 35th Ave";
	private String reqBillToAddressLine2b="line2";
	private String reqBillToAddressState2="NY";
	private String reqBillToAddressPostalCode2="11372";
	private String reqReferenceNumber2="0";
	private String reqMerchantDefinedData22b="1";*/
	
	//End Points
	//private static final String ENDPOINT_CAPTURE_PAYMENT="/spsuser/{spsId}/cybersource/makePayment?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_SIGN_PAYMENT="/spsuser/{spsId}/cybersource/sign/sale?clientId=SPSFacadeAPI";
	private static final String ENDPOINT_ADDCREDITCARD_SUBSCRIPTION="/spsuser/{spsId}/cybersource/creditcard/subs?clientId=SPSFacadeAPI";
	//private static final String ENDPOINT_EDITCREDITCARD_SUBSCRIPTION="/spsuser/{spsId}/cybersource/creditcard/1/subs?clientId=SPSFacadeAPI";
	
  @Test
	public void createAuthenticationTest() 
	{	
		System.out.println("##############################################################");
		System.out.println("******************** Authenticate Service ********************");
		System.out.println("##############################################################");
								
		ExtractableResponse<Response> createLogInResponse=
								given()
										.log().all()
										.contentType("application/json")
										.body(createLogInPayload()).
		                		when()
		                				.post("http://fs-iam-spsapifacade-dev.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
								then()
										.statusCode(200)
										.extract();	
		//sps_session=createLogInResponse.path("SPS_SESSION.value");
		//sps_tsp=createLogInResponse.path("SPS_TSP.value");
		//System.out.println(createLogInResponse.path("SPS_UD.value"));
		//userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		
		//System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		//System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		//System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
		System.out.println("******************** Authentication Response ********************");
		System.out.println(createLogInResponse.asString());
		System.out.println("*****************************************************************");
	}
  
 /* @Test
	public void createLogInAndCapturePaymentTest()
	{
		System.out.println("#####################################################################");	
		System.out.println("******************** Service to Capture a Payment********************");
		System.out.println("#####################################################################");
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
								given()
										.contentType("application/json")
										.body(createLogInPayload()).
		                		when()
		                				.post("http://fs-iam-spsapifacade-dev.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
								then()
										.statusCode(200)
										.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
					
		// Capture Payment
		ExtractableResponse<Response> createCapturePaymentResponse=
								given()
										.log().all()
										.pathParam("spsId",userSPSID)
										.cookies("SPS_SESSION",sps_session)
										.cookie("SPS_TSP",sps_tsp)
										.contentType("application/json")
										.body(createCapturePaymentPayload()).
								when()
										.post(ENDPOINT_CAPTURE_PAYMENT).
								then()
										.statusCode(200)
										//.spec(createCapturePaymentResponse())
											.extract();
		//userSPSID=createCapturePaymentResponse.path("spsId");	
		//System.out.println("User SPSID is: "+userSPSID);
		System.out.println("******************** Captured Payment Response ********************");
		System.out.println(createCapturePaymentResponse.asString());
		System.out.println("*******************************************************************");
	}*/
	
  @Test
	public void createLogInAndSignPaymentTest()
	{
		System.out.println("#####################################################################");	
		System.out.println("******************** Service to Sign a Payment********************");
		System.out.println("#####################################################################");
		//Authentication
		ExtractableResponse<Response> createLogInResponse=
								given()
										.contentType("application/json")
										.body(createLogInPayload()).
		                		when()
		                				.post("http://fs-iam-spsapifacade-dev.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
								then()
										.statusCode(200)
										.extract();	
		sps_session=createLogInResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInResponse.path("SPS_TSP.value");
		System.out.println(createLogInResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
					
		// Sign a Payment
		ExtractableResponse<Response> createSignPaymentResponse=
								given()
										.log().all()
										.pathParam("spsId",userSPSID)
										.cookies("SPS_SESSION",sps_session)
										.cookie("SPS_TSP",sps_tsp)
										.contentType("application/json")
										.body(createSignPaymentPayload()).
								when()
										.post(ENDPOINT_SIGN_PAYMENT).
								then()
										.statusCode(200)
										//.spec(createCapturePaymentResponse())
										.extract();
		//userSPSID=createCapturePaymentResponse.path("spsId");	
		//System.out.println("User SPSID is: "+userSPSID);
		System.out.println("******************** Signed Payment Response ********************");
		System.out.println(createSignPaymentResponse.asString());
		System.out.println("*******************************************************************");
				
	}
	
  @Test
	public void createLogInAndAddCreditCardToSubscriptionTest()
	{
		System.out.println("#######################################################################################");	
		System.out.println("******************** Service to add credit card for a subscription ********************");
		System.out.println("#######################################################################################");
		//Authentication
		ExtractableResponse<Response> createLogInSubsResponse=
								given()
										.contentType("application/json")
										.body(createLogInSubsPayload()).
		                		when()
		                				.post("http://fs-iam-spsapifacade-dev.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
								then()
										.statusCode(200)
										.extract();	
		sps_session=createLogInSubsResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInSubsResponse.path("SPS_TSP.value");
		System.out.println(createLogInSubsResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInSubsResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
					
		// Add Credit Card To Subscription
		ExtractableResponse<Response> addCreditCardToSubscriptionResponse=
								given()
										.log().all()
										.pathParam("spsId",userSPSID)
										.contentType("application/json")
										.cookies("SPS_SESSION",sps_session)
										.cookie("SPS_TSP",sps_tsp)
										.body(addCreditCardToSubscriptionPayload()).
								when()
										.post(ENDPOINT_ADDCREDITCARD_SUBSCRIPTION).
								then()
										.statusCode(202)
										//.spec(createCapturePaymentResponse())
										.extract();
		//userSPSID=createCapturePaymentResponse.path("spsId");	
		//System.out.println("User SPSID is: "+userSPSID);
		System.out.println("******************** Add Credit Card To Subscription Response ********************");
		System.out.println(addCreditCardToSubscriptionResponse.asString());
		System.out.println("**********************************************************************************");
				
	}
/*@Test
	public void createLogInAndEditCreditCardToSubscriptionTest()
	{
		System.out.println("#######################################################################################");	
		System.out.println("******************** Service to edit credit card for a subscription ********************");
		System.out.println("#######################################################################################");
		//Authentication
		ExtractableResponse<Response> createLogInSubsResponse=
								given()
										.contentType("application/json")
		                				.body(createLogInSubsPayload()).
								when()
										.post("http://fs-iam-spsapifacade-dev.scholastic-labs.io/sps-api-facade/spsuser/login?clientId=SPSFacadeAPI").
								then()
										.statusCode(200)
										.extract();	
		sps_session=createLogInSubsResponse.path("SPS_SESSION.value");
		sps_tsp=createLogInSubsResponse.path("SPS_TSP.value");
		System.out.println(createLogInSubsResponse.path("SPS_UD.value"));
		userSPSID=((String)createLogInSubsResponse.path("SPS_UD.value")).split("\\|")[0];
		
		System.out.println(">>>>>>>>>>> SPS Session Id: "+sps_session);
		System.out.println(">>>>>>>>>>> SPS TSP Id: "+sps_tsp);
		System.out.println(">>>>>>>>>>> User SPSID: "+userSPSID);
					
		// Edit Credit Card To Subscription
		ExtractableResponse<Response> editCreditCardToSubscriptionResponse=
								given()
										.log().all()
										.pathParam("spsId",userSPSID)
										.contentType("application/json")
										.cookies("SPS_SESSION",sps_session)
										.cookie("SPS_TSP",sps_tsp)
										.body(editCreditCardToSubscriptionPayload()).
								when()
										.put(ENDPOINT_EDITCREDITCARD_SUBSCRIPTION).
								then()
										.statusCode(200)
										//.spec(createCapturePaymentResponse())
										.extract();
		//userSPSID=createCapturePaymentResponse.path("spsId");	
		//System.out.println("User SPSID is: "+userSPSID);
		System.out.println("******************** Add Credit Card To Subscription Response ********************");
		System.out.println(editCreditCardToSubscriptionResponse.asString());
		System.out.println("**********************************************************************************");
				
	}*/
  
  	private Map<String, Object> createLogInPayload()

	{
		Map<String, Object> createLogInInfo = new HashMap<String, Object>();
		createLogInInfo.put("user",user );
		createLogInInfo.put("password",password);
		createLogInInfo.put("singleToken",singleToken);
		createLogInInfo.put("userForWS", userForWS);
		createLogInInfo.put("addCookie",addCookie);
		createLogInInfo.put("addExpiration",addExpiration);
		return createLogInInfo;
	}
	
	private Map<String, Object> createLogInSubsPayload()

	{
		Map<String, Object> createLogInSubsInfo = new HashMap<String, Object>();
		createLogInSubsInfo.put("user",user1 );
		createLogInSubsInfo.put("password",password);
		createLogInSubsInfo.put("singleToken",singleToken);
		createLogInSubsInfo.put("userForWS", userForWS);
		createLogInSubsInfo.put("addCookie",addCookie);
		createLogInSubsInfo.put("addExpiration",addExpiration);
		return createLogInSubsInfo;
	}

	/*private Map<String, Object> createCapturePaymentPayload()
			
	{
		Map<String, Object> capturePaymentInfo = new HashMap<String, Object>();
		capturePaymentInfo.put("reqCardNumber",reqCardNumber); 
		capturePaymentInfo.put("reqPaymentToken",reqPaymentToken); 
		capturePaymentInfo.put("reqCardExpiryDate",reqCardExpiryDate); 
		capturePaymentInfo.put("reqAccessKey",reqAccessKey); 
		capturePaymentInfo.put("reqBillToEmail",user); 
		capturePaymentInfo.put("signedFieldNames",signedFieldNames);
		capturePaymentInfo.put("authAvsCode",authAvsCode); 
		capturePaymentInfo.put("decision",decision);
		capturePaymentInfo.put("signature",signature); 
		capturePaymentInfo.put("reqBillToPhone",reqBillToPhone); 
		capturePaymentInfo.put("reasonCode",reasonCode); 
		capturePaymentInfo.put("signedDateTime",signedDateTime); 
		capturePaymentInfo.put("reqCurrency",reqCurrency); 
		capturePaymentInfo.put("message",message); 
		capturePaymentInfo.put("authTransRefNo",authTransRefNo); 
		capturePaymentInfo.put("reqPaymentMethod",reqPaymentMethod); 
		capturePaymentInfo.put("transactionId",transactionId); 
		capturePaymentInfo.put("reqProfileId",reqProfileId); 
		capturePaymentInfo.put("reqConsumerId",userSPSID); 
		capturePaymentInfo.put("authResponse",authResponse);
		capturePaymentInfo.put("reqTransactionUuId",reqTransactionUuId); 
		capturePaymentInfo.put("reqBillToSurname",reqBillToSurname);
		capturePaymentInfo.put("reqBillTransRefNo",reqBillTransRefNo);
		capturePaymentInfo.put("reqBillToForename",reqBillToForename); 
		capturePaymentInfo.put("reqBillToAddressCountry",reqBillToAddressCountry);
		capturePaymentInfo.put("reqMerchantDefinedData35",reqMerchantDefinedData35); 
		capturePaymentInfo.put("authTime",authTime); 
		capturePaymentInfo.put("reqTransactionType",reqTransactionType); 
		capturePaymentInfo.put("reqLocale",reqLocale);
		capturePaymentInfo.put("authAmount",authAmount);
		capturePaymentInfo.put("reqReferenceNumber",reqReferenceNumber);
		capturePaymentInfo.put("authAvsCodeRaw",authAvsCodeRaw); 
		capturePaymentInfo.put("authCode",authCode); 
		capturePaymentInfo.put("reqCardType",reqCardType);
		capturePaymentInfo.put("reqMerchantDefinedData32",reqMerchantDefinedData32);
		capturePaymentInfo.put("reqAmount",reqAmount);
		capturePaymentInfo.put("reqBillToAddressCity",reqBillToAddressCity); 
		capturePaymentInfo.put("reqBillToAddressLine1",reqBillToAddressLine1); 
		capturePaymentInfo.put("reqBillToAddressLine2",reqBillToAddressLine2); 
		capturePaymentInfo.put("reqBillToAddressState",reqBillToAddressState); 
		capturePaymentInfo.put("reqBillToAddressPostalCode",reqBillToAddressPostalCode);
		return capturePaymentInfo;
	}*/
	
	private Map<String, Object> addCreditCardToSubscriptionPayload()
	
	{
		Map<String, Object> addCreditCardToSubsInfo = new HashMap<String, Object>();
		addCreditCardToSubsInfo.put("reqCardNumber",reqCardNumber1);
		addCreditCardToSubsInfo.put("reqPaymentToken",reqPaymentToken1);
		addCreditCardToSubsInfo.put("reqCardExpiryDate",reqCardExpiryDate1);
		addCreditCardToSubsInfo.put("reqAccessKey",reqAccessKey1);
		addCreditCardToSubsInfo.put("reqBillToEmail",user1);
		addCreditCardToSubsInfo.put("signedFieldNames",signedFieldNames1);
		addCreditCardToSubsInfo.put("authAvsCode",authAvsCode1);
		addCreditCardToSubsInfo.put("decision",decision1);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData11",reqMerchantDefinedData11);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData10",reqMerchantDefinedData10);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData19",reqMerchantDefinedData19);
		addCreditCardToSubsInfo.put("signature",signature1);
		addCreditCardToSubsInfo.put("requestToken",requestToken1);
		addCreditCardToSubsInfo.put("reqBillToPhone",reqBillToPhone1);
		addCreditCardToSubsInfo.put("reasonCode",reasonCode1);
		addCreditCardToSubsInfo.put("signedDateTime",signedDateTime1);
		addCreditCardToSubsInfo.put("reqCurrency",reqCurrency1);
		addCreditCardToSubsInfo.put("message",message1);
		addCreditCardToSubsInfo.put("authTransRefNo",authTransRefNo1);
		addCreditCardToSubsInfo.put("reqPaymentMethod",reqPaymentMethod1);
		addCreditCardToSubsInfo.put("transactionId",transactionId1);
		addCreditCardToSubsInfo.put("reqProfileId",reqProfileId1);
		addCreditCardToSubsInfo.put("reqConsumerId",userSPSID);
		addCreditCardToSubsInfo.put("authResponse",authResponse1);
		addCreditCardToSubsInfo.put("reqTransactionUuId",reqTransactionUuId1);
		addCreditCardToSubsInfo.put("reqBillToSurname",reqBillToSurname1);
		addCreditCardToSubsInfo.put("reqBillToForename",reqBillToForename1);
		addCreditCardToSubsInfo.put("reqBillToAddressCountry",reqBillToAddressCountry1);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData35",reqMerchantDefinedData35a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData30",reqMerchantDefinedData30a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData9",reqMerchantDefinedData9a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData27",reqMerchantDefinedData27a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData25",reqMerchantDefinedData25a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData22",reqMerchantDefinedData22a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData23",reqMerchantDefinedData23a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData20",reqMerchantDefinedData20a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData21",reqMerchantDefinedData21a);
		addCreditCardToSubsInfo.put("reqMerchantDefinedData12",reqMerchantDefinedData12a);
		addCreditCardToSubsInfo.put("authTime",authTime1);
		addCreditCardToSubsInfo.put("reqTransactionType",reqTransactionType1);
		addCreditCardToSubsInfo.put("reqLocale",reqLocale1);
		addCreditCardToSubsInfo.put("authAmount",authAmount1);
		addCreditCardToSubsInfo.put("authAvsCodeRaw",authAvsCodeRaw1);
		addCreditCardToSubsInfo.put("authCode",authCode1);
		addCreditCardToSubsInfo.put("reqCardType",reqCardType1);
		addCreditCardToSubsInfo.put("reqBillToAddressCity",reqBillToAddressCity1);
		addCreditCardToSubsInfo.put("reqBillToAddressLine1",reqBillToAddressLine1a);
		addCreditCardToSubsInfo.put("reqBillToAddressLine2",reqBillToAddressLine2a);
		addCreditCardToSubsInfo.put("reqBillToAddressState",reqBillToAddressState1);
		addCreditCardToSubsInfo.put("reqBillToAddressPostalCode",reqBillToAddressPostalCode1);
		return addCreditCardToSubsInfo;
	}
	
	/*private Map<String, Object> editCreditCardToSubscriptionPayload()
	
	{
		Map<String, Object> editCreditCardToSubsInfo = new HashMap<String, Object>();
		editCreditCardToSubsInfo.put("reqCardNumber",reqCardNumber1);
		editCreditCardToSubsInfo.put("reqPaymentToken",reqPaymentToken1);
		editCreditCardToSubsInfo.put("reqCardExpiryDate",reqCardExpiryDate2);
		editCreditCardToSubsInfo.put("reqAccessKey",reqAccessKey1);
		editCreditCardToSubsInfo.put("reqBillToEmail",user1);
		editCreditCardToSubsInfo.put("signedFieldNames",signedFieldNames1);
		editCreditCardToSubsInfo.put("authAvsCode",authAvsCode1);
		editCreditCardToSubsInfo.put("decision",decision1);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData11",reqMerchantDefinedData11);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData10",reqMerchantDefinedData10);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData19",reqMerchantDefinedData19);
		editCreditCardToSubsInfo.put("signature",signature1);
		editCreditCardToSubsInfo.put("requestToken",requestToken1);
		editCreditCardToSubsInfo.put("reqBillToPhone",reqBillToPhone1);
		editCreditCardToSubsInfo.put("reasonCode",reasonCode1);
		editCreditCardToSubsInfo.put("signedDateTime",signedDateTime1);
		editCreditCardToSubsInfo.put("reqCurrency",reqCurrency1);
		editCreditCardToSubsInfo.put("message",message1);
		editCreditCardToSubsInfo.put("authTransRefNo",authTransRefNo1);
		editCreditCardToSubsInfo.put("reqPaymentMethod",reqPaymentMethod1);
		editCreditCardToSubsInfo.put("transactionId",transactionId1);
		editCreditCardToSubsInfo.put("reqProfileId",reqProfileId1);
		editCreditCardToSubsInfo.put("reqConsumerId",userSPSID);
		editCreditCardToSubsInfo.put("authResponse",authResponse1);
		editCreditCardToSubsInfo.put("reqTransactionUuId",reqTransactionUuId1);
		editCreditCardToSubsInfo.put("reqBillToSurname",reqBillToSurname2);
		editCreditCardToSubsInfo.put("reqBillToForename",reqBillToForename2);
		editCreditCardToSubsInfo.put("reqBillToAddressCountry",reqBillToAddressCountry1);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData35",reqMerchantDefinedData35a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData30",reqMerchantDefinedData30a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData9",reqMerchantDefinedData9a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData27",reqMerchantDefinedData27a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData25",reqMerchantDefinedData25b);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData22",reqMerchantDefinedData22b);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData23",reqMerchantDefinedData23a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData20",reqMerchantDefinedData20a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData21",reqMerchantDefinedData21a);
		editCreditCardToSubsInfo.put("reqMerchantDefinedData12",reqMerchantDefinedData12a);
		editCreditCardToSubsInfo.put("authTime",authTime1);
		editCreditCardToSubsInfo.put("reqTransactionType",reqTransactionType1);
		editCreditCardToSubsInfo.put("reqLocale",reqLocale1);
		editCreditCardToSubsInfo.put("authAmount",authAmount1);
		editCreditCardToSubsInfo.put("authAvsCodeRaw",authAvsCodeRaw1);
		editCreditCardToSubsInfo.put("authCode",authCode1);
		editCreditCardToSubsInfo.put("reqCardType",reqCardType1);
		editCreditCardToSubsInfo.put("reqBillToAddressCity",reqBillToAddressCity2);
		editCreditCardToSubsInfo.put("reqBillToAddressLine1",reqBillToAddressLine1b);
		editCreditCardToSubsInfo.put("reqBillToAddressLine2",reqBillToAddressLine2b);
		editCreditCardToSubsInfo.put("reqBillToAddressState",reqBillToAddressState2);
		editCreditCardToSubsInfo.put("reqBillToAddressPostalCode",reqBillToAddressPostalCode2);
		editCreditCardToSubsInfo.put("reqReferenceNumber",reqReferenceNumber2);
		return editCreditCardToSubsInfo;
	}*/
	
	private Map<String, Object> createSignPaymentPayload()
	
	{
		Map<String, Object> signPaymentInfo = new HashMap<String, Object>();
		signPaymentInfo.put("cardNumber",cardNumber);
		signPaymentInfo.put("paymentPartial",paymentPartial);
		signPaymentInfo.put("partial",partial);
		signPaymentInfo.put("paymentFull",paymentFull);
		return signPaymentInfo;
	}
}
